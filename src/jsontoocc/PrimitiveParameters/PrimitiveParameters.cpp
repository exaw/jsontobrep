
#include "PrimitiveParameters.h"

double RF::GetPrimitiveLength::operator()(const CylinderParameters& cylinder) const
{
	return cylinder.length;
}

double RF::GetPrimitiveLength::operator()(const TorusParameters& torus) const
{
	return torus.length;

}

double RF::GetPrimitiveLength::operator()(const ConeParameters& cone) const
{
	return cone.length;

}

RF::SetPrimitiveOrigin::SetPrimitiveOrigin (double origin): origin_(origin)
{

}

void RF::SetPrimitiveOrigin::operator()( CylinderParameters& cylinder) const
{
	cylinder.origin = origin_;
}

void RF::SetPrimitiveOrigin::operator()( TorusParameters& torus) const
{
	torus.origin = origin_;

}

void RF::SetPrimitiveOrigin::operator()( ConeParameters& cone) const
{
	cone.origin = origin_;

}
