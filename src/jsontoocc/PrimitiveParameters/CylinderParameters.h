
#pragma once

namespace RF
{
	struct CylinderParameters
	{
		double origin = 0.0;
		double length = 0.0;
		double radius = 0.0;
	};
}