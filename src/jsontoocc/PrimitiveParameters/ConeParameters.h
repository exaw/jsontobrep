
#pragma once

namespace RF
{
	struct ConeParameters
	{
		double origin = 0.0;
		double length = 0.0;
		double leftRadius = 0.0;
		double rightRadius = 0.0;
	};
}