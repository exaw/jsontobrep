
#pragma once

#include <memory>
#include <variant>
#include <vector>

#include "ConeParameters.h"
#include "CylinderParameters.h"
#include "TorusParameters.h"

namespace RF
{
	using PrimitiveParameters = std::variant<ConeParameters, CylinderParameters, TorusParameters>;

	class GetPrimitiveLength
	{
	public:

		double operator () (const CylinderParameters& cylinder) const;
		double operator () (const ConeParameters& cone) const;
		double operator () (const TorusParameters& torus) const;

	private:

	};

	class SetPrimitiveOrigin
	{
	public:
		SetPrimitiveOrigin (double origin);
		void operator () ( CylinderParameters& cylinder) const;
		void operator () ( ConeParameters& cone) const;
		void operator () ( TorusParameters& torus) const;

	private:
		double origin_ = 0.0;
	};

}