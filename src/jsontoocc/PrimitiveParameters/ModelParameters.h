
#pragma once

#include <memory>
#include <vector>

#include "FeaturedPrimitiveParameters.h"

namespace RF
{
	struct ModelParameters
	{
		std::vector<FeaturedPrimitiveParameters> outerContour;
		std::vector<FeaturedPrimitiveParameters> innerContour;
		// Left and Right cont
	};

	using ModelParametersPtr = std::shared_ptr<ModelParameters>;

}