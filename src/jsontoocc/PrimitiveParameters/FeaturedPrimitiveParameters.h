
#pragma once

#include <memory>
#include <variant>
#include <vector>

#include <FeatureParameters/FeatureParameters.h>

#include "PrimitiveParameters.h"

namespace RF
{
	struct FeaturedPrimitiveParameters
	{
		PrimitiveParameters primitive;
		std::vector<FeatureParameters> features;
		double shift = 0.0;
	};

}