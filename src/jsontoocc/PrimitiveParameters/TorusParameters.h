
#pragma once

namespace RF
{
	enum class TorusCurvature { Concave, Convex };


	struct TorusParameters
	{
		double origin = 0.0;
		double length = 0.0;
		double leftRadius = 0.0;
		double rightRadius = 0.0;
		double radius = 0.0;
		double xCenter = 0.0;
		double yCenter = 0.0;

		TorusCurvature curvature = TorusCurvature::Concave;
	};
}