
#pragma once

#include <vector>

#include <gp_Pnt.hxx>

#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Solid.hxx>
#include <Bnd_Box.hxx>


#include <TopoDS_Compound.hxx>


namespace RF
{

	enum class BndType { Sharp, Tolearance };

	enum class PolygonType { Flat, Square, Hexagon, Polygon };

	class GeometryTool
	{
	public:

		GeometryTool ();

		TopoDS_Face MakeBox2dOnAxe (double x, double length, double width) const;

		TopoDS_Face MakeCylinder2dOnAxe (double x, double leftHeigth, double length, double rightHeigth) const;

		TopoDS_Face MakeCircle2dOnAxe (double x, double length, double leftRadius, double rightRadius,
			double radius, double xCenter, double yCenter, bool isBackward);

		TopoDS_Edge MakeEdge (const gp_Pnt& p1, const gp_Pnt& p2) const;

		TopoDS_Wire MakeWire (const std::vector<gp_Pnt>& points) const;

		TopoDS_Wire MakeWire (const std::vector<TopoDS_Edge>& edges) const;

		TopoDS_Face MakeFace (const TopoDS_Wire& wire) const;

		TopoDS_Compound MakeCompound (const std::vector<TopoDS_Shape>& shapes) const;

		TopoDS_Compound MakeCompound (const std::vector<TopoDS_Solid>& solids) const;

		TopoDS_Compound MakeCompound (const std::vector<TopoDS_Face>& faces) const;

		TopoDS_Shape MakeFuse (const std::vector<TopoDS_Shape>& shapes) const;

		TopoDS_Shape MakeFuse (const std::vector<TopoDS_Solid>& solids) const;

		TopoDS_Shape MakeFuse (const std::vector<TopoDS_Face>& faces) const;

		TopoDS_Shape MakeCut (const TopoDS_Shape& shape1, const TopoDS_Shape& shape2) const;

		TopoDS_Shape MakeCommon (const TopoDS_Shape& shape1, const TopoDS_Shape& shape2) const;

		TopoDS_Solid MakeRevolution (const TopoDS_Face& face, const gp_Ax1& axis, double angle) const;

		TopoDS_Solid MakeRevolutionFullOx (const TopoDS_Face& face) const;

		TopoDS_Solid MakeRadialDrill (double originX, double originZ, double length, double radius, bool withTip ) const;

		TopoDS_Wire MakePolygon ( PolygonType polygonType, double radius, double angle, int number) const;

		TopoDS_Solid MakeMilling (PolygonType polygonType, double radius, double angle, double startAngle, int number) const;


		std::vector<TopoDS_Solid> ExplodeOnSolid (const TopoDS_Shape& shape) const;

		Bnd_Box GetBndBox (const TopoDS_Shape& shape, BndType bndType ) const;

		TopoDS_Shape MakeTransformation ( const TopoDS_Shape& shape, const gp_Trsf& transformation ) const;


	private:

	};
}