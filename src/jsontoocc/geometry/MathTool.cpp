
#include "MathTool.h"

#define _USE_MATH_DEFINES
#include <cmath>
#undef _USE_MATH_DEFINES


namespace RF
{
	double RadianToDegree (double radian)
	{
		return (180 / M_PI) * radian;
	}

	double DegreeToRadian (double degree)
	{
		return ( M_PI / 180) * degree;
	}

}
