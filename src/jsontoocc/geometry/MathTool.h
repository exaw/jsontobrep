
#pragma once


namespace RF
{
	double RadianToDegree (double radian);
	double DegreeToRadian (double radian);
}