#include "GeometryTool.h"

#include <cmath>

#include <gp.hxx>
#include <gp_Ax2d.hxx>
#include <gp_Ax2.hxx>

#include <gp_Circ.hxx>
#include <gp_Pnt.hxx>

#include <GCE2d_MakeArcOfCircle.hxx>

#include <BRepBndLib.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Common.hxx>

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRep_Builder.hxx>

#include <BRepPrimAPI_MakeRevol.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakePrism.hxx>

#include <TopExp_Explorer.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>

#include <TopoDS.hxx>

#include <Precision.hxx>

#include <io/BrepWriter.h>

#include "MathTool.h"

namespace
{
	gp_Pnt MakePoint3dAs2d (double x, double y)
	{
		return gp_Pnt (x, y, 0.0);
	}

}

namespace RF
{

	GeometryTool::GeometryTool ()
	{
	}

	TopoDS_Face GeometryTool::MakeBox2dOnAxe (double x, double length, double width) const
	{
		gp_Pnt p1 = MakePoint3dAs2d (x, 0);
		gp_Pnt p2 = MakePoint3dAs2d (x + length, 0);
		gp_Pnt p3 = MakePoint3dAs2d (x + length, width);
		gp_Pnt p4 = MakePoint3dAs2d (x, width);

		TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p1 });

		TopoDS_Face face = MakeFace (wire);

		return face;
	}

	TopoDS_Face GeometryTool::MakeCylinder2dOnAxe (double x, double leftHeigth, double length, double rightHeigth) const
	{
		gp_Pnt p1 = MakePoint3dAs2d (x, 0.0);
		gp_Pnt p2 = MakePoint3dAs2d (x + length, 0.0);
		gp_Pnt p3 = MakePoint3dAs2d (x + length, rightHeigth);
		gp_Pnt p4 = MakePoint3dAs2d (x, leftHeigth);

		TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p1 });

		TopoDS_Face face = MakeFace (wire);

		return face;
	}

	TopoDS_Face GeometryTool::MakeCircle2dOnAxe (double x, double length, double leftRadius, double rightRadius,
		double radius, double xCenter, double yCenter, bool isBackward)
	{
		gp_Pnt p1 = MakePoint3dAs2d (x, 0.0);

		gp_Pnt p2 = MakePoint3dAs2d (x, leftRadius);
		gp_Pnt p3 = MakePoint3dAs2d (length + x, rightRadius);

		gp_Pnt p4 = MakePoint3dAs2d (x + length, 0.0);

		gp_Pnt center = MakePoint3dAs2d (xCenter + x, yCenter);
		gp_Pnt pc1 = isBackward ? p3 : p2;
		gp_Pnt pc2 = isBackward ? p2 : p3;

		gp_Ax2 ax2 = gp::XOY ();
		ax2.Translate (gp::Origin (), center);
		gp_Circ circle (ax2, radius);



		TopoDS_Edge circleEdge = BRepBuilderAPI_MakeEdge (circle, pc1, pc2).Edge ();
		//TopoDS_Edge circleEdge = BRepBuilderAPI_MakeEdge (circle ).Edge ();

		TopoDS_Edge leftEdge = MakeEdge (p1, p2);
		TopoDS_Edge rightEdge = MakeEdge (p3, p4);
		TopoDS_Edge bottomEdge = MakeEdge (p4, p1);

		TopoDS_Wire wire = MakeWire ({ leftEdge, circleEdge, rightEdge, bottomEdge });
		//TopoDS_Wire wire = MakeWire ({ circleEdge });

		TopoDS_Face face = MakeFace (wire);

		return face;
	}

	TopoDS_Edge GeometryTool::MakeEdge (const gp_Pnt& p1, const gp_Pnt& p2) const
	{
		if (p1.Distance (p2) < Precision::Confusion ())
			return TopoDS_Edge ();

		TopoDS_Edge edge = BRepBuilderAPI_MakeEdge (p1, p2).Edge ();
		return edge;
	}

	TopoDS_Wire GeometryTool::MakeWire (const std::vector<gp_Pnt>& points) const
	{
		const size_t nbPoints = points.size ();
		if (nbPoints < 2)
			return TopoDS_Wire ();

		BRepBuilderAPI_MakeWire makeWire;
		for (size_t i = 0; i < nbPoints - 1; i++)
		{
			const gp_Pnt& p1 = points[i];
			const gp_Pnt& p2 = points[i + 1];

			if (p1.Distance (p2) >= Precision::Confusion ())
			{
				BRepBuilderAPI_MakeEdge makeEdge (p1, p2);
				bool isDone = makeEdge.IsDone ();
				TopoDS_Edge edge = makeEdge.Edge ();
				makeWire.Add (edge);
			}
		}

		bool isDone = makeWire.IsDone ();
		TopoDS_Wire wire = makeWire.Wire ();
		bool isNull = wire.IsNull ();
		return wire;

	}

	TopoDS_Wire GeometryTool::MakeWire (const std::vector<TopoDS_Edge>& edges) const
	{
		BRepBuilderAPI_MakeWire makeWire;
		for (const auto& edge : edges)
		{
			if (edge.IsNull () == false)
				makeWire.Add (edge);
		}

		if (makeWire.IsDone () == false)
			return TopoDS_Wire ();

		return makeWire.Wire ();
	}

	TopoDS_Face GeometryTool::MakeFace (const TopoDS_Wire & wire) const
	{
		if (wire.IsNull () == true)
			return TopoDS_Face ();

		BRepBuilderAPI_MakeFace makeFace (wire);

		if (makeFace.IsDone () == true)
		{
			return makeFace.Face ();
		}
		else
		{
			return TopoDS_Face ();
		}

		return TopoDS_Face ();
	}

	TopoDS_Compound GeometryTool::MakeCompound (const std::vector<TopoDS_Shape>& shapes) const
	{
		BRep_Builder builder;
		TopoDS_Compound compound;
		builder.MakeCompound (compound);
		for (const auto& shape : shapes)
		{
			if (shape.IsNull () == false)
				builder.Add (compound, shape);
		}

		return compound;
	}

	TopoDS_Compound GeometryTool::MakeCompound (const std::vector<TopoDS_Solid>& solids) const
	{
		std::vector<TopoDS_Shape> shapes;

		for (const auto& solid : solids)
			shapes.push_back (solid);

		TopoDS_Compound compound = MakeCompound (shapes);
		return compound;
	}

	TopoDS_Compound GeometryTool::MakeCompound (const std::vector<TopoDS_Face>& faces) const
	{
		std::vector<TopoDS_Shape> shapes;

		for (const auto& face : faces)
			shapes.push_back (face);

		TopoDS_Compound compound = MakeCompound (shapes);
		return compound;
	}

	TopoDS_Shape GeometryTool::MakeFuse (const std::vector<TopoDS_Shape>& shapes) const
	{
		if (shapes.empty ())
			return TopoDS_Shape ();

		const size_t nbShapes = shapes.size ();
		if (nbShapes == 1)
			return shapes[0];

		TopoDS_Shape fuseShape = shapes[0];
		for (size_t i = 1; i < nbShapes; i++)
		{
			BRepAlgoAPI_Fuse fuseAlgo (fuseShape, shapes[i]);
			fuseAlgo.Build ();
			if (fuseAlgo.IsDone () == false)
				return TopoDS_Shape ();

			fuseShape = fuseAlgo.Shape ();

		}

		return fuseShape;
	}

	TopoDS_Shape GeometryTool::MakeFuse (const std::vector<TopoDS_Solid>& solids) const
	{
		std::vector<TopoDS_Shape> shapes;

		for (const auto& solid : solids)
			shapes.push_back (solid);

		TopoDS_Shape shape = MakeFuse (shapes);
		if (shape.IsNull ())
			return TopoDS_Shape ();

		return shape;
	}

	TopoDS_Shape GeometryTool::MakeFuse (const std::vector<TopoDS_Face>& faces) const
	{
		std::vector<TopoDS_Shape> shapes;

		for (const auto& face : faces)
			shapes.push_back (face);

		TopoDS_Shape shape = MakeFuse (shapes);
		if (shape.IsNull ())
			return TopoDS_Shape ();

		return shape;
	}

	TopoDS_Shape GeometryTool::MakeCut (const TopoDS_Shape& shape1, const TopoDS_Shape& shape2) const
	{
		if (shape1.IsNull () && shape2.IsNull ())
			return TopoDS_Shape ();

		if (shape1.IsNull () == false && shape2.IsNull ())
			return shape1;


		BRepAlgoAPI_Cut cutAlgo (shape1, shape2);
		cutAlgo.Build ();
		if (cutAlgo.IsDone () == false)
			return TopoDS_Shape ();

		TopoDS_Shape shape = cutAlgo.Shape ();

		return shape;
	}

	TopoDS_Shape GeometryTool::MakeCommon (const TopoDS_Shape& shape1, const TopoDS_Shape& shape2) const
	{
		if (shape1.IsNull () || shape2.IsNull ())
			return TopoDS_Shape ();

		BRepAlgoAPI_Common commonAlgo (shape1, shape2);
		commonAlgo.Build ();
		if (commonAlgo.IsDone () == false)
			return TopoDS_Shape ();

		TopoDS_Shape shape = commonAlgo.Shape ();

		return shape;
	}

	TopoDS_Solid GeometryTool::MakeRevolution (const TopoDS_Face& face, const gp_Ax1& axis, double angle) const
	{
		if (face.IsNull ())
			return TopoDS_Solid ();

		BRepPrimAPI_MakeRevol makeRevol (face, axis, angle, true);
		TopoDS_Shape revolShape = makeRevol.Shape ();
		TopoDS_Solid revolSolid = TopoDS::Solid (revolShape);
		return revolSolid;
	}

	TopoDS_Solid GeometryTool::MakeRevolutionFullOx (const TopoDS_Face& face) const
	{
		if (face.IsNull ())
			return TopoDS_Solid ();

		TopoDS_Solid revolSolid = MakeRevolution (face, gp::OX (), 2 * M_PI);
		return revolSolid;
	}

	TopoDS_Solid GeometryTool::MakeRadialDrill (double originX, double originZ, double length, double radius, bool withTip ) const
	{
		//BRepPrimAPI_MakeCone
		gp_Ax2 cylAxes (gp_Pnt (originX, 0.0, originZ), gp_Dir (0, 0, -1));
		TopoDS_Solid cylinder = BRepPrimAPI_MakeCylinder (cylAxes, radius, length).Solid ();
		if (withTip == false)
		{
			return cylinder;
		}
		else
		{
			double coneLength = (radius / 2.0) * tan (DegreeToRadian (118) / 2);

			gp_Ax2 coneAxes (gp_Pnt (originX, 0.0, originZ - length), gp_Dir (0, 0, -1));
			BRepPrimAPI_MakeCone makeCone (coneAxes, radius, 0.0, coneLength);

			TopoDS_Solid cone = makeCone.Solid ();

			std::vector<TopoDS_Solid> cylAndCone { cylinder, cone };
			TopoDS_Shape tipedDrill = MakeFuse (cylAndCone);

			std::vector<TopoDS_Solid> solids = ExplodeOnSolid (tipedDrill);
			BrepWriter ().Write ("tipedDrill.brep", tipedDrill);

			if (solids.size () != 1)
				throw std::logic_error ("Error: MakeRadialDrill - not single part in result");

			return TopoDS::Solid (solids[0]);
		}
		
		return TopoDS_Solid ();
	}

	TopoDS_Wire GeometryTool::MakePolygon (PolygonType polygonType, double radius, double angle, int number) const
	{
		if (polygonType == PolygonType::Flat)
		{
			double verySmall = -10000;
			double veryBig = 10000;
			gp_Pnt p1 (0.0, radius, verySmall);
			gp_Pnt p2 (0.0, radius, veryBig);
			gp_Pnt p3 (0.0, -1 * radius, veryBig);
			gp_Pnt p4 (0.0, -1 * radius, verySmall);

			TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p1 });
			return wire;
		}
		else  if (polygonType == PolygonType::Square)
		{
			double verySmall = -10000;
			double veryBig = 10000;
			gp_Pnt p1 (0.0, -1 * radius, -1 * radius);
			gp_Pnt p2 (0.0, radius, -1 * radius);
			gp_Pnt p3 (0.0, radius, radius);
			gp_Pnt p4 (0.0, -1 * radius, radius);

			TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p1 });
			return wire;
		}
		else  if (polygonType == PolygonType::Hexagon)
		{
			double verySmall = -10000;
			double veryBig = 10000;

			double a = (2 * radius) / sqrt (3);
			gp_Pnt p1 (0.0, 0.0, a);
			gp_Pnt p2 = p1.Rotated (gp::OX (), M_PI / 3);
			gp_Pnt p3 = p2.Rotated (gp::OX (), M_PI / 3);
			gp_Pnt p4 = p3.Rotated (gp::OX (), M_PI / 3);
			gp_Pnt p5 = p4.Rotated (gp::OX (), M_PI / 3);
			gp_Pnt p6 = p5.Rotated (gp::OX (), M_PI / 3);

			TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p5, p6, p1 });
			return wire;
		}
		else  if (polygonType == PolygonType::Polygon)
		{
			if ( number == 1)
			{
				double verySmall = -10000;
				double veryBig = 10000;

				double a = (2 * radius) / sqrt (3);
				gp_Pnt p1 (0.0, 0.0, a);
				gp_Pnt p2 = p1.Rotated (gp::OX (), M_PI / 3);
				gp_Pnt p3 = p2.Rotated (gp::OX (), M_PI / 3);
				gp_Pnt p4 = p3.Rotated (gp::OX (), M_PI / 3);
				gp_Pnt p5 = p4.Rotated (gp::OX (), M_PI / 3);
				gp_Pnt p6 = p5.Rotated (gp::OX (), M_PI / 3);

				TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p5, p6, p1 });
				return wire;
			}
			else
			{
				double h = radius;
				double normAngle = angle / 2.0;
				double d = tan (normAngle) * h;

				std::vector<gp_Pnt> points{ gp_Pnt (0.0, h, d) };
				for (int i = 1; i < number; i++)
				{
					gp_Pnt p  = points[i-1].Rotated (gp::OX (), angle );
					points.push_back (p);

				}
				points.push_back (points[0]);

				TopoDS_Wire wire = MakeWire (points);
				return wire;
			}
		}
		else
		{
			double verySmall = -10000;
			double veryBig = 10000;
			gp_Pnt p1 (0.0, veryBig, verySmall);
			gp_Pnt p2 (0.0, veryBig, veryBig);
			gp_Pnt p3 (0.0, -1 * veryBig, veryBig);
			gp_Pnt p4 (0.0, -1 * veryBig, verySmall);

			TopoDS_Wire wire = MakeWire ({ p1, p2, p3, p4, p1 });
			return wire;

		}

		return TopoDS_Wire ();
	}

	TopoDS_Solid GeometryTool::MakeMilling (PolygonType polygonType, double radius, double angle, double startAngle, int number) const
	{
		TopoDS_Wire polygon = MakePolygon (polygonType, radius, angle, number);
		if (polygon.IsNull ())
			return TopoDS_Solid ();

		TopoDS_Face polygonFace = MakeFace (polygon);
		if (polygonFace.IsNull ())
			return TopoDS_Solid ();

		double verySmall = -10000;
		double veryBig = 10000;

		gp_Trsf translation;
		translation.SetTranslation (gp_Pnt (0, 0, 0), gp_Pnt (verySmall, 0, 0));

		TopoDS_Shape translatedPolygonFace = MakeTransformation (polygonFace, translation);
		gp_Vec extrudeVector (gp_Pnt (verySmall, 0, 0), gp_Pnt (veryBig, 0, 0));

		BRepPrimAPI_MakePrism makePrism (translatedPolygonFace, extrudeVector);
		makePrism.Build ();
		if (makePrism.IsDone () == false)
			return TopoDS_Solid ();

		TopoDS_Shape prism = makePrism.Shape ();
		gp_Trsf rotation;
		rotation.SetRotation (gp::OX (), startAngle);

		TopoDS_Shape rotatedPrism = MakeTransformation (prism, rotation);

		std::vector<TopoDS_Solid> solids = ExplodeOnSolid (rotatedPrism);
		if (solids.size () != 1)
			throw std::logic_error ("Error: MakeMilling - not single part in result");

		TopoDS_Solid millingTool = solids[0];

		return millingTool;

	}

	std::vector<TopoDS_Solid> GeometryTool::ExplodeOnSolid (const TopoDS_Shape& shape) const
	{
		TopTools_IndexedMapOfShape solidShapes;
		TopExp::MapShapes (shape, TopAbs_SOLID, solidShapes);

		std::vector<TopoDS_Solid> solids;
		for (int i = 1; i <= solidShapes.Extent (); i++)
			solids.push_back (TopoDS::Solid (solidShapes (i)));

		return solids;

	}

	Bnd_Box GeometryTool::GetBndBox (const TopoDS_Shape& shape, BndType bndType) const
	{
		bool isUsingTriangulation = (bndType == BndType::Tolearance);
		Bnd_Box box;
		BRepBndLib::Add (shape, box, isUsingTriangulation);
		return box;
	}

	TopoDS_Shape GeometryTool::MakeTransformation (const TopoDS_Shape& shape, const gp_Trsf& transformation) const
	{
		BRepBuilderAPI_Transform transformTool (shape, transformation, true);
		TopoDS_Shape transformShape = transformTool.Shape ();
		return transformShape;
	}

}
