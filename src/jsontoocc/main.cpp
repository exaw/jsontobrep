
#include <iostream>
#include <builder/CylinderBuilder.h>
#include <PrimitiveParameters/CylinderParameters.h>
#include <io/BrepWriter.h>
#include <io/JsonReader.h>
#include <CncModel/BuildGeometryBar.h>

#include <optional>
#include <boost/program_options.hpp>


using std::cout;
using std::endl;
using std::string;
namespace po = boost::program_options;


class Messager
{
public:
	Messager (const std::string& message) : message_ (message)
	{
		cout << "Begin " << message_ << endl;
	}

	~Messager ()
	{
		cout << "End " << message_ << endl;
	}

private:
	std::string message_;
};

void test1 ()
{
	Messager m ("test 1");
	RF::CylinderParameters cylinder;
	cylinder.length = 10;
	cylinder.origin = 1;
	cylinder.radius = 3;

	RF::CylinderBuilder cylinderBuilder (cylinder);
	TopoDS_Face face = cylinderBuilder.GetBrepGeometry2d ();

	RF::BrepWriter ().Write ("cylinder_shape_1.brep", face);
}

void test2 ()
{

	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\cylinder_1.json)";
	Messager m ("test 2");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);
}

void test3 ()
{
	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\cylinder_1.json)";
	Messager m ("test 3");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	auto geometryBar = RF::BuildGeometryBar (model);
	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();

	RF::BrepWriter ().Write ("cylinder_shape_2_3d.brep", shape3d);
	RF::BrepWriter ().Write ("cylinder_shape_2_2d.brep", shape2d);

}

void test4 ()
{
	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\cone_1.json)";
	Messager m ("test 4");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	auto geometryBar = RF::BuildGeometryBar (model);
	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();

	RF::BrepWriter ().Write ("cone_shape_1_3d.brep", shape3d);
	RF::BrepWriter ().Write ("cone_shape_1_2d.brep", shape2d);

}

void test5 ()
{
	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\circle_1.json)";
	Messager m ("test 5");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	auto geometryBar = RF::BuildGeometryBar (model);
	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();

	RF::BrepWriter ().Write ("circle_shape_1_3d.brep", shape3d);
	RF::BrepWriter ().Write ("circle_shape_1_2d.brep", shape2d);

}

void test6 ()
{
	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\3_part.json)";
	Messager m ("test 6");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	auto geometryBar = RF::BuildGeometryBar (model);
	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();

	RF::BrepWriter ().Write ("3_part_1_shape_3d.brep", shape3d);
	RF::BrepWriter ().Write ("3_part_1_shape_2d.brep", shape2d);

}


void test7 ()
{
	std::string jsonFile = R"(F:\Projects\rapidfactory\develop\data\primitives\3_part_2.json)";
	Messager m ("test 7");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	auto geometryBar = RF::BuildGeometryBar (model);
	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();

	RF::BrepWriter ().Write ("3_part_2_shape_3d.brep", shape3d);
	RF::BrepWriter ().Write ("3_part_2_shape_2d.brep", shape2d);

}

void test8 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\171208_Adapterflansch\)";
	std::string jsonFile = directory + "171208_Adapterflansch.json";
	Messager m ("test 8");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171208_Adapterflansch_3d.brep", shape3d);
	}
	else
	{
		cout << "Error: Can not create 3D Shape" << endl;
	}

	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171208_Adapterflansch_2d.brep", shape2d);
	}
	else
	{
		cout << "Error: Can not create 2D Shape" << endl;
	}
}

void test9 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\171209_Plattengewicht\)";
	std::string jsonFile = directory + "171209_Plattengewicht.json";
	Messager m ("test 9");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_Plattengewicht_3d.brep", shape3d);
	}
	else
	{
		cout << "Error: Can not create 3D Shape" << endl;
	}

	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_Plattengewicht_2d.brep", shape2d);
	}
	else
	{
		cout << "Error: Can not create 2D Shape" << endl;
	}
}

void test10 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\171209_3D-Druck Duse\)";
	std::string jsonFile = directory + "171209_3D-Druck Duse.json";
	Messager m ("test 10");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_3D-Druck Duse_3d.brep", shape3d);
	}
	else
	{
		cout << "Error: Can not create 3D Shape" << endl;
	}

	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_3D-Druck Duse_2d.brep", shape2d);
	}
	else
	{
		cout << "Error: Can not create 2D Shape" << endl;
	}
}

void test11 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\171209_Air Nozzle\)";
	std::string jsonFile = directory + "171209_Air Nozzle.json";
	Messager m ("test 11");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_Air Nozzle_3d.brep", shape3d);
	}
	else
	{
		cout << "Error: Can not create 3D Shape" << endl;
	}

	TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
	if (shape3d.IsNull () == false)
	{
		RF::BrepWriter ().Write (directory + "171209_Air Nozzle_2d.brep", shape2d);
	}
	else
	{
		cout << "Error: Can not create 2D Shape" << endl;
	}
}

void test12 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "radial_boring.json";
	Messager m ("test 12");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "radial_boring_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "radial_boring_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test13 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "axial_boring.json";
	Messager m ("test 13");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "axial_boring_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "axial_boring_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test14 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "axial_boring_cone.json";
	Messager m ("test 14");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "axial_boring_cone_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "axial_boring_cone_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test15 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "boring.json";
	Messager m ("test 15");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "boring_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "boring_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test16 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "milling.json";
	Messager m ("test 16");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "milling_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "milling_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}


void test17 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "milling_polygon.json";
	Messager m ("test 17");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "milling_polygon_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "milling_polygon_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test18 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\features\)";
	std::string jsonFile = directory + "sample_1.json";
	Messager m ("test 18");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "sample_1_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "msample_1_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}


void test19 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\180107_Rolle\)";
	std::string jsonFile = directory + "180107_Rolle.json";
	Messager m ("test 19");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "180107_Rolle_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "180107_Rolle_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test20 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\develop\data\templates\180107_\)";
	std::string jsonFile = directory + "180107_.json";
	Messager m ("test 20");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "180107_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "180107_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}

void test21 ()
{
	std::string directory = R"(F:\Projects\rapidfactory\uvi\STEPImport\data\)";
	std::string jsonFile = directory + "Konuse und zylinder.json";
	Messager m ("test 21");
	RF::JsonReader reader;
	RF::ModelParametersPtr model = reader.Read (jsonFile);

	if (model == nullptr)
	{
		cout << "Error: Can not read model from json file" << endl;
		return;
	}

	auto geometryBar = RF::BuildGeometryBar (model);
	if (geometryBar == nullptr)
	{
		cout << "Error: Can not create basic bar from model" << endl;
		return;
	}

	{
		TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
		if (shape3d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "Konuse und zylinder_3d.brep", shape3d);
		}
		else
		{
			cout << "Error: Can not create 3D Shape" << endl;
		}
	}

	{

		TopoDS_Shape shape2d = geometryBar->GetGeometry2d ();
		if (shape2d.IsNull () == false)
		{
			RF::BrepWriter ().Write (directory + "Konuse und zylinder_2d.brep", shape2d);
		}
		else
		{
			cout << "Error: Can not create 2D Shape" << endl;
		}
	}
}


struct Parameters
{
	string jsonFile;
	string geomFile;
};

std::optional<Parameters> GetParameters (int argc, char* argv[])
{
	try
	{
		po::options_description desc ("Allowed options");
		Parameters parameters;
		desc.add_options ()
			("help,h", "print usage message")
			("json,j", po::value (&parameters.jsonFile), "json file to read")
			("geometry,g", po::value (&parameters.geomFile), "geometry file to write");
		po::variables_map vm;
		po::store (po::command_line_parser (argc, argv).options (desc).run (), vm);
		po::notify (vm);
		if (vm.count ("help") || !vm.count ("json") || !vm.count ("geometry"))
		{
			cout << desc << "\n";
			return {};
		}

		return parameters;
	}
	catch (std::exception& e)
	{
		cerr << "Exception: " << e.what () << endl;
	}
	catch (...)
	{
		return {};
	}
	return {};
}

void ProcessJsonFile ( const Parameters& parameters )
{
	try
	{
		Messager m ("Process json file");
		cout << "Json file: "<<parameters.jsonFile<<"\nGeometry file: "<<parameters.geomFile<<endl;

		RF::JsonReader reader;
		RF::ModelParametersPtr model = reader.Read (parameters.jsonFile);

		if (model == nullptr)
		{
			cout << "Error: Can not read model from json file" << endl;
			return;
		}

		auto geometryBar = RF::BuildGeometryBar (model);
		if (geometryBar == nullptr)
		{
			cout << "Error: Can not create basic bar from model" << endl;
			return;
		}

		{
			TopoDS_Shape shape3d = geometryBar->GetGeometry3d ();
			if (shape3d.IsNull () == false)
			{
				RF::BrepWriter ().Write (parameters.geomFile, shape3d);
			}
			else
			{
				cout << "Error: Can not create 3D Shape" << endl;
			}
		}


	}
	catch (std::exception& e)
	{
		cerr << "Std Exception: " << e.what () << endl;
	}
	catch (...)
	{
		cerr << "Unknown Exception: " << endl;
	}

}

int main (int argc, char* argv[])
{
	cout << "Hello World!" << endl;

	try
	{

		auto optParameters = GetParameters (argc, argv);
		if (!optParameters)
			return 0;

		const Parameters parameters = optParameters.value ();
		
		ProcessJsonFile (parameters);
	}
	catch (std::exception& e)
	{
		cerr << "Std Exception: " << e.what () << endl;
	}
	catch (...)
	{
		cerr << "Unknown Exception: " << endl;
	}

}