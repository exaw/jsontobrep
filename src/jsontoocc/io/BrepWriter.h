
#pragma once

#include <PrimitiveParameters/CylinderParameters.h>

#include <TopoDS_Shape.hxx>

namespace RF
{
	class BrepWriter
	{
	public:

		BrepWriter ( );

		void Write ( const std::string& fileName, const TopoDS_Shape& shape ) const;
	
	};
}