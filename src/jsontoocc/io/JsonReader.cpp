
#include "JsonReader.h"

#include <stdexcept>
#include <fstream>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include <PrimitiveParameters/PrimitiveParameters.h>
#include <PrimitiveParameters/FeaturedPrimitiveParameters.h>

#include <geometry/MathTool.h>


namespace rjson = rapidjson;

namespace RF
{
	namespace
	{

		std::optional<CylinderParameters> ParseCylinderJson (const rjson::Value& item)
		{
			CylinderParameters cylinder;
			//cylinder.origin = item["left"]["dimension"]["value"].GetDouble ();
			cylinder.origin = 0.0;
			cylinder.length = item["middle"]["dimension"]["value"].GetDouble ();
			double diameter = item["right"]["dimension"]["value"].GetDouble ();
			cylinder.radius = diameter / 2.0;
			return cylinder;
		}

		std::optional<ConeParameters> ParseConeJson (const rjson::Value& item)
		{
			ConeParameters cone;
			//cylinder.origin = item["left"]["dimension"]["value"].GetDouble ();
			double leftDiameter = item["left"]["dimension"]["value"].GetDouble ();
			cone.leftRadius = leftDiameter / 2.0;

			cone.length = item["middle"]["dimension"]["value"].GetDouble ();

			double rightDiameter = item["right"]["dimension"]["value"].GetDouble ();
			cone.rightRadius = rightDiameter / 2.0;

			return cone;

		}

		std::optional<TorusParameters> ParseTorusJson (const rjson::Value& item)
		{
			TorusParameters torus;
			double leftDiameter = item["left"]["dimension"]["value"].GetDouble ();
			torus.leftRadius = leftDiameter / 2.0;

			double rightDiameter = item["right"]["dimension"]["value"].GetDouble ();
			torus.rightRadius = rightDiameter / 2.0;

			torus.length = item["middle"]["dimension"]["value"].GetDouble ();
			torus.radius = item["middle"]["radius"]["value"].GetDouble ();

			torus.xCenter = item["middle"]["radius"]["z"]["value"].GetDouble ();
			torus.yCenter = item["middle"]["radius"]["x"]["value"].GetDouble ();

			int curvature = item["middle"]["radius"]["curvature"].GetInt ();
			torus.curvature = curvature == 1 ? TorusCurvature::Concave : TorusCurvature::Convex;

			return torus;

		}


		PrimitiveParameters ParsePrimitiveJson (const rjson::Value& item)
		{
			const std::string type = item["type"].GetString ();
			if (type == "cylinder")
			{
				std::optional<CylinderParameters> optCylinder = ParseCylinderJson (item);
				if (optCylinder.has_value () == false)
				{
					throw std::logic_error ("Error: parsing cylinder");
				}
				else
				{
					return optCylinder.value ();
				}
			}
			else if (type == "cone")
			{
				std::optional<ConeParameters> optCone = ParseConeJson (item);
				if (optCone.has_value () == false)
				{
					throw std::logic_error ("Error: parsing cone");
				}
				else
				{
					return optCone.value ();
				}
			}
			else if (type == "circle")
			{
				std::optional<TorusParameters> optTorus = ParseTorusJson (item);
				if (optTorus.has_value () == false)
				{
					throw std::logic_error ("Error: parsing circle");
				}
				else
				{
					return optTorus.value ();
				}
			}
			{
				throw std::logic_error ("Error: unknown primitive type");
			}

			return PrimitiveParameters ();
		}

		std::optional<RadialBoringParameters> ParseRadialBoringJson (const rjson::Value& item)
		{
			RadialBoringParameters boring;
			boring.diameter = item["diameter"]["value"].GetDouble ();
			boring.axisOffset = item["offsetZ"]["value"].GetDouble ();
			boring.depth = item["depth"]["value"].GetDouble ();

			std::string depthType = item["depthType"].GetString ();
			if (depthType == "throughAll")
				boring.depthType = DepthType::ThroughAll;
			else if (depthType == "tillNext")
				boring.depthType = DepthType::UntillNext;
			else if (depthType == "dimension")
				boring.depthType = DepthType::Dimension;
			else
			{
				//throw std::logic_error ("Error: Undefined Radial Boring depthType");
				std::cerr << "Error: Undefined Radial Boring depthType" << std::endl;
				return std::optional<RadialBoringParameters> ();
			}


			return boring;

		}

		std::optional<AxialBoringParameters> ParseAxialBoringJson (const rjson::Value& item)
		{
			AxialBoringParameters boring;
			bool isNull = item.IsNull ();

			const rjson::Value& contourJson = item[0]["contour"];
			const size_t nbContour = contourJson.Size ();
			double shift = 0.0;

			for (size_t i = 0; i < nbContour; i++)
			{
				const rjson::Value& item = contourJson[i];

				PrimitiveParameters primitive = ParsePrimitiveJson (item);

				std::visit (SetPrimitiveOrigin (shift), primitive);
				shift += std::visit (GetPrimitiveLength (), primitive);

				boring.primitives.push_back (primitive);
			}

			boring.nbBoring = item[0]["circularPattern"]["number"].GetInt ();
			boring.pitchDiameter = item[0]["circularPattern"]["diameter"].GetDouble ();

			double intermediateAngle = item[0]["circularPattern"]["angle"].GetDouble ();
			boring.intermediateAngle = DegreeToRadian (intermediateAngle);

			double startAngle = item[0]["circularPattern"]["startAngle"].GetDouble ();
			boring.startAngle = DegreeToRadian (startAngle);

			//boring.diameter1 = item["diameter"]["value"].GetDouble ();
			//boring.axisOffset = item["offsetZ"]["value"].GetDouble ();
			//boring.depth = item["depth"]["value"].GetDouble ();

			return boring;

		}

		std::optional<MillingParameters> ParseMillJson (const rjson::Value& item)
		{
			MillingParameters mill;
			mill.centerToPlaneDimension = item["centerToPlaneDimension"]["value"].GetDouble ();
			double angle = item["circularPattern"]["angle"].GetDouble ();
			mill.angle = DegreeToRadian (angle);

			double startAngle = item["circularPattern"]["startAngle"].GetDouble ();
			mill.startAngle = DegreeToRadian (startAngle);

			mill.number = item["circularPattern"]["number"].GetInt ();


			std::string millType = item["description"].GetString ();
			if (millType == "polygon")
				mill.millingType = MillingType::Polygon;
			else if (millType == "square")
				mill.millingType = MillingType::Square;
			else if (millType == "hexagon")
				mill.millingType = MillingType::Hexagon;
			else if (millType == "widthAcrossFlats")
				mill.millingType = MillingType::Flat;
			else
			{
				std::cerr << "Error: Undefined Milling Type" << std::endl;
				return std::optional<MillingParameters> ();
			}

			return mill;

		}


		std::vector <FeatureParameters> ParseFeaturesJson (const rjson::Value& item)
		{
			std::vector <FeatureParameters> features;

			if (item["middle"].HasMember ("feature"))
			{
				const rjson::Value& feautureItem = item["middle"]["feature"];
				if (feautureItem.HasMember ("radialBoring"))

				{
					const rjson::Value& radialBoringItem = feautureItem["radialBoring"];

					auto radialBoringOpt = ParseRadialBoringJson (radialBoringItem);
					if (radialBoringOpt.has_value ())
					{

						double cylDiameter = item["right"]["dimension"]["value"].GetDouble ();

						auto radialBoring = radialBoringOpt.value ();
						radialBoring.radialOffset = cylDiameter / 2.0;
						features.push_back (radialBoring);
					}
				}
				else if (feautureItem.HasMember ("mill"))

				{
					const rjson::Value& millItem = feautureItem["mill"];

					auto millOpt = ParseMillJson (millItem);
					if (millOpt.has_value ())
					{
						auto mill = millOpt.value ();
						features.push_back (mill);
					}
				}

			}

			if (item["left"].HasMember ("feature"))
			{
				const rjson::Value& feautureItem = item["left"]["feature"];
				if (feautureItem.HasMember ("boring"))
				{
					const rjson::Value& axialBoringItem = feautureItem["boring"];

					auto axialBoringOpt = ParseAxialBoringJson (axialBoringItem);
					if (axialBoringOpt.has_value ())
					{
						auto axialBoring = axialBoringOpt.value ();
						features.push_back (axialBoring);
					}
				}

			}


			return features;
		}

		FeaturedPrimitiveParameters ParseFeaturedPrimitiveJson (const rjson::Value& item)
		{
			PrimitiveParameters primitive = ParsePrimitiveJson (item);
			std::vector <FeatureParameters> features = ParseFeaturesJson (item);

			FeaturedPrimitiveParameters featuredPrimitive;
			featuredPrimitive.primitive = primitive;
			featuredPrimitive.features = features;

			return featuredPrimitive;
		}






	}

	JsonReader::JsonReader ()
	{
	}

	ModelParametersPtr JsonReader::Read (const std::string& fileName) const
	{
		try
		{

			std::ifstream inputStream (fileName.c_str ());
			if (inputStream.is_open () == false)
				return nullptr;

			//std::string line;
			//std::getline (inputStream, line);

			std::stringstream ss;
			ss << inputStream.rdbuf ();
			std::string jsonString = ss.str ();

			//skipping of UTF BOM symbol if there is
			size_t beginPosition = jsonString.find ('{');
			if (beginPosition == std::string::npos)
				throw std::logic_error ("Not found begin json '{' symbol");

			//std::string jsonString ((std::istreambuf_iterator<char> (inputStream)), std::istreambuf_iterator<char> ());
			jsonString = jsonString.substr (beginPosition);
			rjson::Document document;
			document.Parse (jsonString.c_str ());

			if (document.HasParseError () == true)
			{
				rjson::ParseErrorCode error = document.GetParseError ();
				return nullptr;
			}

			if (document.HasMember ("innerContour") == false)
				return nullptr;

			if (document.HasMember ("outerContour") == false)
				return nullptr;

			RF::ModelParametersPtr model = std::make_shared<ModelParameters> ();

			{
				const rjson::Value& innerContourJson = document["innerContour"];
				const size_t nbInnerContour = innerContourJson.Size ();
				double shift = 0;

				for (size_t i = 0; i < nbInnerContour; i++)
				{
					const rjson::Value& item = innerContourJson[i];

					FeaturedPrimitiveParameters featuredPrimitive = ParseFeaturedPrimitiveJson (item);

					std::visit (SetPrimitiveOrigin (shift), featuredPrimitive.primitive);
					for (auto& feature : featuredPrimitive.features)
					{
						std::visit (SetFeatureOrigin (shift), feature);

					}

					shift += std::visit (GetPrimitiveLength (), featuredPrimitive.primitive);

					model->innerContour.push_back (featuredPrimitive);

				}
			}

			{
				const rjson::Value& outerContourJson = document["outerContour"];
				const size_t nbOuterContour = outerContourJson.Size ();
				double shift = 0;
				for (size_t i = 0; i < nbOuterContour; i++)
				{
					const rjson::Value& item = outerContourJson[i];
					FeaturedPrimitiveParameters featuredPrimitive = ParseFeaturedPrimitiveJson (item);
					std::visit (SetPrimitiveOrigin (shift), featuredPrimitive.primitive);
					for (auto& feature : featuredPrimitive.features)
					{
						std::visit (SetFeatureOrigin (shift), feature);

					}

					shift += std::visit (GetPrimitiveLength (), featuredPrimitive.primitive);

					model->outerContour.push_back (featuredPrimitive);
				}
			}

			return model;
		}
		catch (std::exception& e)
		{
			std::cerr << "Exception (std) in reading json file: " << e.what () << std::endl;
			return nullptr;
		}
		catch (...)
		{
			std::cerr << "Exception (undefined) in reading json file: " << std::endl;
			return nullptr;
		}

	}

}