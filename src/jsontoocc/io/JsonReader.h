
#pragma once

#include <PrimitiveParameters/ModelParameters.h>

namespace RF
{
	class JsonReader
	{
	public:

		JsonReader ( );

		ModelParametersPtr Read ( const std::string& fileName ) const;
	
	};
}