
#include "BrepWriter.h"

#include <geometry/GeometryTool.h>
#include "BrepWriter.h"

#include <BRepTools.hxx>

namespace RF
{

	BrepWriter::BrepWriter ()
	{
	}

	void BrepWriter::Write (const std::string & fileName, const TopoDS_Shape & shape) const
	{
		if (shape.IsNull () == true)
			return;

		BRepTools::Write (shape, fileName.c_str ());
	}

}