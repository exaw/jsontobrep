
#pragma once

namespace RF
{
	enum class MillingType	{ Flat, Square, Hexagon, Polygon };

	struct MillingParameters
	{
		MillingType millingType = MillingType::Hexagon;
		
		double centerToPlaneDimension = 0.0;
		double startAngle = 0.0;
		double angle = 0.0;
		size_t number = 0;
	};
}