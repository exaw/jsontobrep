
#pragma once

#include <vector>

#include <PrimitiveParameters/PrimitiveParameters.h>

namespace RF
{
	enum class AxialBoringBeginType { Cylinder, Cone  };
	enum class AxialBoringEndType { Sharp, Flat };


	struct AxialBoringParameters
	{
		double origin = 0.0;

		std::vector<PrimitiveParameters> primitives;

		double pitchDiameter = 0.0;
		size_t nbBoring = 0;
		double intermediateAngle = 0.0;
		double startAngle = 0.0;

	};
}