
#pragma once

#include <variant>

#include "AxialBoringParameters.h"
#include "MillingParameters.h"
#include "RadialBoringParameters.h"

	namespace RF
	{
	using FeatureParameters = std::variant<RadialBoringParameters, AxialBoringParameters, MillingParameters>;

	class SetFeatureOrigin
	{
	public:
		SetFeatureOrigin (double origin);
		void operator () (AxialBoringParameters& axialBoring) const;
		void operator () (MillingParameters& milling) const;
		void operator () (RadialBoringParameters& radialBoring) const;

	private:
		double origin_ = 0.0;
	};

}