

#include "FeatureParameters.h"
namespace RF
{

	SetFeatureOrigin::SetFeatureOrigin (double origin) : origin_ (origin)
	{

	}

	void SetFeatureOrigin::operator()(AxialBoringParameters& axialBoring) const
	{
		axialBoring.origin = origin_;
	}

	void SetFeatureOrigin::operator()(MillingParameters& milling) const
	{
		
	}

	void SetFeatureOrigin::operator()(RadialBoringParameters& radialBoring) const
	{
		radialBoring.origin = origin_;
	}

}