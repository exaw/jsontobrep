
#pragma once

namespace RF
{

	enum class DepthType { ThroughAll, UntillNext, Dimension };


	struct RadialBoringParameters
	{
		double origin = 0.0;

		double axisOffset = 0.0;
		double radialOffset = 0.0;
		double diameter = 0.0;
		double depth = 0.0;
		DepthType depthType = DepthType::ThroughAll;
	};
}