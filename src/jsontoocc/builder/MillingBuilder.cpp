
#include "MillingBuilder.h"

#include <TopoDS.hxx>


#include <geometry/GeometryTool.h>


namespace RF
{

	MillingBuilder::MillingBuilder (const MillingParameters& millingParameters):
		millingParameters_ (millingParameters)
	{

	}

	TopoDS_Solid MillingBuilder::Apply (const TopoDS_Solid& shape) const
	{
		GeometryTool geomTool;
		TopoDS_Shape commonShape = geomTool.MakeCommon (shape, tool_);
		
		if (commonShape.IsNull ())
			return TopoDS_Solid ();
		
		std::vector<TopoDS_Solid> solids = geomTool.ExplodeOnSolid (commonShape);
		if (solids.size () != 1)
			throw std::logic_error ("Error: MillingBuilder - not single part in result");

		return TopoDS::Solid (solids[0]);

	}

	TopoDS_Face MillingBuilder::Apply (const TopoDS_Face& shape) const
	{
		return TopoDS_Face ();
	}

	TopoDS_Solid MillingBuilder::GetToolShape () const
	{
		return tool_ ;
	}

	void MillingBuilder::BuildTool ()
	{
		GeometryTool geomTool;
		auto const& p = millingParameters_;

		PolygonType polygonType;



		if (p.millingType == MillingType::Flat)
			polygonType = PolygonType::Flat;
		else if (p.millingType == MillingType::Hexagon)
			polygonType = PolygonType::Hexagon;
		else if (p.millingType == MillingType::Polygon)
			polygonType = PolygonType::Polygon;
		else if (p.millingType == MillingType::Square)
			polygonType = PolygonType::Square;
		else
			throw std::logic_error ("Undefined Milling Type");

		TopoDS_Solid tool = geomTool.MakeMilling ( polygonType, p.centerToPlaneDimension, p.angle, p.startAngle, p.number );
		tool_ = tool;
	}

}