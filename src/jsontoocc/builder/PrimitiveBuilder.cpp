
#include "PrimitiveBuilder.h"

#include "CylinderBuilder.h"
#include "ConeBuilder.h"
#include "TorusBuilder.h"


namespace RF
{

		std::pair<TopoDS_Solid, TopoDS_Face> PrimitiveBuilder::operator () (const CylinderParameters& cylinder) const
		{
			CylinderBuilder builder (cylinder);
			TopoDS_Solid solidPart = builder.GetBrepGeometry3d ();
			TopoDS_Face facePart = builder.GetBrepGeometry2d ();
			return { solidPart, facePart };

		}

		std::pair<TopoDS_Solid, TopoDS_Face> PrimitiveBuilder::operator () (const ConeParameters& cone) const
		{
			ConeBuilder builder (cone);
			TopoDS_Solid solidPart = builder.GetBrepGeometry3d ();
			TopoDS_Face facePart = builder.GetBrepGeometry2d ();
			return { solidPart, facePart };
		}

		std::pair<TopoDS_Solid, TopoDS_Face> PrimitiveBuilder::operator()(const TorusParameters& torus ) const
		{
			TorusBuilder builder (torus);
			TopoDS_Solid solidPart = builder.GetBrepGeometry3d ();
			TopoDS_Face facePart = builder.GetBrepGeometry2d ();
			return { solidPart, facePart };
		}

}