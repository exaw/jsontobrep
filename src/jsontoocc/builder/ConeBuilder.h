
#pragma once

#include <PrimitiveParameters/ConeParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class ConeBuilder
	{
	public:

		ConeBuilder (const ConeParameters& coneParameters );

		TopoDS_Solid GetBrepGeometry3d ();
		TopoDS_Face GetBrepGeometry2d ();

	private:

		void Build3d ();

		void Build2d ();

		ConeParameters coneParameters_;

		TopoDS_Solid shape3d_;
		TopoDS_Face shape2d_;
	
	};
}