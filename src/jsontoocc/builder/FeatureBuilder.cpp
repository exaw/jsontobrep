
#include "FeatureBuilder.h"

#include "RadialBoringBuilder.h"
#include "AxialBoringBuilder.h"
#include "MillingBuilder.h"


#include "ConeBuilder.h"
#include "TorusBuilder.h"

namespace RF
{

	FeatureBuilder::FeatureBuilder (const TopoDS_Solid& solid) :solid_ (solid)
	{

	}

	TopoDS_Solid FeatureBuilder::operator()(const RadialBoringParameters& radialBoring) const
	{
		RadialBoringBuilder radialBoringBuilder (radialBoring);
		radialBoringBuilder.BuildTool ();
		TopoDS_Solid boringSolid = radialBoringBuilder.Apply (solid_);
		return boringSolid;
	}

	TopoDS_Solid FeatureBuilder::operator()(const AxialBoringParameters& axialBoring) const
	{
		AxialBoringBuilder axialBoringBuilder (axialBoring);
		axialBoringBuilder.BuildTool ();
		TopoDS_Solid boringSolid = axialBoringBuilder.Apply (solid_);
		return boringSolid;
	}
	

	TopoDS_Solid FeatureBuilder::operator()(const MillingParameters& milling) const
	{
		MillingBuilder millingBuilder (milling);
		millingBuilder.BuildTool ();
		TopoDS_Solid millingSolid = millingBuilder.Apply (solid_);
		return millingSolid;

	}

}