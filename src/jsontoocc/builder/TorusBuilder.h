
#pragma once

#include <PrimitiveParameters/TorusParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class TorusBuilder
	{
	public:

		TorusBuilder (const TorusParameters& torusParameters);

		TopoDS_Solid GetBrepGeometry3d ();
		TopoDS_Face GetBrepGeometry2d ();

	private:

		void Build3d ();

		void Build2d ();

		TorusParameters torusParameters_;

		TopoDS_Solid shape3d_;
		TopoDS_Face shape2d_;
	
	};
}