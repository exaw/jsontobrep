
#include "CylinderBuilder.h"

#include <geometry/GeometryTool.h>

namespace RF
{
	CylinderBuilder::CylinderBuilder (const CylinderParameters & cylinderParameters): cylinderParameters_ (cylinderParameters)
	{

	}

	TopoDS_Solid CylinderBuilder::GetBrepGeometry3d ()
	{
		if (shape3d_.IsNull ())
		{
			Build3d ();
		}

		if (shape3d_.IsNull ())
			return TopoDS_Solid ();

		return shape3d_;
	}

	TopoDS_Face CylinderBuilder::GetBrepGeometry2d ()
	{
		if (shape2d_.IsNull ())
		{
			Build2d ();
		}

		if (shape2d_.IsNull ())
			return TopoDS_Face ();
			
		return shape2d_;

	}

	void CylinderBuilder::Build3d ()
	{
		if (shape2d_.IsNull ())
			Build2d ();

		if (shape2d_.IsNull ())
			return;

		TopoDS_Solid solid = GeometryTool ().MakeRevolutionFullOx (shape2d_);
		if (solid.IsNull () == false)
			shape3d_ = solid;
	}

	void CylinderBuilder::Build2d ()
	{
		shape2d_ = TopoDS_Face ();

		const double x = cylinderParameters_.origin;
		const double length = cylinderParameters_.length;
		const double width = cylinderParameters_.radius;

		GeometryTool geometryTool;

		TopoDS_Face face = geometryTool.MakeBox2dOnAxe (x, length, width);

		if (face.IsNull () == false)
		{
			shape2d_ = face;
		}
	}

}