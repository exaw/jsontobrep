
#pragma once

#include <PrimitiveParameters/CylinderParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class CylinderBuilder
	{
	public:

		CylinderBuilder (const CylinderParameters& cylinderParameters);

		TopoDS_Solid GetBrepGeometry3d ();
		TopoDS_Face GetBrepGeometry2d ();

	private:

		void Build3d ();

		void Build2d ();

		CylinderParameters cylinderParameters_;

		TopoDS_Solid shape3d_;
		TopoDS_Face shape2d_;
	
	};
}