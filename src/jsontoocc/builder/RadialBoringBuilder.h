
#pragma once

#include <FeatureParameters/RadialBoringParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class RadialBoringBuilder
	{
	public:

		RadialBoringBuilder (const RadialBoringParameters& radialBoringParameters);

		TopoDS_Solid Apply (const TopoDS_Solid& shape) const;

		TopoDS_Face Apply (const TopoDS_Face& shape) const;

		TopoDS_Solid GetToolShape () const;

		void BuildTool ();

	private:

		RadialBoringParameters radialBoringParameters_;

		TopoDS_Solid tool_;
	
	};
}