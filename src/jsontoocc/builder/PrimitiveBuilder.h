
#pragma once

#include <utility>

#include <TopoDS_Face.hxx>
#include <TopoDS_Solid.hxx>

#include <PrimitiveParameters/PrimitiveParameters.h>

namespace RF
{
	

	class PrimitiveBuilder
	{
	public:

		std::pair<TopoDS_Solid, TopoDS_Face> operator () (const CylinderParameters& cylinder) const;
		std::pair<TopoDS_Solid, TopoDS_Face> operator () (const ConeParameters& cone) const;
		std::pair<TopoDS_Solid, TopoDS_Face> operator () (const TorusParameters& torus) const;


	private:



	};
}