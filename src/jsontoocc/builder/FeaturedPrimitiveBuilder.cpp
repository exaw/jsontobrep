
#include "FeaturedPrimitiveBuilder.h"

#include "PrimitiveBuilder.h"
#include "FeatureBuilder.h"



namespace RF
{

	FeaturedPrimitiveBuilder::FeaturedPrimitiveBuilder (const FeaturedPrimitiveParameters& featuredPrimitive) :
		featuredPrimitive_ (featuredPrimitive)
	{

	}

	TopoDS_Solid FeaturedPrimitiveBuilder::GetGeometry3d ()
	{
		if (shape3d_.IsNull ())
			BuildGeometry ();

		if (shape3d_.IsNull ())
			return TopoDS_Solid ();

		return shape3d_;
	}

	TopoDS_Face FeaturedPrimitiveBuilder::GetGeometry2d ()
	{
		if (shape2d_.IsNull ())
			BuildGeometry ();

		if (shape2d_.IsNull ())
			return TopoDS_Face ();

		return shape2d_;
	}

	void FeaturedPrimitiveBuilder::BuildGeometry ()
	{
		std::pair<TopoDS_Solid, TopoDS_Face> geometries = std::visit (PrimitiveBuilder (), featuredPrimitive_.primitive);
		if (geometries.first.IsNull () || geometries.second.IsNull ())
			return;

		TopoDS_Solid solid = geometries.first;
		for (const auto& feature : featuredPrimitive_.features)
		{
			TopoDS_Solid featuredSolid = std::visit (FeatureBuilder (solid), feature);
			solid = featuredSolid;
		}

		shape3d_ = solid;

		shape2d_ = geometries.second;

	}

}