
#include "ConeBuilder.h"

#include <geometry/GeometryTool.h>

namespace RF
{
	ConeBuilder::ConeBuilder (const ConeParameters & coneParameters): coneParameters_ (coneParameters)
	{

	}

	TopoDS_Solid ConeBuilder::GetBrepGeometry3d ()
	{
		if (shape3d_.IsNull ())
		{
			Build3d ();
		}

		if (shape3d_.IsNull ())
			return TopoDS_Solid ();

		return shape3d_;
	}

	TopoDS_Face ConeBuilder::GetBrepGeometry2d ()
	{
		if (shape2d_.IsNull ())
		{
			Build2d ();
		}

		if (shape2d_.IsNull ())
			return TopoDS_Face ();
			
		return shape2d_;

	}

	void ConeBuilder::Build3d ()
	{
		if (shape2d_.IsNull ())
			Build2d ();

		if (shape2d_.IsNull ())
			return;

		TopoDS_Solid solid = GeometryTool ().MakeRevolutionFullOx (shape2d_);
		if (solid.IsNull () == false)
			shape3d_ = solid;
	}

	void ConeBuilder::Build2d ()
	{
		shape2d_ = TopoDS_Face ();

		const double x = coneParameters_.origin;
		const double leftHeight = coneParameters_.leftRadius;
		const double length = coneParameters_.length;
		const double rightHeight = coneParameters_.rightRadius;

		GeometryTool geometryTool;

		TopoDS_Face face = geometryTool.MakeCylinder2dOnAxe (x, leftHeight, length, rightHeight);

		if (face.IsNull () == false)
		{
			shape2d_ = face;
		}
	}

}