
#include "AxialBoringBuilder.h"

#include <gp_Trsf.hxx>
#include <TopoDS.hxx>
#include <builder/PrimitiveBuilder.h>
#include <io/BrepWriter.h>


#include <geometry/GeometryTool.h>


namespace RF
{

	AxialBoringBuilder::AxialBoringBuilder (const AxialBoringParameters& axialBoringParameters) :
		axialBoringParameters_ (axialBoringParameters)
	{

	}

	TopoDS_Solid AxialBoringBuilder::Apply (const TopoDS_Solid& shape) const
	{
		if (tool_.IsNull ())
			return TopoDS_Solid ();


		GeometryTool geomTool;
		TopoDS_Shape cutShape = geomTool.MakeCut (shape, tool_);

		if (cutShape.IsNull ())
			return TopoDS_Solid ();

		std::vector<TopoDS_Solid> solids = geomTool.ExplodeOnSolid (cutShape);
		if (solids.size () != 1)
			throw std::logic_error ("Error: RadialBoringBuilder - not single part in result");

		return TopoDS::Solid (solids[0]);
	}

	TopoDS_Face AxialBoringBuilder::Apply (const TopoDS_Face& shape) const
	{
		return shape;
	}

	TopoDS_Shape AxialBoringBuilder::GetToolShape() const
{
		return tool_;
	}

	void AxialBoringBuilder::BuildTool ()
	{
		std::vector<TopoDS_Solid> contour;
		for (const auto& primitive : axialBoringParameters_.primitives)
		{
			std::pair<TopoDS_Solid, TopoDS_Face> geometries = std::visit (PrimitiveBuilder (), primitive);
			if (geometries.first.IsNull ())
				return;

			contour.push_back (geometries.first);
		}

		GeometryTool geomTool;
		TopoDS_Shape contourShape = geomTool.MakeFuse (contour);

		gp_Trsf translation;
		double yTranslate = axialBoringParameters_.pitchDiameter / 2.0;
		translation.SetTranslation (gp_Vec (0.0, yTranslate, 0.0));
		TopoDS_Shape translatedTool = geomTool.MakeTransformation (contourShape, translation);

		std::vector<TopoDS_Shape> drills;
		for (size_t i = 0; i < axialBoringParameters_.nbBoring; i++)
		{
			gp_Trsf rotation;
			rotation.SetRotation (gp::OX (), axialBoringParameters_.startAngle + i * axialBoringParameters_.intermediateAngle);
			TopoDS_Shape rotatedTool = geomTool.MakeTransformation (translatedTool, rotation);
			drills.push_back (rotatedTool);
		}

		TopoDS_Shape tool = geomTool.MakeCompound (drills);
		tool_ = tool;
		

	}

}