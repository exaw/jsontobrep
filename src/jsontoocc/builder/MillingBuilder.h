
#pragma once

#include <FeatureParameters/MillingParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class MillingBuilder
	{
	public:

		MillingBuilder (const MillingParameters& millingParameters_);

		TopoDS_Solid Apply (const TopoDS_Solid& shape) const;

		TopoDS_Face Apply (const TopoDS_Face& shape) const;

		TopoDS_Solid GetToolShape () const;

		void BuildTool ();

	private:

		MillingParameters millingParameters_;

		TopoDS_Solid tool_;
	
	};
}