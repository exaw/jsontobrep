
#include "RadialBoringBuilder.h"

#include <TopoDS.hxx>


#include <geometry/GeometryTool.h>

#include "ConeBuilder.h"


namespace RF
{

	RadialBoringBuilder::RadialBoringBuilder (const RadialBoringParameters& radialBoringParameters):
		radialBoringParameters_ (radialBoringParameters)
	{

	}

	TopoDS_Solid RadialBoringBuilder::Apply (const TopoDS_Solid& shape) const
	{
		GeometryTool geomTool;
		TopoDS_Shape cutShape = geomTool.MakeCut (shape, tool_);
		
		if (cutShape.IsNull ())
			return TopoDS_Solid ();
		
		std::vector<TopoDS_Solid> solids = geomTool.ExplodeOnSolid (cutShape);
		if (solids.size () != 1)
			throw std::logic_error ("Error: RadialBoringBuilder - not single part in result");

		return TopoDS::Solid (solids[0]);

	}

	TopoDS_Face RadialBoringBuilder::Apply (const TopoDS_Face& shape) const
	{
		return TopoDS_Face ();
	}

	TopoDS_Solid RadialBoringBuilder::GetToolShape () const
	{
		return tool_;
	}

	void RadialBoringBuilder::BuildTool ()
	{
		GeometryTool geomTool;
		auto const& p = radialBoringParameters_;

		double depth = p.depth;
		double radialOffset = p.radialOffset;

		if (p.depthType == DepthType::ThroughAll)
		{
			depth = p.radialOffset * 2 * 2;
			radialOffset = p.radialOffset * 2;
		}
		else if (p.depthType == DepthType::UntillNext)
		{
			radialOffset = p.radialOffset * 2;
			depth = radialOffset;
		}

		TopoDS_Solid tool = geomTool.MakeRadialDrill (p.axisOffset + p.origin, radialOffset, depth, p.diameter / 2.0, true );
		
		tool_ = tool;
	}

}