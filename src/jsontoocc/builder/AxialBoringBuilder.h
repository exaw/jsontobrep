
#pragma once

#include <FeatureParameters/AxialBoringParameters.h>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>


namespace RF
{
	class AxialBoringBuilder
	{
	public:

		AxialBoringBuilder (const AxialBoringParameters& axialBoringParameters);

		TopoDS_Solid Apply (const TopoDS_Solid& shape) const;

		TopoDS_Face Apply (const TopoDS_Face& shape) const;

		TopoDS_Shape GetToolShape () const;

		void BuildTool ();

	private:

		AxialBoringParameters axialBoringParameters_;

		TopoDS_Shape tool_;
	
	};
}