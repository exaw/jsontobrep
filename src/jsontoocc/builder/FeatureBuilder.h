
#pragma once

#include <utility>

#include <TopoDS_Face.hxx>
#include <TopoDS_Solid.hxx>

#include <FeatureParameters/FeatureParameters.h>

namespace RF
{


	class FeatureBuilder
	{
	public:
		FeatureBuilder (const TopoDS_Solid& solid);
		
		TopoDS_Solid operator () (const RadialBoringParameters& radialBoring) const;
		TopoDS_Solid operator () (const AxialBoringParameters& axialBoring) const;
		TopoDS_Solid operator () (const MillingParameters& milling ) const;

	private:
		TopoDS_Solid solid_;

	};
}