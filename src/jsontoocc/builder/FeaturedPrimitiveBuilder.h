
#pragma once

#include <utility>

#include <TopoDS_Face.hxx>
#include <TopoDS_Solid.hxx>

#include <PrimitiveParameters/FeaturedPrimitiveParameters.h>

namespace RF
{
	

	class FeaturedPrimitiveBuilder
	{
	public:
		
		FeaturedPrimitiveBuilder (const FeaturedPrimitiveParameters& featuredPrimitive);
		
		TopoDS_Solid GetGeometry3d ();
		
		TopoDS_Face GetGeometry2d ();


	private:

		void BuildGeometry ();

		FeaturedPrimitiveParameters featuredPrimitive_;

		TopoDS_Solid shape3d_;
		TopoDS_Face shape2d_;
	};
}