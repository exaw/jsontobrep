
#include "TorusBuilder.h"

#include <geometry/GeometryTool.h>


namespace RF
{
	TorusBuilder::TorusBuilder (const TorusParameters & torusParameters): torusParameters_ (torusParameters)
	{

	}

	TopoDS_Solid TorusBuilder::GetBrepGeometry3d ()
	{
		if (shape3d_.IsNull ())
		{
			Build3d ();
		}

		if (shape3d_.IsNull ())
			return TopoDS_Solid ();

		return shape3d_;
	}

	TopoDS_Face TorusBuilder::GetBrepGeometry2d ()
	{
		if (shape2d_.IsNull ())
		{
			Build2d ();
		}

		if (shape2d_.IsNull ())
			return TopoDS_Face ();
			
		return shape2d_;

	}

	void TorusBuilder::Build3d ()
	{
		if (shape2d_.IsNull ())
			Build2d ();

		if (shape2d_.IsNull ())
			return;
		
		TopoDS_Solid solid = GeometryTool ().MakeRevolutionFullOx (shape2d_);
		if (solid.IsNull () == false)
			shape3d_ = solid;
	}

	void TorusBuilder::Build2d ()
	{
		shape2d_ = TopoDS_Face ();

		const auto& tp = torusParameters_;

		GeometryTool geometryTool;

		TopoDS_Face face = geometryTool.MakeCircle2dOnAxe ( tp.origin, tp.length, tp.leftRadius, 
			tp.rightRadius, tp.radius, tp.xCenter, tp.yCenter, tp.curvature == TorusCurvature::Convex ? true : false );
		
		if (face.IsNull () == false)
		{
			shape2d_ = face;
		}

	}

}