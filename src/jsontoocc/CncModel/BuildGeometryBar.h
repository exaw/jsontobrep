
#pragma once


#include <PrimitiveParameters/ModelParameters.h>

#include "GeometryBar.h"


namespace RF
{
	GeometryBarPtr BuildGeometryBar (const ModelParametersPtr& modelParameters);

	
}