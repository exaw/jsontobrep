
#include "BuildGeometryBar.h"
#include <builder/FeaturedPrimitiveBuilder.h>

#include <geometry/GeometryTool.h>

namespace RF
{

	RF::GeometryBarPtr BuildGeometryBar (const ModelParametersPtr& modelParameters)
	{
		if (modelParameters == nullptr)
			return nullptr;

		GeometryBarPtr geometryBar = std::make_shared<GeometryBar> ();
		for (const auto& primitive : modelParameters->innerContour)
		{
			FeaturedPrimitiveBuilder builder  (primitive);
			TopoDS_Solid geometry3d = builder.GetGeometry3d ();
			TopoDS_Face geometry2d = builder.GetGeometry2d ();

			geometryBar->AddInnerGeometry ({ geometry3d, geometry2d });
		}

		for (const auto& primitive : modelParameters->outerContour)
		{
			FeaturedPrimitiveBuilder builder (primitive);
			TopoDS_Solid geometry3d = builder.GetGeometry3d ();
			TopoDS_Face geometry2d = builder.GetGeometry2d ();
			geometryBar->AddOuterGeometry ({ geometry3d, geometry2d });

		}

		return geometryBar;
	}

}