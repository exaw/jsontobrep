
#include "GeometryBar.h"

#include <geometry/GeometryTool.h>

namespace RF
{

	GeometryBar::GeometryBar (const GeometryContour& outerContour, const GeometryContour& innerContour) :
		outerContour_ (outerContour), innerContour_ (innerContour)

	{

	}

	GeometryBar::GeometryBar ()
	{

	}

	TopoDS_Shape GeometryBar::GetGeometry3d () const
	{
		GeometryTool geometryTool;
		TopoDS_Shape innerContourShape = geometryTool.MakeFuse (innerContour_.contour3d);
		TopoDS_Shape outerContourShape = geometryTool.MakeFuse (outerContour_.contour3d);
		TopoDS_Shape barShape = geometryTool.MakeCut (outerContourShape, innerContourShape);
		return barShape;
	}

	TopoDS_Shape GeometryBar::GetGeometry2d () const
	{
		GeometryTool geometryTool;
		TopoDS_Shape innerContourShape = geometryTool.MakeFuse (innerContour_.contour2d);
		TopoDS_Shape outerContourShape = geometryTool.MakeFuse (outerContour_.contour2d);
		TopoDS_Shape barShape = geometryTool.MakeCut (outerContourShape , innerContourShape );

		return barShape;

	}

	void GeometryBar::AddInnerGeometry (const std::pair<TopoDS_Solid, TopoDS_Face>& geometries)
	{
		innerContour_.contour2d.push_back (geometries.second);
		innerContour_.contour3d.push_back (geometries.first);

	}

	void GeometryBar::AddOuterGeometry (const std::pair<TopoDS_Solid, TopoDS_Face>& geometries)
	{
		outerContour_.contour2d.push_back (geometries.second);
		outerContour_.contour3d.push_back (geometries.first);
	}

}