
#pragma once

#include <memory>
#include <vector>

#include <TopoDS_Solid.hxx>
#include <TopoDS_Face.hxx>

namespace RF
{

	struct GeometryContour
	{
		std::vector<TopoDS_Solid> contour3d;
		std::vector<TopoDS_Face>  contour2d;
	};

	class GeometryBar
	{
	public:
		GeometryBar ();
		GeometryBar (const GeometryContour& outerContour, const GeometryContour& innerContour);

		TopoDS_Shape GetGeometry3d () const;
		TopoDS_Shape GetGeometry2d () const;

		void AddInnerGeometry ( const std::pair<TopoDS_Solid, TopoDS_Face>& geometries );
		void AddOuterGeometry (const std::pair<TopoDS_Solid, TopoDS_Face>& geometries);


	private:
		
		GeometryContour outerContour_;
		
		GeometryContour innerContour_;
	
	};

	using GeometryBarPtr = std::shared_ptr<GeometryBar>;
}