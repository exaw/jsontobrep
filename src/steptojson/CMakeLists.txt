
cmake_minimum_required(VERSION 3.5)


project(rapidfactory_jsontoocc)

set (CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories (./)

include_directories (/usr/include/opencascade)
include_directories (F:/Projects/library/opencascade-7.2.0/install/inc)
link_directories (F:/Projects/library/opencascade-7.2.0/install/win32/vc14/lib)

include_directories (F:/Projects/library/boost_1_66_0/install/include/boost-1_66)
link_directories (F:/Projects/library/boost_1_66_0/install/lib)

# Boost
#FIND_PACKAGE( Boost 1.30 COMPONENTS program_options REQUIRED )
#INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIR} )

add_executable ( steptojson  
    src/main.cpp
    src/RapidfactureJSONExport.cpp
    src/STEPFeatureDetector.cpp
    src/STEPMetadata.cpp
    src/Utils.cpp
    src/Elements.cpp
    src/GeometryUtils.cpp
    src/Exceptions.cpp
    src/Topology.cpp
)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=return-type -ggdb")
    target_link_libraries (step2json stdc++fs)
endif()

target_link_libraries ( steptojson
                        TKernel
                        TKSTEP
                        TKXSBase
                        TKTopAlgo
                        TKMesh
                        TKBRep
                        TKMath
                        TKGeomBase
                        TKStd
                        TKLCAF
                        TKXCAF
                        TKSTL
                        TKXDESTEP
                        TKXmlXCAF
                        TKFeat
                        TKPrim
                        TKGeomAlgo
                        TKG3d
                        TKG2d
                        TKShHealing
                        TKSTEPAttr
                        TKSTEPBase
                        #${Boost_LIBRARIES}
                        )

#add_custom_target(dist COMMAND python3 utils/package.py DEPENDS step2json COMMENT "Creating distribution package in dist directory...")
