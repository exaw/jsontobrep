#pragma once
#include <vector>
#include <gp_Ax1.hxx>

using namespace std;

template<typename T>
struct Split {
    std::vector<T> Yes;
    std::vector<T> No;
};

template<typename T>
Split<T> SplitByCoaxialityOrReverseCoaxiality(const std::vector<T>& elements, const gp_Ax1& axis) {
    Split<T> ret;
    for(const T& elem: elements) {
        if (IsCoaxialOrReversedCoaxial(axis, elem.Axis())) {
            ret.Yes.push_back(elem);
        } else {
            ret.No.push_back(elem);
        }
    }
    return ret;
}