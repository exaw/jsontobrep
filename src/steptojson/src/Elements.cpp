#include "Elements.hpp"
#include "Utils.hpp"
#include "GeometryUtils.hpp"

gp_Pnt FeatureElement::Location() const { return Axis().Location(); }

bool FeatureElement::ValidateCoaxiality(gp_Ax1 axis, bool oppositeOK) {
    if (Axis().IsCoaxial(axis, linearTolerance, angularTolerance)) {
        return true;
    }
    return oppositeOK && Axis().Reversed().IsCoaxial(axis, linearTolerance, angularTolerance);
}

bool FeatureElement::IsCoaxialTo(const FeatureElement &other) {
    return Axis().IsCoaxial(other.Axis(), angularTolerance, linearTolerance);
}

bool FeatureElement::IsCoaxialOrReversedCoaxial(const gp_Ax1 &otherAxis) {
    return Axis().IsCoaxial(otherAxis, angularTolerance, linearTolerance) ||
           Axis().IsCoaxial(otherAxis.Reversed(), angularTolerance, linearTolerance);
}

gp_Ax1 PlanarElement::Axis() const { return axis; }

// One of the center points for the width / height-corresponding edges
gp_Pnt RectangleElement::WidthAxisCenter() { return MiddlePoint(pointsOnWidthAxis[0], pointsOnWidthAxis[1]); }
gp_Pnt RectangleElement::HeightAxisCenter() { return MiddlePoint(pointsOnHeightAxis[0], pointsOnHeightAxis[1]); }

gp_Ax1 RectangleElement::WidthAxis() {
    return gp_Ax1(pointsOnWidthAxis[0], gp_Vec(pointsOnWidthAxis[0], pointsOnWidthAxis[1]));
}
gp_Ax1 RectangleElement::HeightAxis() {
    return gp_Ax1(pointsOnHeightAxis[0], gp_Vec(pointsOnHeightAxis[0], pointsOnHeightAxis[1]));
}

double RectangleElement::Width() { return pointsOnWidthAxis[0].Distance(pointsOnWidthAxis[1]); }
double RectangleElement::Height() { return pointsOnHeightAxis[0].Distance(pointsOnHeightAxis[1]); }

RectangleElement::RectangleElement(const gp_Ax1 &ax) { this->axis = ax; }

string RectangleElement::ToString() const {
    json j;
    j["w"] = width;
    j["h"] = height;
    j["center"] = PointToJSON(axis.Location());
    j["normal"] = DirectionToJSON(axis.Direction());
    return j.dump();
}

// axis: Normal or antinormal of plane, point is center of circle
CircleElement::CircleElement(const gp_Ax1& axis, double diameter) : diameter(diameter) { this->axis = axis; }

string CircleElement::ToString() const { return "CircleElement " + this->ToJSON(); }

string CircleElement::ToJSON() const {
    json j;
    j["diameter"] = diameter;
    j["description"] = description;
    j["position"] = PointToJSON(axis.Location());
    j["normal"] = DirectionToJSON(axis.Direction());
    return j.dump();
}

bool VolumetricElement::operator==(const VolumetricElement &other) {
    if (this->Type() != other.Type()) {
        return false;
    }
    return this->IsEqual(other);
}

void VolumetricElement::AddAllFeatures(json &j) const {
    for (const auto &feature : leftFeatures) {
        feature->ToRapidfactureJSON(j["left"]["feature"]);
    }
    for (const auto &feature : middleFeatures) {
        feature->ToRapidfactureJSON(j["middle"]["feature"]);
    }
    for (const auto &feature : rightFeatures) {
        feature->ToRapidfactureJSON(j["right"]["feature"]);
    }
}

void VolumetricElement::AddFeature(const ExtraFeaturePtr& feature, PrimitivePlacement placement) {
    if(placement == PrimitivePlacement::Left) {
        leftFeatures.push_back(feature);
    } else if (placement == PrimitivePlacement::Right) {
        rightFeatures.push_back(feature);
    } else if (placement == PrimitivePlacement::Middle) {
        middleFeatures.push_back(feature);
    } else {
        throw std::invalid_argument("Invalid placement");
    }
}

bool VolumetricElement::Contains(const VolumetricElement &elem) const {
    return this->bound1 <= elem.bound1 && elem.bound2 <= this->bound2 &&this->Volume() > elem.Volume();
}

gp_Ax1 VolumetricElement::Axis() const {
    gp_Vec vec(bound1, bound2);
    gp_Vec direction(vec);
    return gp_Ax1(bound1, direction);
}

double VolumetricElement::Length() const {
    // Assumption: bound1 and bound2 on axis, which is validated in Validate()
    return bound1.Distance(bound2);
}

bool VolumetricElement::operator<(const VolumetricElement &other) { return this->bound1 < other.bound1; }

double PolygonalElement::CircumscribedDiameter() const { return 2 * (radius / cos(M_PI / sides)); }

double PolygonalElement::AngleBetweenSides() const { return 360.0 / sides; }

VolumetricElement::VolumetricType PolygonalElement::Type() const { return VolumetricElement::VolumetricType::Polygon; }

PolygonalElement::PolygonalElement(unsigned int sides, const gp_Pnt &bound1, const gp_Pnt &bound2, double radius,
                                   bool isHole)
    : sides(sides), radius(radius) {
    this->isHole = isHole;
    this->bound1 = bound1;
    this->bound2 = bound2;
}

void PolygonalElement::Normalize() {
    // Either bound1 > bound2 or swap bounds
    if (bound2 < bound1) {
        // Swap bound points
        std::swap(bound1, bound2);
    }
}

bool PolygonalElement::IsEqual(const VolumetricElement &otherSuperclass) const {
    const PolygonalElement *other = dynamic_cast<const PolygonalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (!bound1.IsEqual(other->bound1, linearTolerance)) {
        return false;
    }
    if (!bound2.IsEqual(other->bound2, linearTolerance)) {
        return false;
    }
    if (sides != other->sides) {
        return false;
    }
    if (abs(other->radius - this->radius) > linearTolerance) {
        return false;
    }
    // NOTE: isHole equality is not checked any more because if there is a hole with equivalent parameters,
    // then this is an error and the non-hole should be removed
    return true;
}

bool PolygonalElement::IsEqualIgnoreLocation(const VolumetricElement &otherSuperclass) const {
    const PolygonalElement *other = dynamic_cast<const PolygonalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (this->sides != other->sides) {
        return false;
    }
    if(abs(this->Length() - other->Length()) > linearTolerance) {
        return false;
    }
    if (abs(this->radius - other->radius) > linearTolerance) {
        return false;
    }
    return true;
}

string PolygonalElement::ToString() const { return "PolygonalElement " + this->ToJSON(); }

string PolygonalElement::ToJSON() const {
    json j;
    j["sides"] = sides;
    j["radius"] = radius;
    j["description"] = description;
    j["start"] = PointToJSON(bound1);
    j["end"] = PointToJSON(bound2);
    j["isHole"] = isHole;
    return j.dump();
}

json PolygonalElement::ToRapidfactureJSON(PrimitivePlacement placement) const {
    json j;
    j["type"] = "cylinder";
    assert(placement != PrimitivePlacement::Middle);
    if (placement != PrimitivePlacement::Nothing) {
        j["placement"] = (placement == PrimitivePlacement::Left ? "left" : "right");
    }
    // left
    j["left"]["dimension"]["value"] = CircumscribedDiameter();
    // middle
    j["middle"]["dimension"]["value"] = Length();
    j["middle"]["dimension"]["linkToElement"] = 0;
    j["middle"]["feature"]["mill"]["description"] = std::to_string(sides) + "-polygon";
    j["middle"]["feature"]["mill"]["centerToPlaneDimension"]["value"] = radius;
    j["middle"]["feature"]["mill"]["circularPattern"]["startAngle"] = 0; // TODO implement
    j["middle"]["feature"]["mill"]["circularPattern"]["angle"] = AngleBetweenSides();
    j["middle"]["feature"]["mill"]["circularPattern"]["number"] = sides;
    // right
    j["right"]["dimension"]["value"] = CircumscribedDiameter();
    // meta
    j["meta"]["description"] = description;
    AddAllFeatures(j);
    return j;
}

double PolygonalElement::Volume() const {
    // Area * length
    // https://www.mathopenref.com/polygonregulararea.html - apothem given
    double area = radius * radius * sides * tan(M_PI / sides);
    return (isHole ? -1 : 1) * Length() * area;
}

ConicalElement::ConicalElement(const gp_Pnt &bound1, const gp_Pnt &bound2, double diameter1, double diameter2,
                               bool isHole)
    : diameter1(diameter1), diameter2(diameter2) {
    this->isHole = isHole;
    this->bound1 = bound1;
    this->bound2 = bound2;
}

VolumetricElement::VolumetricType ConicalElement::Type() const { return VolumetricElement::VolumetricType::Conic; }

/**
 * If bound1 > bound2  according to some arbitrary predefined proper relation,
 * swap the bounds
 */
void ConicalElement::Normalize() {
    // Either bound1 > bound2 or swap bounds
    if (bound2 < bound1) {
        // Swap bound points and diameters (relevant for conics)
        std::swap(bound1, bound2);
        std::swap(diameter1, diameter2);
    }
}

bool ConicalElement::IsCylinder() const { return abs(diameter1 - diameter2) < linearTolerance; }

bool ConicalElement::IsTruncatedCone() const {
    return (abs(diameter1) < linearTolerance) || (abs(diameter2) < linearTolerance);
}

bool ConicalElement::Validate() const {
    if (Length() < linearTolerance) {
        cerr << "Cylinder length is zero or negative: " << Length() << endl;
        return false;
    }
    if (diameter1 < -linearTolerance) {
        cerr << "Cylinder diameter1 is negative: " << diameter1 << endl;
        return false;
    }
    if (diameter2 < -linearTolerance) {
        cerr << "Cylinder diameter2 is negative: " << diameter2 << endl;
        return false;
    }
    return true;
}

bool ConicalElement::IsEqual(const VolumetricElement &otherSuperclass) const {
    const ConicalElement *other = dynamic_cast<const ConicalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (!Axis().IsCoaxial(other->Axis(), angularTolerance, linearTolerance)) {
        return false;
    }
    if (abs(other->diameter1 - this->diameter1) > linearTolerance) {
        return false;
    }
    if (abs(other->diameter2 - this->diameter2) > linearTolerance) {
        return false;
    }
    if (this->bound1.Distance(other->bound1) > linearTolerance) {
        return false;
    }
    if (this->bound2.Distance(other->bound2) > linearTolerance) {
        return false;
    }
    // NOTE: isHole equality is not checked any more because if there is a hole with equivalent parameters,
    // then this is an error and the non-hole should be removed
    return true;
}

bool ConicalElement::IsEqualIgnoreLocation(const VolumetricElement &otherSuperclass) const {
    const ConicalElement *other = dynamic_cast<const ConicalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (abs(this->Length() - other->Length()) > linearTolerance) {
        return false;
    }
    if (abs(this->diameter1 - other->diameter1) > linearTolerance) {
        return false;
    }
    return true;
}

// For sorting
bool ConicalElement::operator<(const ConicalElement &other) const {
    if (bound1 < other.bound1) {
        return true;
    } else if (bound1 > other.bound1) {
        // Definitely not less than as first criterion is greater than
        return false;
    } else if (bound2 < other.bound2) {
        return true;
    } else {
        return isHole < other.isHole;
    }
}

string ConicalElement::ToString() const { return (IsCylinder() ? "Cylinder " : "Cone ") + this->ToJSON(); }

string ConicalElement::ToJSON() const {
    json j;
    j["description"] = description;
    if(IsCylinder()) {
        j["diameter"] = diameter1;
    } else { // Cone
        j["diameters"] = {diameter1, diameter2};
    }
    j["start"] = PointToJSON(bound1);
    j["end"] = PointToJSON(bound2);
    j["isHole"] = isHole;
    j["length"] = Length();
    if (leftFeatures.size()) {
        j["leftFeatures"] = leftFeatures.size();
    } else if (middleFeatures.size()) {
        j["middleFeatures"] = middleFeatures.size();
    } else if (rightFeatures.size()) {
        j["rightFeatures"] = rightFeatures.size();
    }
    return j.dump();
}

json ConicalElement::ToRapidfactureJSON(PrimitivePlacement placement) const {
    json j;
    j["type"] = "cylinder";
    assert(placement != PrimitivePlacement::Middle);
    if (placement != PrimitivePlacement::Nothing) {
        j["placement"] = (placement == PrimitivePlacement::Left ? "left" : "right");
    }
    j["left"]["dimension"]["value"] = diameter1;
    j["middle"]["dimension"]["value"] = Length();
    j["middle"]["dimension"]["linkToElement"] = 0;
    j["right"]["dimension"]["value"] = diameter2;
    j["meta"]["description"] = description;
    if (!IsCylinder()) { // Is cone
        j["type"] = "cone";
        j["meta"]["dimensionType"] = "diameters";
        j["meta"]["rightSide"] = false; // ?
    }
    AddAllFeatures(j);
    return j;
}

double ConicalElement::Volume() const {
    // https://de.wikipedia.org/wiki/Kegelstumpf#Volumen
    double smallRadius = diameter1 < diameter2 ? diameter1 / 2.0 : diameter2 / 2.0;
    double largeRadius = diameter2 < diameter1 ? diameter1 / 2.0 : diameter2 / 2.0;
    assert(smallRadius <= largeRadius);
    double R = largeRadius - smallRadius;
    assert(R > -linearTolerance); // If this fails, the small/large radius is swapped
    double term1 = (Length() * M_PI) / 3.0;
    double term2 = (R * R + R * smallRadius + smallRadius * smallRadius);
    return (isHole ? -1 : 1) * term1 * term2;
}

ToroidalElement::ToroidalElement(const gp_Pnt &bound1, double radius1, const gp_Pnt &bound2, double radius2,
                                 double majorRadius, double minorRadius, bool isConvex)
    : radius1(radius1), radius2(radius2), majorRadius(majorRadius), minorRadius(minorRadius), isConvex(isConvex) {
    this->bound1 = bound1;
    this->bound2 = bound2;
}

double ToroidalElement::Volume() const {
    // TODO this is not the correct volume, but most likely thats irrelevant.
    // We "estimate" the radius by instead using a cone with the same radii
    // Why? because it difficult to calculate in general
    ConicalElement cone(bound1, bound2, radius1 * 2.0, radius2 * 2.0, isHole);
    return cone.Volume();
}

VolumetricElement::VolumetricType ToroidalElement::Type() const { return VolumetricElement::VolumetricType::Toroid; }

void ToroidalElement::Normalize() {
    // Either bound1 > bound2 or swap bounds
    if (bound2 < bound1) {
        std::swap(bound1, bound2);
        std::swap(radius1, radius2);
    }
}

void ToroidalElement::Validate() {
    // Anything to do here?
}

bool ToroidalElement::IsEqual(const VolumetricElement &otherSuperclass) const {
    const ToroidalElement *other = dynamic_cast<const ToroidalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (!bound1.IsEqual(other->bound1, linearTolerance)) {
        return false;
    }
    if (!bound2.IsEqual(other->bound2, linearTolerance)) {
        return false;
    }
    if (abs(radius1 - other->radius1) > linearTolerance) {
        return false;
    }
    if (abs(radius2 - other->radius2) > linearTolerance) {
        return false;
    }
    if (abs(minorRadius - other->minorRadius) > linearTolerance) {
        return false;
    }
    if (abs(centerCoordinateAxial - other->centerCoordinateAxial) > linearTolerance) {
        return false;
    }
    if (abs(centerCoordinateOrthogonal - other->centerCoordinateOrthogonal) > linearTolerance) {
        return false;
    }
    if (isConvex != other->isConvex) {
        return false;
    }
    // NOTE: isHole equality is not checked any more because if there is a hole with equivalent parameters,
    // then this is an error and the non-hole should be removed
    return true;
}

bool ToroidalElement::IsEqualIgnoreLocation(const VolumetricElement &otherSuperclass) const {
    const ToroidalElement *other = dynamic_cast<const ToroidalElement *>(&otherSuperclass);
    if (other == nullptr) { // Cast failed ; not the same class
        return false;
    }
    if (abs(this->Length() - other->Length()) > linearTolerance) {
        return false;
    }
    if (abs(this->radius1 - other->radius1) > linearTolerance) {
        return false;
    }
    if (abs(this->radius2 - other->radius2) > linearTolerance) {
        return false;
    }
    if (abs(this->minorRadius - other->minorRadius) > linearTolerance) {
        return false;
    }
    if (abs(this->majorRadius - other->majorRadius) > linearTolerance) {
        return false;
    }
    if (this->isConvex != other->isConvex) {
        return false;
    }
    // TODO do we need to check centerCoordinateAxial and centerCoordinateOrthogonal ??
    return true;
}

string ToroidalElement::ToString() const { return "ToroidalElement " + this->ToJSON(); }

string ToroidalElement::ToJSON() const {
    json j;
    // j["bound1"] = PointToJSON(Axis().Location());
    j["axialRadii"] = {radius1, radius2};
    j["torusRadii"] = {majorRadius, minorRadius};
    j["description"] = description;
    j["length"] = Length();
    j["start"] = PointToJSON(bound1);
    j["isHole"] = isHole;
    j["isConvex"] = isConvex;
    return j.dump(); // Not prettified
}

json ToroidalElement::ToRapidfactureJSON(PrimitivePlacement placement) const {
    json j;
    j["type"] = "circle";
    assert(placement != PrimitivePlacement::Middle);
    if (placement != PrimitivePlacement::Nothing) {
        j["placement"] = (placement == PrimitivePlacement::Left ? "left" : "right");
    }
    // End circles
    j["left"]["dimension"]["value"] = radius1 * 2.0;
    j["right"]["dimension"]["value"] = radius2 * 2.0;
    //
    j["middle"]["dimension"]["value"] = Length();
    j["middle"]["dimension"]["linkToElement"] = 0;
    j["middle"]["radius"]["value"] = minorRadius;
    j["middle"]["radius"]["curvature"] = isConvex ? -1 : 1;
    // Compute center point. It is apparently relative
    j["middle"]["radius"]["x"]["value"] = centerCoordinateOrthogonal;
    j["middle"]["radius"]["z"]["value"] = centerCoordinateAxial;

    j["meta"]["description"] = description;
    AddAllFeatures(j);
    return j;
}

bool CompareIgnoreLocation(const VolumetricElementCollection &a, const VolumetricElementCollection &b) {
    if(a.size() != b.size()) {
        return false;
    }
    for (size_t i = 0; i < a.size(); i++)
    {
        if(!a[i]->IsEqualIgnoreLocation(*b[i])) {
            return false;
        }
    }
    return true;
}

std::ostream &operator<<(std::ostream &os, PrimitivePlacement placement) {
    switch(placement) {
        case PrimitivePlacement::Nothing: {
            os << "PrimitivePlacement::Nothing";
            break;
        }
        case PrimitivePlacement::Left: {
            os << "PrimitivePlacement::Left";
            break;
        }
        case PrimitivePlacement::Right: {
            os << "PrimitivePlacement::Right";
            break;
        }
        case PrimitivePlacement::Middle: {
            os << "PrimitivePlacement::Middle";
            break;
        }
        default: {
            os << "PrimitivePlacement::Unknown";
            break;
        }
    }
    return os;
}

void HoleCircle::ToRapidfactureJSON(json& features) const {
    json j1;
    // Build contour list
    for(const auto& elem : structure) {
        j1["contour"].push_back(elem->ToRapidfactureJSON());
    }
    j1["circularPattern"]["diameter"] = diameter;
    j1["circularPattern"]["startAngle"] = startAngle;
    j1["circularPattern"]["angle"] = angleInterval;
    j1["circularPattern"]["number"] = number;

    features["boring"].push_back(j1);
}