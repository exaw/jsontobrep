#include "Topology.hpp"
#include <BRepTopAdaptor_FClass2d.hxx>
#include <BRep_Builder.hxx>
#include <BRep_Tool.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Wire.hxx>

bool IsFacePartOfHole(const TopoDS_Face &face) {
    // Use face tolerance
    Standard_Real tolerance = BRep_Tool::Tolerance(face);
    TopLoc_Location Loc;
    auto Surf = BRep_Tool::Surface(TopoDS::Face(face), Loc);
    for (TopExp_Explorer expw(face, TopAbs_WIRE); expw.More(); expw.Next()) {
        const TopoDS_Wire &wire = TopoDS::Wire(expw.Current());
        TopoDS_Face newFace;
        BRep_Builder BB;
        BB.MakeFace(newFace, Surf, Loc, tolerance);
        BB.Add(newFace, wire);
        BRepTopAdaptor_FClass2d aClass2d(newFace, tolerance);
        // Identify
        TopAbs_State state = aClass2d.PerformInfinitePoint();
        return state == TopAbs_IN;
    }
    return false;
}

TopoDS_Wire InnerWire(const TopoDS_Face &face) {
    // Use face tolerance
    Standard_Real tolerance = BRep_Tool::Tolerance(face);
    TopLoc_Location Loc;
    auto Surf = BRep_Tool::Surface(TopoDS::Face(face), Loc);

    TopoDS_Wire resultWire;
    TopExp_Explorer expw(face, TopAbs_WIRE);

    if (expw.More()) {
        resultWire = TopoDS::Wire(expw.Current());
        expw.Next();
        if (expw.More()) {
            while (expw.More()) {
                const TopoDS_Wire &wire = TopoDS::Wire(expw.Current());
                // Check if its an inner wire
                TopoDS_Face newFace;
                BRep_Builder BB;
                BB.MakeFace(newFace, Surf, Loc, tolerance);
                BB.Add(newFace, wire);
                BRepTopAdaptor_FClass2d aClass2d(newFace, tolerance);
                TopAbs_State state = aClass2d.PerformInfinitePoint();
                if (state == TopAbs_IN) {
                    resultWire = wire;
                }
                expw.Next();
            }
        }
    }
    return resultWire;
}