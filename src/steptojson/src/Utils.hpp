#pragma once

#include "common.hpp"
#include "lib/json.hpp"

#include <IFSelect_ReturnStatus.hxx>
#include <TopoDS.hxx>
#include <cassert>
#include <ctime>
#include <gp_Dir.hxx>
#include <iomanip>
#include <string>

#include <boost/optional.hpp>

using json = nlohmann::json;


typedef vector<gp_Lin> GPLineVector;
typedef vector<gp_Circ> GPCircVector;
typedef vector<vector<gp_Pnt> > InterpolatedBSplineVector;

#define RAD_TO_DEG(v) ((v)*180 / M_PI)

/**
 * https://stackoverflow.com/a/26221725/2597135
 */
template <typename... Args> std::string string_format(const std::string &format, Args... args) {
    size_t size = snprintf(nullptr, 0, format.c_str(), args...) + 1; // Extra space for '\0'
    unique_ptr<char[]> buf(new char[size]);
    snprintf(buf.get(), size, format.c_str(), args...);
    return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}

string XYZToString(const gp_XYZ &xyz);

json XYZToJSON(const gp_XYZ &xyz);

string DirectionToString(const gp_Dir &dir);

json DirectionToJSON(const gp_Dir &dir);

string PointToString(const gp_Pnt &pnt);

json PointToJSON(const gp_Pnt &pnt);

gp_Pnt MiddlePoint(const gp_Pnt &p1, const gp_Pnt &p2);

std::string GetCurrentDate();

const char* CompoundTypeToString(TopAbs_ShapeEnum type);

const char* IFSelectReturnStatusToString(IFSelect_ReturnStatus code);

/**
 * Get the "single element" from the given vector if ALL elements in the vector
 * are withing the given tolerance.
 * If there are no values in the vector or at least one of the values is not
 * within tolerance to the first one, return no value.
 */
boost::optional<double> GetSingleElement(const vector<double> &v, double tolerance);

std::ostream &operator<<(std::ostream &os, const gp_XY &xy);
std::ostream &operator<<(std::ostream &os, const gp_Pnt2d &xy);
std::ostream &operator<<(std::ostream &os, const gp_Dir2d &xy);
