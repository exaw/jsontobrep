#pragma once
#include <STEPControl_Reader.hxx>
#include <StepData_StepModel.hxx>
#include <map>
#include <string>
#include <vector>

using namespace std;

/**
 * Extract label for the entire file
 */
std::string ParseSTEPFileLabel(Handle(StepData_StepModel) model);

/**
 * Get labels for individual files
 */
std::vector<std::pair<TopoDS_Shape, std::string>> ExtractTopologyToNameMap(STEPControl_Reader &reader);
