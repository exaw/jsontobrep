#pragma once
#include <map>
#include <set>
#include <vector>
#include <gp_Pnt.hxx>

using namespace std;

/**
 * Result of a grouping operation with separate key storage and index-based access.
 */
template <typename T1, typename T2> struct GroupResult {
    multimap<T1, T2> m;
    set<T1> keys;

    void Insert(const T1 &k, const T2 &v) {
        keys.insert(k);
        m.insert(std::make_pair(k, v));
    }

    /**
     * Number of unique keys
     */
    size_t Size() const { return keys.size(); }

    /**
     * Total number of keys
     */
    size_t TotalSize() const { return m.size(); }

    /**
     * Get the first value for a given key
     */
    const T2 &FirstValue(const T1 &key) const {
        if (m.count(key) < 1) {
            throw std::out_of_range("No such key");
        }
        return m.lower_bound(key)->second;
    }

    /**
     * Get all values for a single key.
     * Copies the values into the vector
     */
    std::vector<T2> Values(const T1 &key) const {
        vector<T2> ret; 
        ret.reserve(m.count(key));
        for (auto it = m.lower_bound(key); it != m.upper_bound(key); it++) {
            ret.emplace_back(it->second);
        }
        return ret;
    }

    /**
     * Get all values for a single key.
     * Returns a vector
     */
    std::vector<const T2 *> ValuePointers(const T1 &key) const {
        vector<const T2 *> ret;
        ret.reserve(m.count(key));
        for (auto it = m.lower_bound(key); it != m.upper_bound(key); it++) {
            ret.emplace_back(&it->second);
        }
        return ret;
    }

    /**
     * Get the nth key
     * NOTE: Index based accessed is not neccessarily deterministic across equivalent instances
     */
    const T1 &operator[](std::size_t idx) const {
        if (keys.size() <= idx) {
            throw std::out_of_range("Not enough keys to get this index");
        }
        auto it = keys.begin();
        std::advance(it, idx);
        return *it;
    }
};

/**
 * Group by .Location().
 * Use e.g. with a vector<gp_Circ>
 */
template <typename T>
GroupResult<gp_Pnt, T> GroupByLocation(const vector<T> &entities) {
    GroupResult<gp_Pnt, T> ret; 
    for (auto &entity : entities) {
        ret.Insert(entity.Location(), entity);
    }
    return ret;
}

/**
 * Group by predefined key-value pairs
 * Use e.g. with a vector<pair<gp_Pnt, gp_Circ>>
 */
template <typename T1, typename T2>
GroupResult<T1, T2> GroupByPair(const vector<pair<T1, T2> > &entities) {
    GroupResult<T1, T2> ret;
    for (const auto &entity : entities) {
        ret.Insert(entity.first, entity.second);
    }
    return ret;
}
