#pragma once
#include "Elements.hpp"
#include "RapidfactureJSONExport.hpp"
#include "Topology.hpp"
#include "Utils.hpp"
#include "common.hpp"
#include <Geom_ConicalSurface.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_ToroidalSurface.hxx>
#include <STEPControl_Reader.hxx>
#include <StepData_StepModel.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Solid.hxx>
#include <gp_Dir.hxx>
#include <gp_Vec.hxx>
#include <string>
#include <vector>

using namespace std;

class STEPFeatureDetector {
  public:
    std::string filename;
    TopoDS_Solid solid;
    STEPControl_Reader reader;
    // Cylinders, volumetric polygons
    VolumetricElementCollection volumetricElements;
    // Elements which are not required for the import but should still be counted as processed
    // (e.g. coaxial planes, e.g. "planar ends")
    FeatureElementCollection ignoredElements;
    // Rectangles, circles, ...
    PlanarElementCollection planarElements;
    Handle(StepData_StepModel) model;
    std::string label; // For entire solid
    // Maps labels from STEP to the shape
    std::vector<std::pair<TopoDS_Shape, std::string>> topoNameMap;
    RFJSONExportData exportData;
    // Faces and a bitset showing if a face could be converted properly
    std::vector<TopoDS_Face> allFaces;
    // For radial structures (polygons & hole circles), the 0° reference.
    // Set arbitrarily once, but then kept for the whole part.
    bool have0DegreeReferenceVector;
    gp_Vec zeroDegreeReferenceVector;

    STEPFeatureDetector(const std::string &filename);

    void ExportRFJSON(const std::string &filename);
    /**
     * Export ONE STL containing all faces, 
     * ONE STL containg processed faces and ONE STL containing unprocessed faces
     */
    void ExportSTL(const std::string& prefix);

  private:

    void Parse(const TopoDS_Solid &solid);

    void HandleCylindricalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_CylindricalSurface) csurf,
                                  const std::string &label);

    void HandlePlane(int faceIdx, const TopoDS_Face &face, Handle(Geom_Plane) plane, const std::string &label);

    void HandleToroidalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_ToroidalSurface) tsurf, const std::string &label);

    void HandleConicalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_ConicalSurface) geomCone, const std::string &label);
    /**
     * Get the label for a given shape.
     * @return the label or "" if no label or a NONE label has been found
     */
    const char *GetLabelForShape(const TopoDS_Shape &shape);

    void IterateFaces(const TopoDS_Solid &solid);

    void PrintElements(const VolumetricElementCollection& elements);

    /**
     * Use the existing 0 degree reference vector or (if none is set)
     * update it
     */
    gp_Vec GetOrUpdateZeroDegreeReferenceVector(const gp_Vec& newVec);

    /**
     * Use the existing 0 degree reference vector or (if none is set)
     * update it from the given point projected orthogonally onto the given axis
     */
    gp_Vec GetOrUpdateZeroDegreeReferenceVector(const gp_Pnt &pnt, const gp_Ax1& axis);

    void ProcessCoaxialityClasses();

    void RecognizePolygons(const gp_Ax1& currentAxis);

    // Extract rectangle elements from planarElements
    std::vector<shared_ptr<RectangleElement>> GetRectangles();

    
};
