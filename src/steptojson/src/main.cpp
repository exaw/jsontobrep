
#include <iostream>
#include <string>
#include <cctype>
#include <typeinfo>

#include <boost/program_options.hpp>

#include "STEPFeatureDetector.hpp"
#include "Exceptions.hpp"

using namespace boost::program_options;
using namespace std;

/**
 * Used to skip digits in the front of a typeid().name() result.
 */
const char* SkipDigits (const char* s) {
	while (isdigit (*s)) {
		s++;
	}
	return s;
}

int main (int argc, char **argv) 
{
	/**
	 * Parse command line option
	 */
	string stepFile;
	string jsonFile;
	string stlFile;

	// Declare command line syntax
	options_description desc ("Allowed options");
	desc.add_options ()
		("help,h", "print usage message")("input,i", value (&stepFile), "STEP file to read")(
			"json,j", value (&jsonFile), "Where to write the JSON to")("stl,s", value (&stlFile),
				"Where to write the STL to");

	// Parse
	variables_map vm;
	store (command_line_parser (argc, argv).options (desc).run (), vm);
	notify (vm);
	if (vm.count ("help") || !vm.count ("input")) 
	{
		cout << "Usage: " << argv[0] << " -i <STEP file> [-j <JSON output>] [-s <STL prefix>]" << endl;
		cout << desc << "\n";
		return 0;
	}

	// Read file
	try
	{
		auto detector = STEPFeatureDetector (vm["input"].as<std::string> ());

		if (vm.count ("json")) {
			detector.ExportRFJSON (vm["json"].as<std::string> ());
		}
		if (vm.count ("stl")) {
			detector.ExportSTL (vm["stl"].as<std::string> ());
		}
	}
	catch (FileLevelException& exc)
	{
		cerr << "STEP file read failed: " << SkipDigits (typeid(exc).name ()) << "(" << exc.what () << ")" << endl;
	}
	catch (PartLevelException& exc)
	{
		cerr << "Part analysis failed: " << SkipDigits (typeid(exc).name ()) << "(" << exc.what () << ")" << endl;
	}
}
