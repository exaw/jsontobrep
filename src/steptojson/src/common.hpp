#pragma once
#include <vector>
#include <memory>
#include <gp_Pnt.hxx>
#include <gp_Lin.hxx>
#include <gp_Circ.hxx>

using namespace std;

/**
 * Params
 */
static constexpr Standard_Real linearTolerance = 1e-8; // Unit probably mm
static constexpr Standard_Real angularTolerance = 1e-6; // Unit unknown, probably radiant
