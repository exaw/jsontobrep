#include "GeometryUtils.hpp"
#include <BRepTools_WireExplorer.hxx>
#include <BRep_Tool.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <GCPnts_QuasiUniformAbscissa.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <Geom2dAPI_ProjectPointOnCurve.hxx>
#include <Geom2d_Curve.hxx>
#include <Geom2d_Line.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <Geom_Circle.hxx>
#include <IntAna2d_AnaIntersection.hxx>
#include <ShapeAnalysis.hxx>
#include <Standard_Type.hxx>
#include <TopoDS_Wire.hxx>
#include <cassert>
#include <gp_Lin2d.hxx>

bool IsCoaxialOrReversedCoaxial (const gp_Ax1 &firstAxis, const gp_Ax1 &secondAxis) {
	return firstAxis.IsCoaxial (secondAxis, angularTolerance, linearTolerance) ||
		firstAxis.IsCoaxial (secondAxis.Reversed (), angularTolerance, linearTolerance);
}

bool LineIntersection (const gp_Lin &lin1, const gp_Lin &lin2, gp_Pnt &intersect) {
	Handle (Geom_Line) hline1 = new Geom_Line (lin1);
	Handle (Geom_Line) hline2 = new Geom_Line (lin2);

	auto intersector = GeomAPI_ExtremaCurveCurve (hline1, hline2);

	gp_Pnt intersectB;

	intersector.NearestPoints (intersect, intersectB);
	if (intersect.Distance (intersectB) > linearTolerance) {
		// Lines have no overlapping point within tolerance
		return false;
	}
	return true;
}

bool LineIntersection (const gp_Lin2d &lin1, const gp_Lin2d &lin2, gp_Pnt2d &intersect) {
	auto intersector = IntAna2d_AnaIntersection (lin1, lin2);
	assert (intersector.IsDone ());
	if (intersector.NbPoints () == 0) {
		return false;
	}
	intersect = intersector.Point (0).Value ();
	return true;
}

bool LineIntersection (const gp_Ax1 &lin1, const gp_Ax1 &lin2, gp_Pnt &intersect) {
	return LineIntersection (gp_Lin (lin1), gp_Lin (lin2), intersect);
}

bool LineIntersection (const gp_Ax2d &lin1, const gp_Ax2d &lin2, gp_Pnt2d &intersect) {
	return LineIntersection (gp_Lin2d (lin1), gp_Lin2d (lin2), intersect);
}

bool IsNormal (const gp_Lin &lin1, const gp_Lin &lin2) {
	return lin1.Position ().IsNormal (lin2.Position (), angularTolerance);
}

bool IsParallel (const gp_Lin &lin1, const gp_Lin &lin2) {
	return lin1.Position ().IsParallel (lin2.Position (), angularTolerance);
}

gp_Pnt OrthogonalProjectPointOntoAxis (const gp_Ax1 &ax, const gp_Pnt &pnt) {
	Handle (Geom_Line) hax = new Geom_Line (ax);
	auto projector = GeomAPI_ProjectPointOnCurve (pnt, hax);
	projector.Perform (pnt);
	if (projector.NbPoints () == 0) {
		// TODO use more appropriate exception
		throw std::out_of_range ("Projection failed");
	}
	return projector.NearestPoint ();
}

gp_Pnt2d OrthogonalProjectPointOntoAxis (const gp_Ax2d &ax, const gp_Pnt2d &pnt) {
	Handle (Geom2d_Line) hax = new Geom2d_Line (ax);
	auto projector = Geom2dAPI_ProjectPointOnCurve (pnt, hax);
	if (projector.NbPoints () == 0) {
		// TODO use more appropriate exception
		throw std::out_of_range ("Projection failed");
	}
	return projector.NearestPoint ();
}

RectangleElement *TryParseRectangle (const gp_Dir &direction, GPLineVector &lines) {
	assert (lines.size () == 4); // If not, check caller
	// 2 pairs of lines are parallel if this is a rectangle at all
	// p1l2 = pair1 line2. line1/2 order has no meaning
	gp_Lin &p1l1 = lines[0], p1l2, p2l1, p2l2;
	// Quick identification of the
	if (IsParallel (p1l1, lines[1])) {
		// 0 || 1 ; 2 || 3
		p1l2 = lines[1];
		p2l1 = lines[2];
		p2l2 = lines[3];
	}
	else if (IsParallel (p1l1, lines[2])) {
		// 0 || 2 ; 1 || 3
		p1l2 = lines[2];
		p2l1 = lines[1];
		p2l2 = lines[3];
	}
	else if (IsParallel (p1l1, lines[3])) {
		// 0 || 3 ; 1 || 2
		p1l2 = lines[3];
		p2l1 = lines[1];
		p2l2 = lines[2];
	}
	else {
		return nullptr;
	}
	// Check if we actually have a rectangle
	if (!IsParallel (p1l1, p1l2)) {
		return nullptr;
	}
	else if (!IsParallel (p2l1, p2l2)) {
		return nullptr;
	}
	else if (!IsNormal (p1l1, p2l1)) { // Normal == orthogonal
		return nullptr;
	}
	else if (!IsNormal (p1l1, p2l2)) {
		return nullptr;
	}
	else if (!IsNormal (p1l2, p2l1)) {
		return nullptr;
	}
	else if (!IsNormal (p1l2, p2l2)) {
		return nullptr;
	}
	// Compute corner points
	gp_Pnt corners[4];
	if (!LineIntersection (p1l1, p2l1, corners[0])) {
		return nullptr; // Not intersecting
	}
	if (!LineIntersection (p1l1, p2l2, corners[1])) {
		return nullptr; // Not intersecting
	}
	if (!LineIntersection (p1l2, p2l1, corners[2])) {
		return nullptr; // Not intersecting
	}
	if (!LineIntersection (p1l2, p2l2, corners[3])) {
		return nullptr; // Not intersecting
	}

	// Find long/short edges
	// Corner distances: max (diagonal), "medium" (long edge) ; min (short edge)
	double d1 = corners[0].Distance (corners[1]);
	double d2 = corners[0].Distance (corners[2]);
	double d3 = corners[0].Distance (corners[3]);
	double maxDistance = max (d1, max (d2, d3));
	bool isD1Max = abs (d1 - maxDistance) < linearTolerance;
	bool isD2Max = abs (d2 - maxDistance) < linearTolerance;
	bool isD3Max = abs (d3 - maxDistance) < linearTolerance;
	assert (isD1Max || isD2Max || isD3Max);
	auto secondOpposingCorner = (isD1Max ? corners[1] : isD2Max ? corners[2] : corners[3]);
	// Compute center of rectangle: Middle between the opposing corners
	gp_Pnt center = MiddlePoint (corners[0], secondOpposingCorner);
	// Compute axis from direction
	gp_Ax1 axis (center, direction);
	// Create (not completely filled) rectangle obj
	auto elem = new RectangleElement (axis);
	// Compute width & height (distance between each pair of parallels)
	// Although these are redundant, they are difficult to computer later
	// width/height are interchangable
	double width = p1l1.Distance (p1l2);
	double height = p2l1.Distance (p2l2);
	// First point on axes is always corners[0] by convention
	elem->pointsOnWidthAxis[0] = corners[0];
	elem->pointsOnHeightAxis[0] = corners[0];
	// Compute vectors along the sides
	if (abs (d1 - width) < linearTolerance) {
		elem->pointsOnWidthAxis[1] = corners[1];
	}
	else if (abs (d2 - width) < linearTolerance) {
		elem->pointsOnWidthAxis[1] = corners[2];
	}
	else if (abs (d3 - width) < linearTolerance) {
		elem->pointsOnWidthAxis[1] = corners[3];
	}
	else {
		cerr << "Corner error: Can't find width-corresponding corner set" << endl;
	}
	if (abs (d1 - height) < linearTolerance) {
		elem->pointsOnHeightAxis[1] = corners[1];
	}
	else if (abs (d2 - height) < linearTolerance) {
		elem->pointsOnHeightAxis[1] = corners[2];
	}
	else if (abs (d3 - height) < linearTolerance) {
		elem->pointsOnHeightAxis[1] = corners[3];
	}
	else {
		cerr << "Corner error: Can't find height-corresponding corner set" << endl;
	}
	// Copy corners
	for (size_t i = 0; i < 4; i++) {
		elem->corners[i] = corners[i];
	}
	return elem;
}

bool ArePlanarAnglesPolygonal (const std::vector<double> &angles, int n) {
	std::vector<double> referenceAngles;
	for (size_t i = 0; i < n; i++) {
		// 0..360°
		referenceAngles.push_back (i * (360.0 / n));
	}
	// Need to have the correct number of angles
	if (angles.size () != referenceAngles.size ()) {
		return false;
	}
	// Check difference
	for (size_t i = 0; i < referenceAngles.size (); i++) {
		if (abs (angles[i] - referenceAngles[i]) > angularTolerance) {
			return false;
		}
	}
	return true;
}

FaceSubGeometries SplitFaceSubGeometries (const TopoDS_Face &face, double deflection) {
	FaceSubGeometries ret;
	// Analyze outer wire of face
	auto wire = ShapeAnalysis::OuterWire (face);
	for (BRepTools_WireExplorer wireExplorer (wire); wireExplorer.More (); wireExplorer.Next ()) {
		const TopoDS_Edge &edge = wireExplorer.Current ();
		Standard_Real umin, umax;
		// Get unbounded curve and bounding parameters
		auto edgeCurve = BRep_Tool::Curve (edge, umin, umax);
		// Handle "no curve" case, occurs e.g. for conical surfaces
		if (edgeCurve.IsNull ()) {
			continue;
		}
		// Assign depending on type
		if (edgeCurve->IsKind (STANDARD_TYPE (Geom_Circle))) {
			ret.circles.push_back (Handle (Geom_Circle)::DownCast (edgeCurve)->Circ ());
		}
		else if (edgeCurve->IsKind (STANDARD_TYPE (Geom_Line))) {
			ret.lines.push_back (Handle (Geom_Line)::DownCast (edgeCurve)->Lin ());
		}
		else if (edgeCurve->IsKind (STANDARD_TYPE (Geom_BSplineCurve))) {
			ret.bsplines.push_back (InterpolateEquidistantPointsOnCurve (
				Handle (Geom_BSplineCurve)::DownCast (edgeCurve), deflection));
		}
		else {
			// Ignored in production as this is just supposed to split
			cout << "\t\tEncountered unknown geometry type while extracting surface sub-geometries"
				<< edgeCurve->DynamicType ()->Name () << endl;
		}
	}
	return ret;
}

bool ValidateAllCirclesCoaxialAndSameRadius (const GPCircVector &circles) {
	if (circles.size () > 1) {
		double refRadius = circles[0].Radius ();
		gp_Ax1 refAxis = circles[0].Axis ();
		for (const auto &circle : circles) {
			if (!circle.Axis ().IsCoaxial (refAxis, angularTolerance, linearTolerance)) {
				return false;
			}
			if (abs (circle.Radius () - refRadius) > linearTolerance) {
				return false;
			}
		}
	} // Else: Either 0 or 1 circle, no mismatch possible
	return true;
}

gp_Pnt PointOnRadius (const gp_Circ &circ) {
	auto xAx = circ.XAxis ();
	auto center = xAx.Location ().XYZ ();
	auto directionTimesRadius = xAx.Direction ().XYZ () * circ.Radius ();
	return gp_Pnt (center + directionTimesRadius);
}

bool operator<(const gp_Pnt &a, const gp_Pnt &b) {
	return a.XYZ () < b.XYZ ();
}

bool operator<(const gp_XYZ &a, const gp_XYZ& b) {
	if ((b.X () - a.X ()) > linearTolerance) {
		return true;
	}
	else if ((b.Y () - a.Y ()) > linearTolerance) {
		return true;
	}
	else if ((b.Z () - a.Z ()) > linearTolerance) {
		return true;
	}
	return false;
}

bool operator<=(const gp_Pnt &a, const gp_Pnt &b) {
	return (a < b) || a.IsEqual (b, linearTolerance);
}

bool operator>(const gp_Pnt &a, const gp_Pnt &b) {
	if ((a.X () - b.X ()) > linearTolerance) {
		return true;
	}
	else if ((a.Y () - b.Y ()) > linearTolerance) {
		return true;
	}
	else if ((a.Z () - b.Z ()) > linearTolerance) {
		return true;
	}
	return false;
}

bool operator<(const gp_Ax1 &a, const gp_Ax1 &b) {
	if (a.Location ().Distance (b.Location ()) > linearTolerance) {
		return a.Location () < b.Location ();
	}
	return a.Direction ().XYZ () < b.Direction ().XYZ ();
}

vector<double> Compute360AnglesAroundAxis (const std::vector<gp_Pnt>& points, const gp_Ax1& axis, const gp_Vec& zeroDegReferenceVector) {
	std::vector<double> angles (points.size ());
	if (points.empty ()) {
		return angles;
	}
	cout << XYZToString (axis.Direction ().XYZ ()) << " " << XYZToString (zeroDegReferenceVector.XYZ ()) << endl;
	assert (gp_Vec (axis.Direction ()).IsNormal (zeroDegReferenceVector, angularTolerance));
	// norm.Angle() on the axis only gives unsigned angle, but we need the signed angle for proper operation.
	// Therefore we re-create the axis for each planar using the intersection point and the
	// planar center position.
	// NOTE: We will compare angles with "arbitrarily" chosen index 0 planar
	auto posRef = zeroDegReferenceVector.Crossed (gp_Vec (axis.Direction ()));
	for (size_t i = 1; i < points.size (); i++) {
		//Compute projeted base point
		gp_Pnt basePoint = OrthogonalProjectPointOntoAxis (axis, points[i]);

		auto normAx = gp_Vec (basePoint, points[i]);
		double angle = RAD_TO_DEG (zeroDegReferenceVector.AngleWithRef (normAx, axis.Direction ()));
		// For some reason, without this branch we end up with "360" instead of "180",
		// not too sure why that is so.
		if (!zeroDegReferenceVector.IsOpposite (normAx, angularTolerance)) {
			angle += 180;
		}
		angles[i] = angle;
	}
	return angles;
}

double ComputeVolume (const VolumetricElementCollection &primitives, VolumeSummationMode mode) {
	double volume;
	for (const auto& primitive : primitives) {
		double primVol = primitive->Volume ();
		if (mode == VolumeSummationMode::All || (mode == VolumeSummationMode::PositiveOnly && primVol > linearTolerance) ||
			(mode == VolumeSummationMode::NegativeOnly && primVol < -linearTolerance)) {
			volume += primVol;
		}
	}
	return volume;
}

double AxisDistance (const gp_Ax1 &ax1, const gp_Ax1 &ax2) {
	// Use any point on ax1, ax1.Location() will do
	auto p2 = OrthogonalProjectPointOntoAxis (ax2, ax1.Location ());
	return p2.Distance (ax1.Location ());
}

DistanceGroups GroupParallelByDistanceFromAxis (const gp_Ax1 &refAxis, const CoaxialityGroups &primitives) {
	DistanceGroups ret;
	for (const auto &group : primitives) {
		auto axis = group.first;
		if (!axis.IsParallel (refAxis, angularTolerance)) {
			cout << "Not parallel" << endl;
			continue; // Distance between non parallel axes is meaningless in the context of finding radial features
		}
		double distance = AxisDistance (axis, refAxis);
		if (abs (distance) < linearTolerance) {
			continue; // Prevent adding the same axis as distance class
		}
		bool finished = false;
		for (auto &otherPair : ret) {
			if (abs (distance - otherPair.first) < linearTolerance) {
				otherPair.second.push_back (group.first);
				// This entity is finished!
				finished = true;
				break;
			}
		}
		if (!finished) { // Not inserted anywhere else
			ret[distance] = { group.first };
		}
	}
	return ret;
}

bool DifferenceLessThanLinearToleranceComparator::operator()(const double &lhs, const double &rhs) const {
	return abs (lhs - rhs) < linearTolerance;
}

void SortElements (VolumetricElementCollection &src) {
	std::sort (src.begin (), src.end (), [](const VolumetricElementPtr &p1, const VolumetricElementPtr &p2) {
		// First: Sort non-holes before holes
		if (!p1->isHole && p2->isHole) {
			return true;
		}
		else if (p1->isHole && !p2->isHole) {
			return false;
		}
		// First: sort by bound1 (via operator<)
		if (p1->bound1.Distance (p2->bound1) > linearTolerance) {
			return (*p1) < (*p2);
		}
		else {
			// Second sort column: is hole
			if (p1->isHole != p2->isHole) {
				return p1->isHole < p2->isHole; // false before true
			}
			else {
				// Third sort column: volume (usually depends on diameter)
				return p1->Volume () < p2->Volume ();
			}
		}
	});
}

VolumetricElementCollection UniquifyElements (const VolumetricElementCollection &src, FeatureElementCollection& ignored) {
	/**
	 * New brute force algorithm.
	 * std::unique required std::sort and there is no 100% correct sort criterion
	 * that ensures all dupes are removed!
	 */
	vector<bool> isDuplicate (src.size ());
	size_t numDuplicates = 0;
	for (size_t i = 0; i < src.size (); i++)
	{
		if (isDuplicate[i]) {
			continue;
		}
		// For every element after the current one
		for (size_t j = i + 1; j < src.size (); j++) {
			if (isDuplicate[j]) {
				continue; //already duplicate, no need to check
			}
			// Avoid considering hole elements as a duplicate of non-holes
			// (but removing holes if otherwise identical is OK)
			// This SHOULD be handled by sorting but its better to be safe here
			if (src[i]->isHole && !src[j]->isHole) {
				continue;
			}
			if (*src[i] == *src[j]) {
				isDuplicate[j] = true; // keep i as non duplicate
				numDuplicates++;
			}
		}
	}
	// Insert into either ret or ignore
	VolumetricElementCollection ret;
	ret.reserve (src.size () - numDuplicates);
	for (size_t i = 0; i < src.size (); i++) {
		if (isDuplicate[i]) {
			ignored.push_back (dynamic_pointer_cast<FeatureElement>(src[i]));
		}
		else {
			ret.push_back (src[i]);
		}
	}
	return ret;
}

std::optional<VolumetricElementCollection> FindCoalityGroup (const gp_Ax1 &axis,
	const CoaxialityGroups &groups) {
	for (const auto& group : groups) {
		if (IsCoaxialOrReversedCoaxial (axis, group.first)) {
			return group.second;
		}
	}
	return {};
}

void FixUndetectedHolesByContains (const VolumetricElementCollection &elems) {
	// Find any pair of outer elements where one contains the other completely
	for (size_t i = 0; i < elems.size (); i++) {
		for (size_t j = 0; j < elems.size (); j++) {
			if (i == j || elems[i]->isHole || elems[j]->isHole) {
				continue;
			}
			if (elems[i]->Contains (*elems[j])) { // i.e. they would overlap.
				cout << "Element (A) is completely contained within element (B) ; making (A) a hole:\n\t(A) "
					<< elems[j]->ToString () << "\n\t(B) " << elems[i]->ToString () << endl,
					elems[j]->isHole = true;
			}
		}
	}
}

vector<gp_Pnt> InterpolateEquidistantPointsOnCurve (Handle (Geom_BSplineCurve) bspline, double deflection) {
	// Provide additional info about the curve for the algorithm
	GeomAdaptor_Curve adaptor (bspline);
	// Quasi uniform abscissae == parameters to later compute equidistant points on curve
	GCPnts_QuasiUniformDeflection algorithm (adaptor, deflection);
	// Check success
	vector<gp_Pnt> ret;
	if (!algorithm.IsDone ()) {
		return ret;
	}
	// Check point
	for (size_t i = 1; i <= algorithm.NbPoints (); i++)
	{
		ret.push_back (algorithm.Value (i));
	}
	return ret;
}

gp_Pnt2d PointToAxialRadialCartesianCoordinate (const gp_Ax1 &axis, const gp_Pnt &pnt, bool absolute) {
	/* With:
	 *    (L) is the location of the given axis
	 *    (P) is the given point
	 *    (O) is the orthogonal projection of (P) onto the given axis
	 * Then:
	 *    X = Distance between (L) and (O)
	 *    Y = Distance between (O) and (P)
	 */
	auto L = axis.Location ();
	auto O = OrthogonalProjectPointOntoAxis (axis, pnt);
	auto X = L.Distance (O);
	auto Y = O.Distance (pnt);
	/**
	 * X might be "in direction of axis" or "opposite to direction of axis"
	 * Distance() is always unsigned so we dont know.
	 * Compute vector LO and check its relation to axis.Direction()
	 */
	gp_Vec LO (L, O);
	if (abs (LO.Magnitude ()) < linearTolerance) {
		X = 0.0; // Avoid exception due to null magnitude in IsOpposite
	}
	else if (!absolute && LO.IsOpposite (axis.Direction (), angularTolerance)) {
		X = -X;
	}
	return gp_Pnt2d (X, Y);
}

vector<gp_Pnt2d> PointsToAxialRadialCartesianCoordinates (const gp_Ax1 &axis, const vector<gp_Pnt> &pnts, bool absolute) {
	vector<gp_Pnt2d> ret (pnts.size ());
	for (size_t i = 0; i < pnts.size (); i++) {
		ret[i] = PointToAxialRadialCartesianCoordinate (axis, pnts[i], absolute);
	}
	return ret;
}

gp_Pnt2d PointOfExtremeCoordinate (const vector<gp_Pnt2d> &vec, Coordinate mode, ExtremumOperation op) {
	// Parameter check
	if (vec.size () == 0) {
		throw std::invalid_argument ("Empty vector");
	}
	else if (mode == Coordinate::Z) {
		throw std::invalid_argument ("Can't use Coordinate::Z for 2D point");
	}
	// Actually find maximum
	size_t maxIdx = 0; // index of maximum X
	Standard_Real extremeV = vec[0].X ();
	for (size_t i = 1; i < vec.size (); i++)
	{
		auto v = (mode == Coordinate::X ? vec[i].X () : vec[i].Y ());
		if ((op == ExtremumOperation::Max && v > extremeV) || (op == ExtremumOperation::Min && v < extremeV)) {
			maxIdx = i;
			extremeV = v;
		}
	}
	return vec[maxIdx];
}

gp_Pnt PointOfExtremeDistance (const vector<gp_Pnt> &vec, const gp_Pnt& refPnt, ExtremumOperation op) {
	// Parameter check
	if (vec.size () == 0) {
		throw std::invalid_argument ("Empty vector");
	}
	// Actually find maximum
	size_t maxIdx = 0; // index of maximum X
	Standard_Real extremeV = refPnt.Distance (vec[0]);
	for (size_t i = 1; i < vec.size (); i++) {
		auto v = refPnt.Distance (vec[i]);
		if ((op == ExtremumOperation::Max && v > extremeV) || (op == ExtremumOperation::Min && v < extremeV)) {
			maxIdx = i;
			extremeV = v;
		}
	}
	return vec[maxIdx];
}

vector<double> ExtractCoordinate (const vector<gp_Pnt2d> &pnts, Coordinate mode) {
	vector<double> ret (pnts.size ());
	if (mode == Coordinate::Z) {
		throw std::invalid_argument ("Can't use Coordinate::Z for 2D point");
	}
	for (size_t i = 0; i < pnts.size (); i++) {
		if (mode == Coordinate::X) {
			ret[i] = pnts[i].X ();
		}
		else if (mode == Coordinate::Y) {
			ret[i] = pnts[i].Y ();
		}
	}
	return ret;
}
