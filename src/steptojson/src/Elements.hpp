#pragma once

#include "Utils.hpp"
#include "common.hpp"
#include <gp_Ax1.hxx>
#include <gp_Pnt.hxx>
#include <iostream>
#include <memory>
#include <string>

#include "lib/json.hpp"

using json = nlohmann::json;

using namespace std;

class VolumetricElement;
class PlanarElement;
class FeatureElement;
class ExtraFeature;

/*
 * Type conventions
 */
typedef shared_ptr<VolumetricElement> VolumetricElementPtr;
typedef shared_ptr<PlanarElement> PlanarElementPtr;
typedef shared_ptr<FeatureElement> FeatureElementPtr;
typedef shared_ptr<ExtraFeature> ExtraFeaturePtr;

typedef vector<VolumetricElementPtr> VolumetricElementCollection;
typedef vector<PlanarElementPtr> PlanarElementCollection;
typedef vector<FeatureElementPtr> FeatureElementCollection;
typedef vector<ExtraFeaturePtr> ExtraFeatureCollection;

enum class PrimitivePlacement {
    Nothing, Left, Right, Middle
};

std::ostream &operator<<(std::ostream &os, PrimitivePlacement placement);

/**
 * Baseclass for all features
 */
class FeatureElement {
  public:
    virtual string ToString() const = 0;
    virtual gp_Ax1 Axis() const = 0;
    // The faces this shape originates from, as indices in the underlying set of faces
    // (usually STEPFeatureDetector::allFaces)
    std::vector<int> sourceFaces;

    // User-defined description. Usually imported from the STEP entity description
    string description;

    gp_Pnt Location() const;

    bool ValidateCoaxiality(gp_Ax1 axis, bool oppositeOK = false);

    bool IsCoaxialTo(const FeatureElement &other);

    bool IsCoaxialOrReversedCoaxial(const gp_Ax1 &otherAxis);
};

/**
 * Extra features that can be applied to a face
 */
class ExtraFeature {
  public:
    virtual void ToRapidfactureJSON(json &features) const = 0;
};

class HoleCircle : public ExtraFeature {
public:
    double diameter; // main axis to structure axis
    double startAngle;
    double angleInterval;
    size_t number;
    VolumetricElementCollection structure;

    void ToRapidfactureJSON(json &features) const;
};

/**
 * A geometric element defined by a brep on an infinite plane.
 * Does not have any volume
 */
class PlanarElement : public FeatureElement {
  public:
    bool isHole;
    gp_Ax1 axis;

    virtual string ToString() const = 0;

    gp_Ax1 Axis() const;

    PlanarElement() = default;
};

/**
 * A rectangle projected onto a planar.
 * NOT a cylinder surface section altough this might look like
 * a rectangle in the corresponding coordinate system.
 * Axis is the normal of the plane
 */
class RectangleElement : public PlanarElement {
  public:
    gp_Pnt corners[4]; // Two diagonally opposing corners
    // already defined by other parameters but hard to calculate
    double width, height;

    // Points on the (arbitrarily defined) width and height axes
    // NOTE: Do not make any assumption about which specific points these are
    gp_Pnt pointsOnWidthAxis[2];
    gp_Pnt pointsOnHeightAxis[2];

    // One of the center points for the width / height-corresponding edges
    gp_Pnt WidthAxisCenter();
    gp_Pnt HeightAxisCenter();

    gp_Ax1 WidthAxis();
    gp_Ax1 HeightAxis();

    double Width();
    double Height();

    RectangleElement(const gp_Ax1 &ax);

    string ToString() const;
};

/**
 * 2D circle outline on a 2D plane in 3D space
 */
class CircleElement : public PlanarElement {
public:
    gp_Ax1 axis;
    double diameter;

    // axis: Normal or antinormal of plane, point is center of circle
    CircleElement(const gp_Ax1& axis, double diameter);

    string ToString() const;
    string ToJSON() const;
};

/**
 * An element with a volume, e.g. a cylinder.
 */
class VolumetricElement : public FeatureElement {
  public:
    enum class VolumetricType {
        Conic,
        Polygon,
        Toroid // i.e. radius
    };

    /**
     * Extra features like radial boring
     */
    ExtraFeatureCollection leftFeatures;
    ExtraFeatureCollection middleFeatures;
    ExtraFeatureCollection rightFeatures;

    /**
     * Add a new feature to the collection determined by placement
     */
    void AddFeature(const ExtraFeaturePtr& feature, PrimitivePlacement placement);

    /**
     * Add left, middle and right features to the root object, if any
     */
    void AddAllFeatures(json& j) const;

    /**
     * Bounding points which are the centers of the bounding circles.
     * These implicitly define the axis of the cylinder.
     */
    gp_Pnt bound1, bound2;
    /**
     * True if this is a cylindric (part of a) hole instead of a cylindric solid
     */
    bool isHole;

    virtual string ToString() const = 0;
    virtual json ToRapidfactureJSON(PrimitivePlacement placement=PrimitivePlacement::Nothing) const = 0;
    virtual void Normalize() = 0;
    // Dynamic type / inheritance utility
    virtual VolumetricType Type() const = 0;
    // Used in operator==
    virtual bool IsEqual(const VolumetricElement &other) const = 0;
    /**
     * Is equal considering only relative parameters instead of bounding points.
     * Axis parallelism is not taken into consideration
     */
    virtual bool IsEqualIgnoreLocation(const VolumetricElement &other) const = 0;

    /**
     * @return true if this contains elem completely, i.e.
     * this->bound1 <= elem->bound1 && this->bound2 >= elem->bound2 && this->Volume > other->Volume
     */
    bool Contains(const VolumetricElement& elem) const;

    bool operator==(const VolumetricElement& other);

    gp_Ax1 Axis() const;

    virtual double Volume() const = 0;

    double Length() const;

    bool operator<(const VolumetricElement& other);

    VolumetricElement() = default;
};

/**
 * An axial polygon, e.g. a hexagon.
 */
class PolygonalElement : public VolumetricElement {
  public:
    unsigned int sides;
    /**
     * Face-to-face radius, i.e. half the distance of a face to a hypothetical opposing faces
     */
    double radius;

    /**
     * this->radius is the face-center radius.
     * Compute the circumscribed diameter, i.e. the minimum
     * turned radius to be able to mill the polygon.
     * Ref https://www.mathopenref.com/polygonradius.html
     */
    double CircumscribedDiameter() const;

    double AngleBetweenSides() const;

    VolumetricType Type() const;

    PolygonalElement(unsigned int sides, const gp_Pnt &bound1, const gp_Pnt &bound2, double radius, bool isHole = false);

    void Normalize();

    bool IsEqual(const VolumetricElement &otherSuperclass) const;
    bool IsEqualIgnoreLocation(const VolumetricElement &other) const;

    string ToString() const;

    string ToJSON() const;

    double Volume() const;

    json ToRapidfactureJSON(PrimitivePlacement placement=PrimitivePlacement::Nothing) const;
};

/**
 * A conical element of a given diameter, or a conical element (which is treated as a cylinder with different diameters)
 * between bound1 and bound2
 */
class ConicalElement : public VolumetricElement {
public:
    /**
     * diameter1 is at this.Location() ; diameter2 is at bound()
     */
    double diameter1, diameter2;

    ConicalElement(const gp_Pnt& bound1, const gp_Pnt& bound2, double diameter1, double diameter2, bool isHole=false);

    VolumetricType Type() const;

    /**
     * If bound1 > bound2  according to some arbitrary predefined proper relation,
     * swap the bounds
     */
    void Normalize();

    bool IsCylinder() const;

    bool IsTruncatedCone() const;
    
    bool Validate() const;

    bool IsEqual(const VolumetricElement& otherSuperclass) const;
    bool IsEqualIgnoreLocation(const VolumetricElement &other) const;

    // For sorting
    bool operator<(const ConicalElement& other) const;

    string ToString() const;

    string ToJSON() const;

    double Volume() const;

    json ToRapidfactureJSON(PrimitivePlacement placement=PrimitivePlacement::Nothing) const;
};


class ToroidalElement : public VolumetricElement {
public:
    double radius1; // Bounding circle radius 1
    double radius2; // Bounding circle radius 2
    double minorRadius; // Radius of the rotated circle
    double majorRadius; // Radius of the center of the center, referenced to the axis
    bool isConvex;
    // These are relative to bound1
    double centerCoordinateAxial;
    double centerCoordinateOrthogonal;

    ToroidalElement(const gp_Pnt& bound1, double radius1, const gp_Pnt& bound2, double radius2,
                    double majorRadius, double minorRadius, bool isConvex);

    VolumetricType Type() const;

    void Normalize();

    void Validate();

    bool IsEqual(const VolumetricElement &otherSuperclass) const;
    bool IsEqualIgnoreLocation(const VolumetricElement &other) const;

    string ToString() const;

    string ToJSON() const;

    double Volume() const;

    json ToRapidfactureJSON(PrimitivePlacement placement=PrimitivePlacement::Nothing) const;
};

// For hole circles mainly
bool CompareIgnoreLocation(const VolumetricElementCollection &a, const VolumetricElementCollection &b);
