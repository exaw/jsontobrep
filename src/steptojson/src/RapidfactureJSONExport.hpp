#pragma once
#include "Utils.hpp"
#include "Elements.hpp"
#include "common.hpp"
#include "Elements.hpp"
#include <gp_Pnt.hxx>
#include <vector>

#include "lib/json.hpp"

using namespace std;
using json = nlohmann::json;

/**
 * Algorithm container for splitting volumetric contours into n
 */
class RFJSONExportData {
  public:
    VolumetricElementCollection outerShape;
    VolumetricElementCollection leftInnerShape;
    VolumetricElementCollection rightInnerShape;
    // Next point where the next inner contour boundary point might be (i.e. the next inner element)
    gp_Pnt leftBorder, rightBorder;
    bool isThroughHole; // true => rightInnerShape is empty ; continous through hole

    json ToJSON(const std::string &stepLabel);

    /**
     * Iteratively find the next unprocessed elements for the left inner contour, if any
     * Call until false is returned.
     * NOTE: left inner elements are processed from left to right
     */
    bool FindNextLeftElement(const VolumetricElementCollection &allElements, std::vector<bool> &alreadyProcessed);

    /**
     * Iteratively find the next unprocessed elements for the right inner contour, if any.
     * Call until false is returned.
     * NOTE: Right inner elements are processed from right to left and need to be reversed
     */
    bool FindNextRightElement(const VolumetricElementCollection &allElements, std::vector<bool> &alreadyProcessed);

    /**
     * Load from the given collection of mixed inner/outer elements.
     */
    void Load(const VolumetricElementCollection &allElements);

    /**
     * Validate main axis-wise adjacency. i.e. that there are no gaps between elements
     */
    void CheckAdjacency();

    void SortOuterElements();
};
