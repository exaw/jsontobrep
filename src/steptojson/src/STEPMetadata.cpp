#include "STEPMetadata.hpp"
#include "Utils.hpp"
#include <Standard_Handle.hxx>
#include <StepRepr_RepresentationItem.hxx>
#include <StepRepr_Representation.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shape.hxx>
#include <XSControl_TransferReader.hxx>
#include <XSControl_WorkSession.hxx>

string ConvertHAsciiStringToStdString(Handle(TCollection_HAsciiString) handle) {
    if (handle.IsNull()) {
        return std::string();
    }
    return std::string(handle->ToCString());
}

std::string ParseSTEPFileLabel(Handle(StepData_StepModel) model) {
    for (Standard_Integer i = 1; i <= model->NbEntities(); i++) {
        auto ent = Handle(StepRepr_Representation)::DownCast(model->Value(i));
        if (ent.IsNull() || ent->Name().IsNull() || ent->Name()->Length() == 0) {
            continue;
        }
        return string(ent->Name()->ToCString());
    }
    return "";
}

std::vector<std::pair<TopoDS_Shape, std::string>> ExtractTopologyToNameMap(STEPControl_Reader& reader) {
    const auto WS = reader.WS();
    const auto theModel = WS->Model();
    auto tp = WS->MapReader();

    std::vector<std::pair<TopoDS_Shape, std::string>> topoNameMap;

    Standard_Integer nb = theModel->NbEntities();
    for (Standard_Integer i = 1; i <= nb; i++) {
        auto ent = Handle(StepRepr_RepresentationItem)::DownCast(theModel->Value(i));
        // "Convert" entity so it is known in the session
        reader.TransferEntity(ent);
        if (ent.IsNull()) {
            continue;
        }
        // Get name
        string name = ConvertHAsciiStringToStdString(ent->Name());
        // cout << ent->DynamicType()->Name() << " " << WS->TransferReader()->HasResult(theModel->Value(i) << endl;
        // Can we get a shape for this entity?
        if (WS->TransferReader()->HasResult(ent)) {
            // Get topological shape for that entity
            auto shape = WS->TransferReader()->ShapeResult(ent);
            /*cout << ent->DynamicType()->Name() << " --> "
                 << (shape.IsNull() ? "null" : CompoundTypeToString(shape.ShapeType())) << endl;*/
            // Continue only if there is a shape AND a map
            if (shape.IsNull()) {
                continue;
            }
            if (name == "NONE") {
                continue;
            }
            // cout << ent->DynamicType()->Name() << " " << name << endl;
            // Iterate faces
            for (TopExp_Explorer faceExplorer(shape, TopAbs_FACE); faceExplorer.More(); faceExplorer.Next()) {
                // get current face and convert to TopoDS_Face
                const TopoDS_Face &face = TopoDS::Face(faceExplorer.Current());
                topoNameMap.emplace_back(face, name);
            }
        }
    }
    return topoNameMap;
}