
#include <filesystem>

#include "STEPFeatureDetector.hpp"
#include "Exceptions.hpp"
#include "GeometryUtils.hpp"
#include "GroupBy.hpp"
#include "STEPMetadata.hpp"
#include "STEPMetadata.hpp"
#include "Split.hpp"
#include "Utils.hpp"
#include <BRepLib_FindSurface.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <BRep_Tool.hxx>
#include <Geom_Circle.hxx>
#include <Standard_Type.hxx>
#include <StlAPI.hxx>
#include <StlAPI_Writer.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS_Builder.hxx>
#include <TopoDS_Compound.hxx>
//#include <VrmlData_Scene.hxx>
//#include <VrmlData_ShapeConvert.hxx>

#include <gp_Ax1.hxx>
#include <gp_Cone.hxx>
#include <gp_Cylinder.hxx>
#include <gp_Lin.hxx>
#include <gp_Lin2d.hxx>
#include <gp_Pln.hxx>
#include <gp_Torus.hxx>

void TESTING_Cutouts(const TopoDS_Face &face, const FaceSubGeometries &faceSubGeometries) {
    // Analyze outer wire of face
    auto wire = InnerWire(face);
    for (BRepTools_WireExplorer wireExplorer(wire); wireExplorer.More(); wireExplorer.Next()) {
        const TopoDS_Edge &edge = wireExplorer.Current();
        Standard_Real umin, umax;
        // Get unbounded curve and bounding parameters
        auto edgeCurve = BRep_Tool::Curve(edge, umin, umax);
        // Handle "no curve" case, occurs e.g. for conical surfaces
        if (edgeCurve.IsNull()) {
            continue;
        }
        // Assign depending on type
        if (edgeCurve->IsKind(STANDARD_TYPE(Geom_Circle))) {
            cout << "Cutout element: Circle" << endl;
            // faceSubGeometries.circles.push_back(Handle(Geom_Circle)::DownCast(edgeCurve)->Circ());
        } else if (edgeCurve->IsKind(STANDARD_TYPE(Geom_Line))) {
            cout << "Cutout element: Line" << endl;
            // faceSubGeometries.lines.push_back(Handle(Geom_Line)::DownCast(edgeCurve)->Lin());
        } else {
            // Ignored in production as this is just supposed to split
            cout << "\t\tEncountered unknown geometry type while extracting surface cutout sub-geometries"
                 << edgeCurve->DynamicType()->Name() << endl;
        }
    }
}

STEPFeatureDetector::STEPFeatureDetector(const std::string &filenameArg) : have0DegreeReferenceVector(false) {
    if(filenameArg == "") {
        cerr << "Empty STEP filename, can't process!" << endl;
        return;
    }
    // Check if input file exists
    if(!std::experimental::filesystem::exists(filenameArg)) {
        throw STEPFileDoesNotExistException(filenameArg.c_str());
    }
    this->filename = filenameArg;
    cout << "Reading STEP file from " << filename << endl;
    
    auto stepStat = reader.ReadFile(filename.c_str());

    if(stepStat != IFSelect_ReturnStatus::IFSelect_RetDone) {
        throw STEPFileInvalidException(IFSelectReturnStatusToString(stepStat));
    }

    // Extract label for entire file (as opposed to label for certain entities)
    this->label = ParseSTEPFileLabel(reader.StepModel());

    // Check if there is anything to convert
    auto numroots = reader.NbRootsForTransfer();
    if (numroots < 1) {
        throw NoEntitiesInSTEPFileException("");
    }
    // Convert STEP to shape
    if (reader.TransferRoots() < 1) {
        throw NoTransferableRootsInSTEPFileException("");
    }

    // Use a single shape
    auto theshape = reader.OneShape();
    if (theshape.IsNull()) {
        throw STEPToTopoShapeConversionFailed("");
    }
    if (theshape.ShapeType() != TopAbs_SOLID) {
        throw STEPShapeIsNotASolidException(CompoundTypeToString(theshape.ShapeType()));
    }
    // Extract labels for individual things (faces, edges, vertices etc)
    topoNameMap = ExtractTopologyToNameMap(reader);
    // Extract model
    model = reader.StepModel();

    // Do the heavy lifting work
    solid = TopoDS::Solid(theshape);
    Parse(solid);
}

void STEPFeatureDetector::Parse(const TopoDS_Solid &solid) {
    IterateFaces(solid);
    // Print cylinders
    SortElements(volumetricElements); // Sort before uniq, makes behaviour more deterministic
    cout << "-------------- Before unique filter" << endl;
    PrintElements(volumetricElements);
    // Run unique filter
    volumetricElements = UniquifyElements(volumetricElements, ignoredElements);
    // print again
    cout << "-------------- After unique filter" << endl;
    PrintElements(volumetricElements);
    cout << "--------------" << endl;

    if (volumetricElements.size() == 0) {
        cerr << "There are no volumetric elements in the part, can't process" << endl;
        return;
    }

    // Also processes polygons and hole cirlces
    ProcessCoaxialityClasses();
    // Sometimes holes are not recognized but we can identify them if they lie completely within an outer structure.
    FixUndetectedHolesByContains(volumetricElements);
    // Remove ignored elements before sorting and deduping
    // Sort by pointer
    std::sort(volumetricElements.begin(), volumetricElements.end());
    std::sort(ignoredElements.begin(), ignoredElements.end());
    VolumetricElementCollection removeResult;
    // remove ignored elements
    auto it = std::set_difference(volumetricElements.begin(), volumetricElements.end(),
    ignoredElements.begin(), ignoredElements.end(), std::back_inserter(removeResult));
    cout << "Removed " << (volumetricElements.size() - removeResult.size()) << " ignored elements ; " <<
    removeResult.size() << " elements left" << endl;

    // Re-sort because some elements might have been added and removed
    volumetricElements = UniquifyElements(removeResult, ignoredElements);
    SortElements(volumetricElements);

    // Prepare for export
    exportData.Load(volumetricElements);
}

void STEPFeatureDetector::ExportRFJSON(const std::string& filename) {
    ofstream of(filename);
    // Write to JSON file, using pretty indents and rounding to 10 nm (1e-5mm) to account for numeric precision
    of << setw(4) << exportData.ToJSON(label);
    of.close();
}

void STEPFeatureDetector::HandleCylindricalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_CylindricalSurface) csurf,
                                                   const std::string &label) {
    // Get infinite cylindrical surface, of which the cylinder is a part of.
    auto cylinder = csurf->Cylinder();
    // Find symmetry axis of the cylinder, which is the symmetry axis of the current sub-part
    auto cylAx = cylinder.Axis();
    bool isHole = IsFacePartOfHole(face);

    cout << "\tCylindrical surface" << (isHole ? " (hole)" : "") << " in axis " << DirectionToString(cylAx.Direction())
         << " @ " << PointToString(cylAx.Location()) << endl;
    // Extract bound faceSubGeometries.circles (untrimmed), from which we can determine the bounds
    auto faceSubGeometries = SplitFaceSubGeometries(face);
    //TESTING_Cutouts(face, faceSubGeometries.lines, faceSubGeometries.circles);
    // It is assumed that outer faceSubGeometries.circles are th outer bounds of the cylinder
    if (faceSubGeometries.circles.size() >= 2) { 
        // We expect to see two centers, but possibly more than one oing
        auto centerGroups = GroupByLocation(faceSubGeometries.circles);
        if (centerGroups.Size() > 2) {
            cerr << "Found " << centerGroups.Size() << " unique centers instead of 2" << endl;
            return;
        }
        auto center1 = centerGroups[0];
        auto center2 = centerGroups[1];
        auto circle1 = centerGroups.FirstValue(center1);
        auto circle2 = centerGroups.FirstValue(center2);
        double radius1 = circle1.Radius();
        // Validation
        if (!IsCoaxialOrReversedCoaxial(circle1.Axis(), circle2.Axis())) {
            cerr << "Two bounding circle oaxiality mismatch!" << endl;
            return;
        }
        if (!gp_Lin(cylAx).Contains(center1, linearTolerance)) {
            cerr << "Geometry cylinder axis does not contain bounding circle 1 center" << endl;
            return;
        }
        if (!gp_Lin(cylAx).Contains(center2, linearTolerance)) {
            cerr << "Geometry cylinder axis does not contain bounding circle 2 center" << endl;
            return;
        }
        // Construct result. NOTE: Cylinders are treated as special case conics
        auto diameter = radius1 * 2.0;
        auto *elem = new ConicalElement(center1, center2, diameter, diameter, isHole);
        // Normalize direction etc
        elem->Normalize();
        if (!elem->Validate()) {
            cerr << "Cylinder failed validation" << endl;
        }
        // Tolerate opposite axis when validating
        if (!elem->ValidateCoaxiality(cylAx, true)) {
            cerr << "Cylinder is not coaxial to face base cylinder axis" << endl;
        }
        elem->description = label;
        elem->sourceFaces.push_back(faceIdx);
        volumetricElements.emplace_back(elem);
    } else if (faceSubGeometries.circles.size() == 0 && faceSubGeometries.bsplines.size() == 2) {
        /**
         * This is the case for a cross through hole through a cylinder or a cone
         * We will emit this as a regular cylinder (hole?), as we don't know the axis yet.
         *
         * The cylinder bound points will be termined by the outermost bsplines,
         * i.e. the emitted cylinder length will be very slightly larger than the actual
         * drill depth.
         */
        auto bsplinePointsA = faceSubGeometries.bsplines[0];
        auto bsplinePointsB = faceSubGeometries.bsplines[1];
        assert(bsplinePointsA.size() > 0);
        assert(bsplinePointsB.size() > 0);
        // In order to determine the outermost points we first need to find a reference point.
        // This point must be far away to avoid false extremum points
        // Therefore we use point A for curve B and vice versa, because it is assumed
        // they are at opposite points of the cylinder.
        // The points can be ANY point, so [0] will do just fine.
        auto pointA = bsplinePointsA[0];
        auto pointB = bsplinePointsB[0];

        auto extremePointA = PointOfExtremeDistance(bsplinePointsA, pointB);
        auto extremePointB = PointOfExtremeDistance(bsplinePointsB, pointA);
        // Compute centers of the "virtual" bounding circles
        auto bound1 = OrthogonalProjectPointOntoAxis(cylAx, extremePointA);
        auto bound2 = OrthogonalProjectPointOntoAxis(cylAx, extremePointB);

        /**
         * Build the element
         */
        double diameter = 2.0 * cylinder.Radius();
        auto *elem = new ConicalElement(bound1, bound2, diameter, diameter, isHole);
        elem->Normalize();
        if (!elem->Validate()) {
            cerr << "Cylinder failed validation" << endl;
        }
        // Tolerate opposite axis when validating
        if (!elem->ValidateCoaxiality(cylAx, true)) {
            cerr << "Cylinder is not coaxial to face base cylinder axis" << endl;
        }
        elem->description = label;
        elem->sourceFaces.push_back(faceIdx);
        volumetricElements.emplace_back(elem);
    } else {
        cerr << "\tCylindrical surface does not have 2 (trimmed) circular outer wire edges but "
            << faceSubGeometries.circles.size() << " circles, "
             << faceSubGeometries.lines.size() << " lines & "
             << faceSubGeometries.bsplines.size() << " B-splines" << endl;
    }
}

void STEPFeatureDetector::HandlePlane(int faceIdx, const TopoDS_Face &face, Handle(Geom_Plane) plane, const std::string &label) {
    auto pln = plane->Pln();
    bool isHole = IsFacePartOfHole(face);
    // Get axis, ignore point
    auto dir = pln.Axis().Direction();
    // Parse the bound of the plane
    auto faceSubGeometries = SplitFaceSubGeometries(face);

    if (faceSubGeometries.lines.size() == 0 && faceSubGeometries.circles.size() >= 1) {
        if (!ValidateAllCirclesCoaxialAndSameRadius(faceSubGeometries.circles)) {
            cerr << "Not all faceSubGeometries.circles are coaxial and have the same radius on this plane!" << endl;
            return;
        }
        // Just use the first circle, as they are equivalent anyway
        double diameter = faceSubGeometries.circles[0].Radius();
        gp_Ax1 axis = faceSubGeometries.circles[0].Axis(); // Normal, passes through center

        auto element = new CircleElement(axis, diameter);
        element->isHole = isHole;
        element->sourceFaces.push_back(faceIdx);
        planarElements.emplace_back(element);
        // cout << element->ToString() << endl;
    } else if (faceSubGeometries.lines.size() == 4 && faceSubGeometries.circles.size() == 0) {
        RectangleElement *rect = nullptr;
        if ((rect = TryParseRectangle(pln.Axis().Direction(), faceSubGeometries.lines)) != nullptr) {
            cout << "\t" << rect->ToString() << endl;
            rect->isHole = isHole;
            planarElements.emplace_back(rect);
        } else {
            cerr << "\tNot actually a rectangle" << endl;
        }
    } else {
        cerr << "No known combination of entities  " << faceSubGeometries.lines.size() << " line and " << faceSubGeometries.circles.size()
             << " circles for that plane" << endl;
    }
}

void STEPFeatureDetector::HandleToroidalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_ToroidalSurface) tsurf,
                                                const std::string &label) {
    // Get infinite toroidal surface, of which the cylinder is a part of.
    auto torus = tsurf->Torus();
    // Get geometric properties
    auto coordSys = torus.Position();
    auto isRightHanded = coordSys.Direct();
    auto axis = torus.Axis();
    double minorRadius = torus.MinorRadius();
    double majorRadius = torus.MajorRadius();
    // NOTE: center == axis.Location()
    // cout << "\tCenter: " << PointToString(center) << " " << isRightHanded << endl;
    // cout << "\tRadius: " << minorRadius << " " << majorRadius << endl;
    // cout << "\tAxis: " << DirectionToString(axis.Direction()) << endl;
    bool isOuter = false;
    // Parse the bound of the plane
    auto faceSubGeometries = SplitFaceSubGeometries(face);

    // Split by axis equivalence into co-axial and other faceSubGeometries.circles
    // Coaxial circles are stored in coaxSplit.Yes
    Split<gp_Circ> coaxSplit = SplitByCoaxialityOrReverseCoaxiality(faceSubGeometries.circles, axis);

    // Get axial faceSubGeometries.circles and bounds
    if (coaxSplit.Yes.size() != 2) {
        cerr << "Coaxiality split 'yes' part has size of " << coaxSplit.Yes.size() << " but 2 is required!" << endl;
        return;
    }
    auto axCirc1 = coaxSplit.Yes[0];
    auto axCirc2 = coaxSplit.Yes[1];

    if (!IsCoaxialOrReversedCoaxial(axCirc1.Axis(),  axCirc2.Axis())) {
        cerr << "Bounding circle planar axes are not parallel" << endl;
        return;
    }
    // Normalize center order, as we need that for convexness computation
    if (axCirc1.Location() > axCirc2.Location()) {
        std::swap(axCirc1, axCirc2);
    }
    // Extract generic info from the faceSubGeometries.circles
    auto radius1 = axCirc1.Radius();
    auto radius2 = axCirc2.Radius();
    cout << "\tRadii " << radius1 << " and " << radius2 << endl;
    auto bound1 = axCirc1.Location();
    auto bound2 = axCirc2.Location();

    // Get points on radius of circle
    auto ax1PointOnRadius = PointOnRadius(axCirc1);
    auto ax2PointOnRadius = PointOnRadius(axCirc2);
    // The X axis vectors are coplanar because the faceSubGeometries.circles are coplanar,
    // But we dont know if they are orthogonal.
    gp_Ax1 radiusPointsAxis(ax1PointOnRadius, gp_Vec(ax1PointOnRadius, ax2PointOnRadius));
    // Split
    if (coaxSplit.No.size() == 0) {
        cerr << "Coaxiality split 'no' part is empty" << endl;
        return;
    }
    // Use a "random" radial circle to determine the center point
    auto radCirc = coaxSplit.No[0];
    auto toroidCenterPoint = radCirc.Location();

    /**
     * Treat the problem as 2D geometry problem
     * For that we first compute cartesian coordinates
     * corresponding to "position on axis" (X) and "distance from axis" (Y)
     * 
     */
    gp_Ax1 virtualAxis(axCirc1.Location(), gp_Vec(bound1, bound2));

    gp_Pnt2d center = PointToAxialRadialCartesianCoordinate(virtualAxis, toroidCenterPoint);
    gp_Pnt2d r1 = PointToAxialRadialCartesianCoordinate(virtualAxis, ax1PointOnRadius);
    gp_Pnt2d r2 = PointToAxialRadialCartesianCoordinate(virtualAxis, ax2PointOnRadius);

    // Due to the definition of the virtual coordinate system, we can use this simple "X" axis
    gp_Ax2d mainAx2D(gp_Pnt2d(0.0, 0.0), gp_Dir2d(1.0, 0.0));
    gp_Lin2d radiiIntersector(gp_Ax2d(r1, gp_Vec2d(r1, r2)));

    // get line equation in general form ax+by+c=0
    double A,B,C;
    radiiIntersector.Coefficients(A,B,C);
    // Convert general form to slope intersect form y=mx+b
    double m = -A/B;
    double b = -C/B;

    // Compute line @ x coordinate of center
    double y = m*center.X() + b;
    // Convex if it lies "below" the line, concave else
    bool isConvex = center.Y() < y;

    cout << (isConvex ? "\tConvex" : "\tConcave") << endl;

    // Build torus object
    auto *elem = new ToroidalElement(bound1, radius1, bound2, radius2, majorRadius, minorRadius, isConvex);
    // Swap bound1 and bound2 (and radii) according if they are not in the right order
    elem->Normalize();

    // Compute center point relative to bound1
    // NOTE: elem->bound1 is not neccessarily == bound1 due to normalization

    // This is somewhat more complicated that one would expect as toroidCenterPoint might be located anywhere in 3D,
    // and we dont want to explicitly use a plane defined by axis an toroidCenterPoint.
    // Therefore we use trigonometry to find out the X/Y cartesian distance in the center plane

    // Compute axial ("Z" in RF JSON) distance by using orthognal projection
    // NOTE: magnitude == length
    elem->centerCoordinateAxial = center.X();
    // Determine orthogonal-to-axis dimension
    elem->centerCoordinateOrthogonal = center.Y();

    // Normalize direction etc
    elem->Validate();
    // Tolerate opposite axis when validating
    if (!elem->ValidateCoaxiality(axis, true)) {
        cerr << "Cylinder is not coaxial to face base cylinder axis" << endl;
    }
    elem->description = label;
    elem->sourceFaces.push_back(faceIdx);
    volumetricElements.emplace_back(elem);
}

void STEPFeatureDetector::HandleConicalSurface(int faceIdx, const TopoDS_Face &face, Handle(Geom_ConicalSurface) geomCone,
                                               const std::string &label) {
    gp_Cone cone = geomCone->Cone();
    auto axis = cone.Axis();

    bool isHole = IsFacePartOfHole(face);

    // Parse the bound of the plane
    auto faceSubGeometries = SplitFaceSubGeometries(face);

    /**
    * For every case:
    *  1. Compute first bound point
    *  2. Compute second bound point
    *  3. Compute first diameter (at axis location)
    *  4. Compute second diameter (at bound location)
    */
    gp_Pnt bound1, bound2;
    double diameter1, diameter2;

    if (faceSubGeometries.circles.size() == 1) { // Acute cone
        auto circle = faceSubGeometries.circles[0];
        bound1 = circle.Location();        // 1.
        bound2 = cone.Apex();              // 2.: Acute, therefore can use apex
        diameter1 = circle.Radius() * 2.0; // 3.
        diameter2 = 0.0;                   // 4.: Acute
    } else if (faceSubGeometries.circles.size() == 2) {      // Truncated cone. Circles correspond to the "bottom" and "top" circle
        // We will randomly assign one of the faceSubGeometries.circles as "reference" circle.
        // Normalization will take care of removing the randomization.
        auto circle1 = faceSubGeometries.circles[0];
        auto circle2 = faceSubGeometries.circles[1];
        bound1 = circle1.Location();
        bound2 = circle2.Location();
        diameter1 = circle1.Radius() * 2.0; // 3.
        diameter2 = circle2.Radius() * 2.0; // 4.
    } else {
        cerr << "Conical surface contains " << faceSubGeometries.circles.size() << " bounding faceSubGeometries.circles, can only handle 1 or 2!" << endl;
    }
    // If this is a truncated cone, we'll see faceSubGeometries.circles
    auto *elem = new ConicalElement(bound1, bound2, diameter1, diameter2, isHole);
    // Normalize direction etc
    elem->Normalize();
    elem->Validate();
    // Tolerate opposite axis when validating
    if (!elem->ValidateCoaxiality(cone.Axis(), true)) {
        cerr << "Cylinder is not coaxial to face base cylinder axis" << endl;
    }
    elem->description = label;
    elem->sourceFaces.push_back(faceIdx);
    volumetricElements.emplace_back(elem);
}

/**
 * Get the label for a given shape.
 * @return the label or "" if no label or a NONE label has been found
 */
const char *STEPFeatureDetector::GetLabelForShape(const TopoDS_Shape &shape) {
    for (auto &pair : topoNameMap) {
        if (shape.IsEqual(pair.first)) {
            return pair.second.c_str();
        }
    }
    return ""; // Nothing found
}

void STEPFeatureDetector::IterateFaces(const TopoDS_Solid &solid) {
    // Convert face explorer iterator to vector
    for (TopExp_Explorer faceExplorer(solid, TopAbs_FACE); faceExplorer.More(); faceExplorer.Next()) {
        const auto &face = TopoDS::Face(faceExplorer.Current());
        if(face.IsNull()) {
            continue;
        }
        allFaces.push_back(face);
    }
    // Iterate all the faces
    for (size_t i = 0; i < allFaces.size(); i++)
    {
        const TopoDS_Face &face = allFaces[i];
        const char *faceLabel = GetLabelForShape(face);
        cout << "Face " << faceLabel << endl;

        /**
        * Compute infinite underlying surface of the face.
        * Basically this tells us if we have:
        *  - A bounded section of a cylinder or
        *  - A bounded section of a planes
        */
        BRepLib_FindSurface bfs(face);
        if (!bfs.Found()) {
            cerr << "\tNo surface on face!" << endl;
            continue;
        }
        auto surf = bfs.Surface();
        cout << "\tSurf type: " << surf->DynamicType()->Name() << endl;

        if (surf->IsKind(STANDARD_TYPE(Geom_CylindricalSurface))) {
            auto csurf = Handle(Geom_CylindricalSurface)::DownCast(surf);
            HandleCylindricalSurface(i, face, csurf, faceLabel);
        } else if (surf->IsKind(STANDARD_TYPE(Geom_Plane))) {
            auto plane = Handle(Geom_Plane)::DownCast(surf);
            HandlePlane(i, face, plane, faceLabel);
        } else if (surf->IsKind(STANDARD_TYPE(Geom_ConicalSurface))) {
            auto geomCone = Handle(Geom_ConicalSurface)::DownCast(surf);
            HandleConicalSurface(i, face, geomCone, faceLabel);
        } else if (surf->IsKind(STANDARD_TYPE(Geom_ToroidalSurface))) {
            auto torus = Handle(Geom_ToroidalSurface)::DownCast(surf);
            HandleToroidalSurface(i, face, torus, faceLabel);
        } else {
            cerr << "Unexpected face surface type: " << surf->DynamicType()->Name() << endl;
        }
    }
}

void STEPFeatureDetector::PrintElements(const VolumetricElementCollection& elements) {
    // Print cylinders
    for (auto cylinder : elements) {
        cout << cylinder->ToString() << endl;
    }
}

void STEPFeatureDetector::ProcessCoaxialityClasses() {
    // TODO Actually compute coaxiality classes instead of just checking if everything is coaxial
    // First check if there is only one coaxiality class
    gp_Ax1 currentAxis;
    auto coaxialityGroups = GroupByCoaxiality(volumetricElements);
    cout << "Part has " << coaxialityGroups.size() << " coaxiality classes with a total of "
         << volumetricElements.size() << " primitives" << endl;
    if (coaxialityGroups.size() > 1) {
        /**
         * Heuristic: Part with the largest coaxial outer volume (=> ignore holes)
         * is the main axis.
         * This should cover almost all realistic usecases 
         */
        // Find max index.
        double maxVolume = 0.0;
        gp_Ax1 maxAxis;
        for (const auto& pair : coaxialityGroups) {
            double volume = ComputeVolume(pair.second, VolumeSummationMode::PositiveOnly);
            if (volume > maxVolume) {
                maxVolume = volume;
                maxAxis = pair.first;
            }
        }
        // Check validity
        if(maxVolume < linearTolerance) {
            throw MainAxisHasZeroVolumeException("");
        }
        // Now try to merge other axes
        cout << "Main axis is axis with volume " << maxVolume << " mm²" << endl;
        currentAxis = maxAxis;

        // Group by distance to identify possible faceSubGeometries.circles
        auto distGroups = GroupParallelByDistanceFromAxis(currentAxis, coaxialityGroups);
        // NOTE: Usuallys there are zero or none distance groups
        for(auto& kv: distGroups) {
            auto distance = kv.first;
            auto axisKeys = kv.second; // keys for coaxiality groups. NOTE: Use FindCoalityGroup()!
            cout << "Radial distance class: " << axisKeys.size() << " with distance " << distance << " mm" << endl;
            // Get one (arbitrary) bound point for each class. We can just use the point on the axis
            vector<gp_Pnt> boundPoints;
            boundPoints.reserve(axisKeys.size());
            for(const gp_Ax1& axisKey : axisKeys) {
                boundPoints.push_back(axisKey.Location());
            }
            // Get angles
            auto angles = Compute360AnglesAroundAxis(
                boundPoints, currentAxis, GetOrUpdateZeroDegreeReferenceVector(boundPoints[0], currentAxis));
            std::sort(angles.begin(), angles.end());
            /**
             * Try to find some regularity in the difference between adjacent angles
             * TODO We currently assume that there are no extra elements in this distance group,
             * i.e. every angle difference is the same.
             */
            vector<double> angleDifferences(angles.size() - 1);
            for (size_t i = 0; i < angles.size() - 1; i++) {
                angleDifferences[i] = angles[i + 1] - angles[i];
            }
            // Assume found hole circle if we have all-equal angles
            auto angleIntervalOptional = GetSingleElement(angleDifferences, linearTolerance);
            if(!angleIntervalOptional) {
                // Cant process this group any further, continue.
                cout << "Could not recognize hole circle because angle intervals are not equal: ";
                for (auto angle : angleDifferences) {
                    cout << angle;
                }
                cout << endl;
                continue; // Skip this distance class
            }
            double angleInterval = angleIntervalOptional.value();
            cout << "Found hole circle with " << angles.size() << " holes with " << angleInterval << "° angle" << endl;
            /**
             * Find out if all the coaxiality groups of the hole circle are equivalent
             * (if not, its not a proper hole circle)
             */
            // Get a list of hole circle coax groups
            vector<VolumetricElementCollection> holeCircleCoaxGroups;
            holeCircleCoaxGroups.reserve(axisKeys.size());
            size_t numHoleCircleCoaxGroupElements = 0;
            for (const gp_Ax1 &axisKey : axisKeys) {
                auto coaxGroupOptional = FindCoalityGroup(axisKey, coaxialityGroups);
                if(!coaxGroupOptional) {
                    cerr << "Coaxiality group not found. This is a programming error!" << endl;
                    continue;
                }
                auto coaxGroup = coaxGroupOptional.value();
                SortElements(coaxGroup);
                holeCircleCoaxGroups.push_back(coaxGroup);
                numHoleCircleCoaxGroupElements++;
            }
            cout << "Found " << holeCircleCoaxGroups.size() << " hole circle coax groups with " << numHoleCircleCoaxGroupElements
            << " elements in total" << endl;
            // Check equivalence of all groups (with the first one)
            const auto& group1 = holeCircleCoaxGroups[0];
            bool allAreEqual = true;
            // We use i=0 here intentionally to compare group1 to itself as a validity check
            for (size_t i = 0; i < holeCircleCoaxGroups.size(); i++) {
                const auto &groupi = holeCircleCoaxGroups[i];
                if (!CompareIgnoreLocation(group1, groupi)) {
                    allAreEqual = false;
                    cout << "Hole circle coaxiality group " << i << " is not equivalent to group1.\nGroup 1 [" << group1.size() << " elements]:" << endl;
                    for(const auto& elem : group1) {
                        cout << "\t" << elem->ToString() << endl;
                    }
                    cout << "Group " << i << " [" << groupi.size() << " elements]:\n";
                    for (const auto &elem : groupi) {
                        cout << "\t" << elem->ToString() << endl;
                    }
                    cout << endl;
                    break;
                }
            }
            if(!allAreEqual) {
                continue; // Skip this distance class
            }
            // Now that we know all are equivalent, just use pne group for further processing
            const auto &holeStructure = group1;
            cout << "Found structure of hole circle with " << holeStructure.size() << " elements:" << endl;
            for (const auto &elem : holeStructure) {
                cout << "\t" << elem->ToString() << endl;
            }
            // Assert that all things in the hole structure are holes, or else calling it a "hole circle"
            // does not make any sense
            bool isOK = true;
            for (size_t i = 1; i < holeStructure.size(); i++) { // Iterate groups of elements
                if(!holeStructure[i]->isHole) {
                    cerr << "Hole structure element " << i << " is not a hole ; non-hole radial elements cant be processed!" << endl;
                    isOK = false;
                }
            }
            if(!isOK) {
                continue;
            }
            /// Add all groups to the ignored elements list
            // list which will make them not appear on the unprocessed elements list.
            size_t numIgnored = 0;
            for (size_t i = 0; i < holeCircleCoaxGroups.size(); i++) { // Iterate groups of elements
                const auto &groupi = holeCircleCoaxGroups[i];
                for (size_t j = 0; j < groupi.size(); j++) { // Iterate elements
                    ignoredElements.push_back(groupi[j]);
                    numIgnored++;
                }
            }
            cout << "Ignored " << numIgnored << " hole circle elements" << endl;
            /**
             * Find element of currentAxis to which the hole circle belongs,
             * i.e. match boundary point of the hole circle to any of the elements.
             * For through-hole hole faceSubGeometries.circles we expect there to be two hole faceSubGeometries.circles
             */
            auto coaxGroupOptional = FindCoalityGroup(currentAxis, coaxialityGroups);
            if (!coaxGroupOptional) {
                cerr << "Current axis coaxiality group not found. This is a programming error!" << endl;
                continue;
            }
            auto currentAxisCoaxGroup = coaxGroupOptional.value();
            SortElements(currentAxisCoaxGroup);
            // Extract "raw" hole bounds. These are off-axis!
            const gp_Pnt holeBound1Raw = holeStructure[0]->bound1;
            const gp_Pnt holeBound2Raw = holeStructure[holeStructure.size() - 1]->bound2;
            // Project the points onto the axis
            const gp_Pnt holeBound1 = OrthogonalProjectPointOntoAxis(currentAxis, holeBound1Raw);
            const gp_Pnt holeBound2 = OrthogonalProjectPointOntoAxis(currentAxis, holeBound2Raw);
            PrimitivePlacement placement;

            VolumetricElementPtr matchingElement = nullptr;
            for (const auto &elem : currentAxisCoaxGroup) {
                // Matching only bound1-bound1 and bound2-bound2 is sufficient
                // because the bound order has been sorted while the VolumetricElements have been constructed
                gp_Pnt bound1 = elem->bound1;
                gp_Pnt bound2 = elem->bound2;
                if (bound1.IsEqual(holeBound1, linearTolerance)) {
                    placement = PrimitivePlacement::Left;
                    matchingElement = elem;
                    //cout << "Found left-matching surface for hole structure:\n\t" << elem->ToString() << endl;
                    break;
                } else if (bound2.IsEqual(holeBound2, linearTolerance)) {
                    placement = PrimitivePlacement::Right;
                    matchingElement = elem;
                    //cout << "Found right-matching surface for hole structure:\n\t" << elem->ToString() << endl;
                    break;
                }
            }

            if(matchingElement == nullptr) {
                cerr << "Could not find any matching surface for hole circle!" << endl;
                continue;
            } else {
                cout << "Found matching surface for hole circle with placement " << placement << " on element\n\t"
                << matchingElement->ToString() << endl;
            }
            /**
             * Build hole circle object
             */
            auto holeCircle = std::make_shared<HoleCircle>();
            holeCircle->diameter = 2.0 * distance;
            holeCircle->number = angles.size();
            holeCircle->angleInterval = angleInterval;
            holeCircle->structure = holeStructure;
            // The element order for the left face is left-to-right //TODO is it?
            /*if(placement == PrimitivePlacement::Left) {
                std::reverse(holeStructure.begin(), holeStructure.end());
            }*/
            matchingElement->AddFeature(dynamic_pointer_cast<ExtraFeature>(holeCircle), placement);
        }
        // TODO Implement fully excentric part support when available in the CAD
    } else {
        // One part axis only
        assert(volumetricElements.size() > 0);
        currentAxis = volumetricElements[0]->Axis();
    }
    /**
     * Add coaxial planars to the ignore list.
     * This prevents "end faces" to be shown as unprocessed element (we just dont care about them)
     * Note that coaxial also means that the center is on the axis.
     */
    for (const auto& planar : planarElements) {
        if(planar->IsCoaxialOrReversedCoaxial(currentAxis)) {
            ignoredElements.push_back(dynamic_pointer_cast<FeatureElement>(planar));
        }
    }
    RecognizePolygons(currentAxis);
}

std::vector<shared_ptr<RectangleElement>> STEPFeatureDetector::GetRectangles() {
    std::vector<shared_ptr<RectangleElement>> rectangleElements;
    for (const auto& planar : planarElements) {
        auto cast = dynamic_pointer_cast<RectangleElement>(planar);
        if (cast == nullptr) { // This is not a rectangle!
            continue;          // Ignore
        }
        rectangleElements.emplace_back(std::move(cast));
    }
    return rectangleElements;
}

void STEPFeatureDetector::RecognizePolygons(const gp_Ax1& currentAxis) {
    // Extract rectangles so we dont have to check types all the time
    auto rectangleElements = GetRectangles();
    /**
     * Find n-sided regular volumetric polygons
     */
    std::vector<bool> isRectangleCoaxialToCurrentAxis;
    isRectangleCoaxialToCurrentAxis.reserve(rectangleElements.size());
    std::transform(rectangleElements.begin(), rectangleElements.end(),
                   std::back_inserter(isRectangleCoaxialToCurrentAxis), [&](const auto &rect) {
                       // Planar axes can not be normalized, therefore coaxial || opposite
                       return rect->IsCoaxialOrReversedCoaxial(currentAxis);
                   });
    // Filter acoaxial parts
    vector<pair<gp_Pnt, shared_ptr<RectangleElement>>> orthogonalIntersectingParts;
    for (int i = 0; i < rectangleElements.size(); i++) {
        auto &rect = rectangleElements[i];
        if (isRectangleCoaxialToCurrentAxis[i]) {
            continue;
        }
        // not coaxial - maybe orthogonal and there is a cut point
        bool isOrthogonal = rect->Axis().IsNormal(currentAxis, angularTolerance);
        gp_Pnt intersection;
        bool hasIntersection = LineIntersection(rect->Axis(), currentAxis, intersection);
        if (isOrthogonal && hasIntersection) {
            cout << "Orthogonal & axis-intersecting plane: " << rect->ToString() << endl;
            // This is a candidate for a n-face.
            orthogonalIntersectingParts.emplace_back(intersection, rect);
        } else if (hasIntersection) {
            cout << "Axis-intersecting non-orthogonal plane: " << rect->ToString() << endl;
        } else if (isOrthogonal) { // No intersection
            cout << "Orthogonal plane: " << rect->ToString() << endl;
        } else {
            cout << "Non-coaxial plane: " << rect->ToString() << endl;
        }
    }
    // Find co-intersecting groups of planars
    auto coIntersectingGroups = GroupByPair(orthogonalIntersectingParts);
    // For each co-intersecting group
    for (auto &intersectPoint : coIntersectingGroups.keys) {
        // Get members of co-intersecting group
        auto rectangles = coIntersectingGroups.Values(intersectPoint);
        if (rectangles.size() <= 1) {
            cerr << "Found set of co-intersecting rectangles with only " << rectangles.size() << " elements" << endl;
            continue;
        }
        // norm.Angle() on the axis only gives unsigned angle, but we need the signed angle for proper operation.
        // Therefore we re-create the axis for each planar using the intersection point and the
        // planar center position.
        auto primitiveCenterPoints = GetAxisLocations(rectangles);
        // NOTE: We will compare angles with "arbitrarily" chossen index 0 planar
        auto zeroDegreeReferencePoint = primitiveCenterPoints[0];
        auto zeroDegreeReferenceVector = gp_Vec(intersectPoint, primitiveCenterPoints[0]);
        auto angles = Compute360AnglesAroundAxis(primitiveCenterPoints, gp_Ax1(intersectPoint, currentAxis.Direction()),
                                                 zeroDegreeReferenceVector);

        // TODO current assumption: The are no stray co-intersecting rectangles,
        // i.e. every co-intersecting planar must participate in the n-face
        // Are there cases where there could be additional
        // Need to especially consider a stray planar[0] which would make
        // the angles not relative to 0 but relative to another

        // Sort angles to be able to compare easily
        std::sort(angles.begin(), angles.end());

        // Check for appropriate polygon
        if (ArePlanarAnglesPolygonal(angles, angles.size())) {
            cout << "Found " << angles.size() << "-polygon " << (rectangles[0]->isHole ? "(hole)" : "") << endl;
        }
        /**
         * Get depth and boundary points along current axis.
         * In order to do that, we will just select an arbitrary rectangle.
         * NOTE: width and height axis are arbitrarily defined
         */
        auto rect = rectangles[0];
        gp_Pnt centerPoint; // of the corner-corner line parallel to currentAxis
        // Boundary points, to be projected to currentAxis
        double depth;
        gp_Pnt bound1OnRect, bound2OnRect;
        if (currentAxis.IsParallel(rect->WidthAxis(), angularTolerance)) {
            depth = rect->width;
            centerPoint = rect->WidthAxisCenter();
            bound1OnRect = rect->pointsOnWidthAxis[0];
            bound2OnRect = rect->pointsOnWidthAxis[1];
        } else if (currentAxis.IsParallel(rect->HeightAxis(), angularTolerance)) {
            depth = rect->height;
            centerPoint = rect->HeightAxisCenter();
            bound1OnRect = rect->pointsOnHeightAxis[0];
            bound2OnRect = rect->pointsOnHeightAxis[1];
        } else {
            cerr << "Neither width axis nor height axis of planar are parallel to the axis" << endl;
            continue;
        }
        // Check if the (center point - intersectPoint) vector is orthogonal to the axis
        if (!currentAxis.IsNormal(gp_Ax1(centerPoint, gp_Vec(centerPoint, intersectPoint)), angularTolerance)) {
            cerr << "Center point to intersect point vector is not orthogonal to axis" << endl;
            continue;
        }
        // Project boundary points on current axis (orthogonal i.e. shortest line projection)
        // to obtain bounds on the current axis
        auto bound1 = OrthogonalProjectPointOntoAxis(currentAxis, bound1OnRect);
        auto bound2 = OrthogonalProjectPointOntoAxis(currentAxis, bound2OnRect);
        // Get inner radius (face center to intersection point)
        double innerRadius = intersectPoint.Distance(rectangles[0]->Location());
        // Build polygonal element
        auto *polygon = new PolygonalElement(angles.size(), bound1, bound2, innerRadius, rectangles[0]->isHole);
        polygon->description = label;
        // As source faces use all faces from the rectangles
        polygon->sourceFaces = CollectSourceFacesFromAll(rectangles);
        polygon->Normalize();
        volumetricElements.emplace_back(polygon);
    }
}

void STEPFeatureDetector::ExportSTL(const std::string &prefix) {
    /**
     * Find out which shapes have been used in a volumetric element
     * or is ignored.
     */
    std::vector<bool> faceHasBeenUsed(allFaces.size());
    for(const auto& elem : volumetricElements) {
        for(int srcFace : elem->sourceFaces) {
            faceHasBeenUsed[srcFace] = true;
        }
    }
    for(const auto& elem : ignoredElements) {
        for(int srcFace : elem->sourceFaces) {
            faceHasBeenUsed[srcFace] = true;
        }
    }

    /**
     * Export single STL with everything
     */
    BRepMesh_IncrementalMesh(solid, 0.01);
    StlAPI::Write(solid, (prefix + ".stl").c_str(), true /* binary */);

    /**
     * Export STLs with unprocessed and processed faces
     */
    TopoDS_Compound processed, unprocessed;
    TopoDS_Builder processedBuilder, unprocessedBuilder;
    processedBuilder.MakeCompound(processed);
    unprocessedBuilder.MakeCompound(unprocessed);
    for(size_t i = 0 ; i < allFaces.size() ; i++) {
        if(faceHasBeenUsed[i]) {
            processedBuilder.Add(processed, allFaces[i]);
        } else {
            unprocessedBuilder.Add(unprocessed, allFaces[i]);
        }
    }
    //Mesh
    BRepMesh_IncrementalMesh(processed, 0.01);
    BRepMesh_IncrementalMesh(unprocessed, 0.01);
    // Export
    StlAPI::Write(processed, (prefix + ".processed.stl").c_str(), true /* text */);
    StlAPI::Write(unprocessed, (prefix + ".unprocessed.stl").c_str(), true /* text */);
}

gp_Vec STEPFeatureDetector::GetOrUpdateZeroDegreeReferenceVector(const gp_Vec &newVec) {
    if (!have0DegreeReferenceVector) {
        zeroDegreeReferenceVector = newVec;
        have0DegreeReferenceVector = true;
    }
    return zeroDegreeReferenceVector;
}

gp_Vec STEPFeatureDetector::GetOrUpdateZeroDegreeReferenceVector(const gp_Pnt &pnt, const gp_Ax1 &axis) {
    gp_Pnt projected = OrthogonalProjectPointOntoAxis(axis, pnt);
    return GetOrUpdateZeroDegreeReferenceVector(gp_Vec(projected, pnt));
}
