#include "Utils.hpp"
#include <sstream>

#include <boost/optional.hpp>

const gp_Dir xDir(gp_XYZ(1.0, 0.0, 0.0));
const gp_Dir yDir(gp_XYZ(0.0, 1.0, 0.0));
const gp_Dir zDir(gp_XYZ(0.0, 0.0, 1.0));

const char* CompoundTypeToString(TopAbs_ShapeEnum type) {
    switch (type) {
    case TopAbs_COMPOUND: return "compound";
    case TopAbs_COMPSOLID: return "compsolid";
    case TopAbs_SOLID: return "solid";
    case TopAbs_SHELL: return "shell";
    case TopAbs_FACE: return "face";
    case TopAbs_WIRE: return "wire";
    case TopAbs_EDGE: return "edge";
    case TopAbs_VERTEX: return "vertex";
    case TopAbs_SHAPE: return "shape";
    default: return "unknown";
    }
}

string XYZToString(const gp_XYZ &xyz) { return string_format("[%f, %f, %f]", xyz.X(), xyz.Y(), xyz.Z()); }

json XYZToJSON(const gp_XYZ &xyz) { return {xyz.X(), xyz.Y(), xyz.Z()}; }

string DirectionToString(const gp_Dir &dir) {
    if (dir.IsParallel(xDir, linearTolerance)) {
        return "\"X\"";
    } else if (dir.IsParallel(yDir, linearTolerance)) {
        return "\"Y\"";
    } else if (dir.IsParallel(zDir, linearTolerance)) {
        return "\"Z\"";
    } else {
        return XYZToString(dir.XYZ());
    }
}

json DirectionToJSON(const gp_Dir &dir) {
    if (dir.IsParallel(xDir, linearTolerance)) {
        return "X";
    } else if (dir.IsParallel(yDir, linearTolerance)) {
        return "Y";
    } else if (dir.IsParallel(zDir, linearTolerance)) {
        return "Z";
    } else {
        return XYZToJSON(dir.XYZ());
    }
}

string PointToString(const gp_Pnt &pnt) { return XYZToString(pnt.XYZ()); }

json PointToJSON(const gp_Pnt &pnt) { return XYZToJSON(pnt.XYZ()); }

gp_Pnt MiddlePoint(const gp_Pnt &p1, const gp_Pnt &p2) { return gp_Pnt((p1.XYZ() + p2.XYZ()) / 2.0); }

std::string GetCurrentDate() {
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);
    ostringstream ss;
    ss << std::put_time(&tm, "%d.%m.%Y");
    return ss.str();
}

const char* IFSelectReturnStatusToString(IFSelect_ReturnStatus code) {
    switch (code) {
    case IFSelect_RetVoid: return "Void";
    case IFSelect_RetDone: return "Done";
    case IFSelect_RetError: return "Error";
    case IFSelect_RetFail: return "Fail";
    case IFSelect_RetStop: return "Stop";
    default: return "Unknown";
    }
}

struct EqualWithinTolerance {
    double tolerance;
    EqualWithinTolerance(double tolerance) : tolerance(tolerance) {
    }

    bool operator()(double lhs, double rhs) const {
        return abs(lhs - rhs) > tolerance;
    }
};

boost::optional<double> GetSingleElement(const vector<double>& v, double tolerance) {
    if(v.empty()) {
        return boost::optional<double>();
    }
    // Use std::adjacent_find to exploit possible compiler optimizaton
    if (std::adjacent_find(v.begin(), v.end(), EqualWithinTolerance(tolerance)) == v.end()) {
        return boost::make_optional(v[0]);
    } else {
        return boost::optional<double>();
    }
}

std::ostream &operator<<(std::ostream &os, const gp_XY &xy) { return (os << '[' << xy.X() << ", " << xy.Y() << ']'); }

std::ostream &operator<<(std::ostream &os, const gp_Pnt2d &xy) { return os << xy.XY(); }

std::ostream &operator<<(std::ostream &os, const gp_Dir2d &xy) { return os << xy.XY(); }