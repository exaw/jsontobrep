#include "RapidfactureJSONExport.hpp"
#include "GeometryUtils.hpp"
#include "GroupBy.hpp"

void PushBackListOfVolumetricElements(json &j, const VolumetricElementCollection &volumetrics,
                                      PrimitivePlacement placement) {
    // Add elements, if any
    for (const auto &element : volumetrics) {
        j.push_back(element->ToRapidfactureJSON(placement));
    }
}

json RFJSONExportData::ToJSON(const std::string &stepLabel) {
    json j;
    j["meta"]["companyName"] = "";
    j["meta"]["marking"] = stepLabel;
    j["meta"]["drawingNumber"] = "<Import>"; // TODO?
    j["meta"]["draftsman"] = "";
    j["meta"]["checker"] = "";
    j["meta"]["date"] = GetCurrentDate(); // TO;
    j["meta"]["scale"] = "auto";
    j["meta"]["paperSize"] = "A4";
    j["meta"]["units"] = "mm";
    j["meta"]["generalToleranceNorm"] = "DIN ISO 2768";
    j["meta"]["generalToleranceClass"] = "m";
    j["meta"]["material"] = "16MnCr5";
    j["meta"]["surface"] = "Ra 6,3";
    j["meta"]["innerEdgeMin"] = 0.3;
    j["meta"]["innerEdgeMax"] = 0.5;
    j["meta"]["outerEdgeMin"] = -0.1;
    j["meta"]["outerEdgeMax"] = 0.0;
    // Ensure innerContour and outerContour are always at least [], not null or not present
    j["innerContour"] = json::array();
    j["outerContour"] = json::array();
    // Add contours
    PushBackListOfVolumetricElements(j["outerContour"], outerShape, PrimitivePlacement::Nothing);
    PushBackListOfVolumetricElements(j["innerContour"], leftInnerShape, PrimitivePlacement::Left);
    PushBackListOfVolumetricElements(j["innerContour"], rightInnerShape, PrimitivePlacement::Right);
    return j;
}

bool RFJSONExportData::FindNextLeftElement(const VolumetricElementCollection &allElements,
                                           std::vector<bool> &alreadyProcessed) {
    bool foundSomething = false;
    for (size_t i = 0; i < allElements.size(); i++) {
        if (alreadyProcessed[i]) {
            continue;
        }
        const auto &elem = allElements[i];
        if (elem->bound1.IsEqual(leftBorder, linearTolerance)) {
            // Check continous through
            if (elem->bound2.IsEqual(leftBorder, linearTolerance)) {
                isThroughHole = true;
            }
            leftInnerShape.emplace_back(elem);
            leftBorder = elem->bound2;
            alreadyProcessed[i] = true;
            foundSomething = true;
        }
        // Continue iterating, maybe we'll find another element without starting the loop over
    }
    return foundSomething;
}

bool RFJSONExportData::FindNextRightElement(const VolumetricElementCollection &allElements,
                                            std::vector<bool> &alreadyProcessed) {
    bool foundSomething = false;
    for (size_t i = 0; i < allElements.size(); i++) {
        if (alreadyProcessed[i]) {
            continue;
        }
        const auto &elem = allElements[i];
        if (elem->bound2.IsEqual(rightBorder, linearTolerance)) {
            rightInnerShape.emplace_back(elem);
            rightBorder = elem->bound1;
            // Continous through holes would already be known from the left border run
            alreadyProcessed[i] = true;
            foundSomething = true;
        }
        // Continue iterating, maybe we'll find another element without starting the loop over
    }
    return foundSomething;
}

void RFJSONExportData::Load(const VolumetricElementCollection &allElements) {
    cout << "Loading " << allElements.size() << " elements for export" << endl;
    // Mark elements here that shall be ignored
    std::vector<bool> alreadyProcessed(allElements.size());
    // Load outer elements
    for (size_t i = 0; i < allElements.size(); i++) {
        if (!allElements[i]->isHole) {
            outerShape.emplace_back(allElements[i]);
            alreadyProcessed[i] = true;
        }
    }
    if (outerShape.empty()) {
        cerr << "Could not find any outer shape" << endl;
        return;
    }
    // Sort in order to be able to easily obtain the left and right border
    SortOuterElements();
    leftBorder = outerShape[0]->bound1;
    rightBorder = outerShape[outerShape.size() - 1]->bound2;
    // Try to find left border elements iteratively
    while (FindNextLeftElement(allElements, alreadyProcessed)) {
    }
    if (!isThroughHole) {
        // Try to find left border elements iteratively
        while (FindNextRightElement(allElements, alreadyProcessed)) {
        }
    }
    // Right elements are inserted in the wrong order but std::vector prepending is SLOW. Fix that!
    std::reverse(rightInnerShape.begin(), rightInnerShape.end());
    // Print stats
    if (isThroughHole) {
        cout << "Found " << outerShape.size() << " outer contours and " << leftInnerShape.size()
             << " inner contours (through hole)" << endl;

    } else {
        cout << "Found " << outerShape.size() << " outer contours, " << leftInnerShape.size()
             << " left inner contours and " << rightInnerShape.size() << " right inner contours" << endl;
    }
    /**
     * Print leftover elements
     */
    for (size_t i = 0; i < allElements.size(); i++) {
        if (alreadyProcessed[i]) {
            continue;
        }
        cout << "Found unprocessed element: \n\t" << allElements[i]->ToString() << endl;
    }
    /**
     * Validate that outer elements are adjacent
     */
    CheckAdjacency();
}

void RFJSONExportData::CheckAdjacency() {
    // Check pairs
    for (size_t i = 0; i < outerShape.size() - 1; i++) {
        auto a = outerShape[i];
        auto b = outerShape[i + 1];
        // Compute signed distance
        auto dist = a->bound2.Distance(b->bound1);
        if(a->bound2 > b->bound1) {
            dist = -dist;
        }
        if (dist > linearTolerance) {
            // TODO prints false results as inner holes and outer holes are not treated separately
            cerr << "Element " << i << " and " << (i + 1) << " of the outer shell have a gap of " << dist
                 << " mm between them:\n\t" << a->ToString() << "\n\t" << b->ToString() << endl;
            ;
        }
    }
}

void RFJSONExportData::SortOuterElements() {
    std::sort(outerShape.begin(), outerShape.end(), [](const VolumetricElementPtr &a, const VolumetricElementPtr &b) {
        return a->Location() < b->Location();
    });
}