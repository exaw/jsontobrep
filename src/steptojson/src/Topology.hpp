#pragma once

#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

/**
 * Find out if a given face is part of a hole or of the outside of the solid
 * @return true if solid, false if hole
 */
bool IsFacePartOfHole(const TopoDS_Face &face);

/**
 * Like ShapeAnalysis::OuterWire, but returns first inner wire
 */
TopoDS_Wire InnerWire(const TopoDS_Face &face);
