#pragma once
#include <stdexcept>

using namespace std;

/**
 * Baseclass for exceptions that occured before or during parsing the file,
 * meaning that no Brep could be derived
 */
struct FileLevelException : std::invalid_argument {
    FileLevelException(const char *msg) : invalid_argument(msg) {}
};

struct STEPFileDoesNotExistException : FileLevelException {
    STEPFileDoesNotExistException(const char *msg) : FileLevelException(msg) {}
};

struct STEPFileInvalidException : FileLevelException {
    STEPFileInvalidException(const char *msg) : FileLevelException(msg) {}
};

struct NoEntitiesInSTEPFileException : FileLevelException {
    NoEntitiesInSTEPFileException(const char *msg) : FileLevelException(msg) {}
};

struct NoTransferableRootsInSTEPFileException : FileLevelException {
    NoTransferableRootsInSTEPFileException(const char *msg) : FileLevelException(msg) {}
};

struct STEPToTopoShapeConversionFailed : FileLevelException {
    STEPToTopoShapeConversionFailed(const char *msg) : FileLevelException(msg) {}
};

struct STEPShapeIsNotASolidException : FileLevelException {
    STEPShapeIsNotASolidException(const char *msg) : FileLevelException(msg) {}
};

/**
 * Baseclass for exceptions that occured before or during parsing the file,
 * meaning that no Brep could be derived
 */
struct PartLevelException : std::invalid_argument {
    PartLevelException(const char *msg) : invalid_argument(msg) {}
};

struct MainAxisHasZeroVolumeException : PartLevelException {
    MainAxisHasZeroVolumeException(const char *msg) : PartLevelException(msg) {}
};
