#pragma once
#include "Elements.hpp"
#include "GroupBy.hpp"
#include "Utils.hpp"
#include "common.hpp"
#include <GeomAPI_ExtremaCurveCurve.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <Geom_Line.hxx>
#include <Geom_BSplineCurve.hxx>
#include <cassert>
#include <gp_Ax1.hxx>
#include <gp_Ax2d.hxx>
#include <gp_Lin.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pnt2d.hxx>
#include <map>
#include <math_Matrix.hxx>
#include <set>

#include <optional>

bool IsCoaxialOrReversedCoaxial(const gp_Ax1 &firstAxis, const gp_Ax1 &secondAxis);

/**
 * Get the intersection point of two lines
 * @return true if intersected ; point is stored in intersect parameter
 */
bool LineIntersection(const gp_Lin &lin1, const gp_Lin &lin2, gp_Pnt &intersect);
bool LineIntersection(const gp_Lin2d &lin1, const gp_Lin2d &lin2, gp_Pnt2d &intersect);
bool LineIntersection(const gp_Ax1 &lin1, const gp_Ax1 &lin2, gp_Pnt &intersect);
bool LineIntersection(const gp_Ax2d &lin1, const gp_Ax2d &lin2, gp_Pnt2d &intersect);

bool IsNormal(const gp_Lin &lin1, const gp_Lin &lin2);

bool IsParallel(const gp_Lin &lin1, const gp_Lin &lin2);

gp_Pnt OrthogonalProjectPointOntoAxis(const gp_Ax1 &ax, const gp_Pnt &pnt);

gp_Pnt2d OrthogonalProjectPointOntoAxis(const gp_Ax2d &ax, const gp_Pnt2d &pnt);

/**
 * Try to parse a rectangle from two lines
 */
RectangleElement *TryParseRectangle(const gp_Dir &direction, GPLineVector &lines);

/**
 * Takes sorted signed angles in degrees and computes if the vector contains all
 * angles that make up a n-polygon
 */
bool ArePlanarAnglesPolygonal(const std::vector<double> &angles, int n);


struct FaceSubGeometries {
    GPLineVector lines;
    GPCircVector circles;
    InterpolatedBSplineVector bsplines;
};

/**
 * For a given face, get the geometric elements that make up its outer contour,
 * if there is any such face.
 * Note that the curves are not trimmed.
 */
FaceSubGeometries SplitFaceSubGeometries(const TopoDS_Face &face, double deflection=1e-3);

/**
 * Convert a point from the absolute 3D coordinate system to an
 * axial(X)-radial(Y)-cartesian coordinate system that ignores
 * rotation around the axis and has these coordinates.
 * With:
 *    (L) is the location of the given axis
 *    (P) is the given point
 *    (O) is the orthogonal projection of (P) onto the given axis
 * Then:
 *    X = Distance between (L) and (O) (signed, + is in the direction of the axis)
 *    Y = Distance between (O) and (P)
 * i.e. X is the axial direction distance, Y is the radial distance from the axis.
 * NOTE: Due to the definition, a negative Y is not possible, therefore
 * the resulting coordinate system has only 2 quadrants.
 * @param absolute set to true if you want unsigned, i.e. absolute distances from the reference point
 */
gp_Pnt2d PointToAxialRadialCartesianCoordinate(const gp_Ax1 &axis, const gp_Pnt &pnt, bool absolute=false);
vector<gp_Pnt2d> PointsToAxialRadialCartesianCoordinates(const gp_Ax1 &axis, const vector<gp_Pnt> &pnts, bool absolute=false);

/**
 * Validate that this is a simple buch of circles:
 * Coaxial and same radius, i.e. those are just
 * segments of the same circle
 * @return true on successful validation
 */
bool ValidateAllCirclesCoaxialAndSameRadius(const GPCircVector &circles);

gp_Pnt PointOnRadius(const gp_Circ &circ);

bool operator<(const gp_Pnt &a, const gp_Pnt &b);
bool operator<=(const gp_Pnt &a, const gp_Pnt &b);
bool operator>(const gp_Pnt &a, const gp_Pnt &b);
bool operator<(const gp_Ax1 &a, const gp_Ax1 &b);
bool operator<(const gp_XYZ &a, const gp_XYZ &b);

/**
 * Compute a point for the given axis ax and point pnt that fulfils these properties:
 *  - For the same, a coaxial or reverse axis, the same point is returned
 *  - The point does not lie on the plane.
 *  - If pnt is on the axis, the orthogonal projection of the result point onto ax is pnt.
 * It is intended to be used to get a reference defining a plane
 */
/*gp_Pnt ComputeReferencePointForAxis(const gp_Ax1& ax, const gp_Pnt& pnt) {
    gp_Lin lin(ax); // Allows to use Contains()
    // These points will be tried in order. One of them is guaranteed not to lie on the axis.
    gp_Pnt try1(pnt.X(), pnt.Y())
    if(ax.)
}*/

/**
 * Extract ->Axis().Location() for every element
 */
template <typename T> vector<gp_Pnt> GetAxisLocations(const std::vector<T> &primitives) {
    std::vector<gp_Pnt> points(primitives.size());
    for (size_t i = 0; i < primitives.size(); i++) {
        points[i] = primitives[i]->Axis().Location();
    }
    return points;
}

/**
 * Get all source faces for he
 */
template <typename T> vector<int> CollectSourceFacesFromAll(const std::vector<T> &primitives) {
    vector<int> ret;
    for (const auto &prim : primitives) {
        for (const int face : prim->sourceFaces) {
            ret.push_back(face);
        }
    }
    return ret;
}

/**
 * Compute 0-360° angles around a given base point, with a given 0° reference.
 * Returns a vector of 0-360° angles (in degrees) of the same length as points
 */
vector<double> Compute360AnglesAroundAxis(const std::vector<gp_Pnt> &points, const gp_Ax1 &axis,
                                          const gp_Vec &zeroDegReferenceVector);

enum class VolumeSummationMode {
    All,          // outer minus holes
    PositiveOnly, // outer
    NegativeOnly  // holes
};

/**
 * Compute total volume of a given set of primitives, using a given summation mode
 */
double ComputeVolume(const VolumetricElementCollection &primitives, VolumeSummationMode mode);

double AxisDistance(const gp_Ax1 &ax1, const gp_Ax1 &ax2);

typedef map<gp_Ax1, VolumetricElementCollection> CoaxialityGroups;

/**
 * Group elements which are coaxial or reversed coaxial into one group
 */
template <typename T> CoaxialityGroups GroupByCoaxiality(const vector<T> &primitives) {
    CoaxialityGroups ret;
    for (const auto &primitive : primitives) {
        auto axis = primitive->Axis();
        // Try to find other axis which is coaxial
        bool finished = false;
        for (auto &otherPair : ret) {
            if (IsCoaxialOrReversedCoaxial(axis, otherPair.first)) {
                otherPair.second.push_back(primitive);
                // This entity is finished!
                finished = true;
                break;
            }
        }
        if (!finished) {
            VolumetricElementCollection elems = {primitive};
            ret.insert({axis, elems});
        }
    }   
    return ret;
}

struct DifferenceLessThanLinearToleranceComparator {
    bool operator()(const double &lhs, const double &rhs) const;
};

typedef map<double, vector<gp_Ax1>> DistanceGroups;

/**
 * Group elements which have the same distance into one group.
 * Ignores primitives which are not parallel to refAxis.
 * Ignores primitives with distance 0.
 */
DistanceGroups GroupParallelByDistanceFromAxis(const gp_Ax1 &refAxis, const CoaxialityGroups &primitives);

VolumetricElementCollection UniquifyElements(const VolumetricElementCollection &src, FeatureElementCollection &ignored);

/**
 * Sort elements according to a predefined metric)
 */
void SortElements(VolumetricElementCollection &src);

/**
 * Find an element in a set of coaxiality groups, comparing the axes using IsCoaxialOrReversedCoaxial()
 */
std::optional<VolumetricElementCollection> FindCoalityGroup(const gp_Ax1 &axis,
                                                                          const CoaxialityGroups &groups);

/**
 * Find and fix undetected holes (i.e. element that are not holes but should be).
 * An element is an undetected hole if it is contained within an outer element.
 */
void FixUndetectedHolesByContains(const VolumetricElementCollection &elems);

vector<gp_Pnt> InterpolateEquidistantPointsOnCurve(Handle(Geom_BSplineCurve) bspline, double deflection=1e-3);

enum class Coordinate { X, Y, Z };
enum class ExtremumOperation { Min, Max };

/**
 * Get the point that has the maximum coordinate.
 * The coordinate used is defined by mode.
 */
gp_Pnt2d PointOfExtremeCoordinate(const vector<gp_Pnt2d> &vec, Coordinate mode,
                                  ExtremumOperation op = ExtremumOperation::Max);

/**
 * Get the point of vec that has the maximum/minimum distance to refPnt
 */
gp_Pnt PointOfExtremeDistance(const vector<gp_Pnt> &vec, const gp_Pnt &refPnt,
                              ExtremumOperation op = ExtremumOperation::Max);

/**
 * Extract the given coordinate for all of the given points
 */
vector<double> ExtractCoordinate(const vector<gp_Pnt2d> &pnts, Coordinate mode);
